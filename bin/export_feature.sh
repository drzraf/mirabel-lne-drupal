#!/bin/bash

# permissions to exlude by module name:
# names = devel, system, locale
#ignore2=$(drush @beta ev 'echo implode(")|(", array_keys(devel_permission() + system_permission() + locale_permission()));')
ignore2=$(drush @beta ev 'echo implode(")|(", array_keys(system_permission() + locale_permission()));')
ignore1='contextual|inline|filters|audio|help|overlay|shortcut|toolbar|terms|(delete any)'

# add permission features ignoring perm related to the above
mapfile -t permfeatures < <( drush @beta features-components % | sed -nr \
    -e "/$ignore1/d" \
    -e "/($ignore2)/d" \
    -e '/user_permission/s/ (user_permission:.*[^ ]) +$/\1/p' )

echo "${permfeatures[@]}"
exit
# node: article, assoc, book, page
# field: notably: user-user-%
drush @beta features-export --destination=sites/all/modules/mirabel/modules mirabel_features \
    node \
    field \
    user_role:adherent	\
    views_view:vassoc views_view:theme_env views_view:presse \
    views_view:vie_associative views_view:dossiers views_view:veille_juridique \
    "${permfeatures[@]}"

# TODO:
# triggers (with patch) + actions ?
# navigation menu
# taxonomy (+ terms) => taxonomy_xml

# drush @beta features-export|grep taxonomy:|cut -d: -f2
