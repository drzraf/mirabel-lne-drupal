$ = jQuery;

/**
   credits to David Lynch for MapHighlight:
   http://davidlynch.org/projects/maphilight/docs/
*/
function assoimg(url) {
    if(url == '')
	$('#assoimg').empty();
    else
	$('#assoimg').html('<img src="' + url + '" />');
}

function assoblockinit() {
    for(depid in Drupal.settings.mirabel) {
	var dep = Drupal.settings.mirabel[depid];
	var div = $("<div />");

	for(var i in dep) {
	    var adiv = $("<div />");

	    var a = $("<a />");
	    a.attr('href', '/node/' + dep[i].nid);
	    if(dep[i].imgpath) {
		var img = $("<img />");
		img.attr('src', dep[i].imgpath);
		img.addClass("assomap");
		adiv.append(img);

		a.attr('onmouseover', 'assoimg("' + dep[i].imgpath + '")');
		a.attr('onmouseout', 'assoimg("")');
	    }
	    else {
		adiv.css('padding-left', '40px');
	    }

	    if(dep[i].adhesion == 1)
		a.css('font-weight', 'bold');
	    a.text(dep[i].title);

	    adiv.append(a);

	    if(dep[i].ville)
		adiv.append(' (' + dep[i].ville + ')');
	    div.append(adiv);
	}

	div.addClass('dep'+depid+" miradepblock").hide();
	//$("#mapdesc").html(div);
	$("#mapdesc").append(div);
    }
}

function assoblockshow(depid) {
    $('#mapdesc div.miradepblock').hide();
    $("div.dep"+depid).show('slow');
}

function toggleKeepHighlight(element) {
    var data = $(element).mouseout().data('maphilight') || {};
    data.alwaysOn = !data.alwaysOn;
    $(element).data('maphilight', data).trigger('alwaysOn.maphilight');
}

var maplocked = false;

$(document).ready(function(){
    //assoblockinit();
    $('.map').maphilight();
    $('map[name=lorraine] area').each(function() {
	$(this).click(function() {
	    var depid = $(this).attr("title");
	    if(depid == maplocked)
		maplocked = false;
	    else {
		toggleKeepHighlight("map[name=lorraine] area[title="+maplocked+"]");
		maplocked = depid;
	    }
	    toggleKeepHighlight($(this));
	    assoblockshow(depid);
	});

	$(this).mouseenter(function() {
	    var depid = $(this).attr("title");
	    if(!maplocked)
		assoblockshow(depid);
	});
    });
});
