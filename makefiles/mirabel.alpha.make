core = "7.x"
api = "2"

# audiofield


# projects[uuid][type] = "module"
# projects[uuid][download][type] = "git"
# projects[uuid][download][branch] = "7.x-1.x"
# # optionnal: Support for filefield
# projects[uuid][patch][] = "https://drupal.org/files/issues/uuid-files-808690-18.patch"

# projects[uuid_features][type] = "module"
# projects[uuid_features][download][type] = "git"
# projects[uuid_features][download][branch] = "7.x-1.x"

# taxonomy feature drush export:
# # Add component that exports vocabulary with all it's terms (applies with -F3)
# projects[uuid_features][patch][] = "https://drupal.org/files/issues/uuid_features-965450-17.patch"
# # Add support for related terms and synonyms (applies with -F3)
# projects[uuid_features][patch][] = "https://drupal.org/files/issues/uuid_features-966510-4.patch"

# theme views with preprocess() only (#939462)
projects[drupal][patch][] = "https://drupal.org/files/939462-48.patch"

projects[visualize_backtrace][type] = "module"
projects[visualize_backtrace][download][type] = "git"
projects[visualize_backtrace][download][branch] = "6.x-1.x"
projects[visualize_backtrace][patch][] = "http://drupal.org/files/issues/d7_upgrade-988918-8.patch"

#todo: fixes branches and patch (rm'ed alpha)
projects[og][type] = "module"
projects[og][download][type] = "git"
#projects[og][download][branch] = "7.x-2.x"

projects[panels][type] = "module"
projects[panels][download][type] = "git"
#projects[panels][download][branch] = "7.x-2.x"

projects[migrate][type] = "module"
projects[migrate][download][type] = "git"
#projects[migrate][download][branch] = "7.x-2.x"

# helpful file_uri_to_object
# https://drupal.org/node/685818

# migrate with ?
# https://drupal.org/project/patterns
