core = "7.x"
api = "2"

# needs #640352 and #1149910 : relative paths
# + #1205856 : media widget integration
projects[insert][type] = "module"
projects[insert][download][type] = "git"
projects[insert][download][branch] = "7.x-1.x"
# insert/media integration
projects[media][patch][] = "http://drupal.org/files/add-insert-suport-for-media-1140404-2.patch"

# cropping: #969284
# projects[media][patch][] = "http://drupal.org/files/media-mit-test-changes.patch"

projects[pdfthumb][type] = "module"
projects[pdfthumb][download][type] = "git"
projects[pdfthumb][download][branch] = "7.x-1.x"
projects[pdfthumb][patch][] = "http://drupal.org/files/1760618-media-widget.patch"
projects[pdfthumb][patch][] = "http://drupal.org/files/1761632-schema-update.patch"
projects[pdfthumb][patch][] = "http://drupal.org/files/1761632-no-double-thumbnails.patch"
# includes http://drupal.org/files/1762034-seetings.patch
projects[pdfthumb][patch][] = "http://drupal.org/files/1762060-customdir.patch"


# nodesinblock module ?
# + 1463806 (https://drupal.org/files/nodesinblock-hideblocktitle.patch)
# ... or rather use views
