core = "7.x"
api = "2"

# MEDIA submodule
projects[media_image_transform][type] = "module"
projects[media_image_transform][download][type] = "git"
projects[media_image_transform][download][url] = "http://git.drupal.org/sandbox/cocoloco/1090384.git"
projects[media_image_transform][destination] = "modules/media/modules/media_image_transform"

# MEDIA submodule patch: #969284
projects[media][patch][] = "http://drupal.org/files/media-mit-test-changes.patch"

# amazing:
# https://drupal.org/node/1193036#comment-5122514


projects[video][type] = "module"
projects[video][download][type] = "git"
projects[video][download][branch] = "7.x-2.x"


# jQuery-File-Upload
libraries[jqueryFU][download][type] = "git"
libraries[jqueryFU][download][url]  = "git://github.com/blueimp/jQuery-File-Upload.git"

projects[multiform][type] = "module"
projects[multiform][download][type] = "git"
projects[multiform][download][branch] = "master"

# media multiselect file => plupload
projects[media][patch][] = "http://drupal.org/files/media-views_tab_multiselect-1658452-9.patch"
# test http://git.drupal.org/sandbox/fangel/1652676.git
