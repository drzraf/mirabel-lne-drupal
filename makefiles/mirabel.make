

projects[nodeblock][type] = "module"
projects[nodeblock][download][type] = "git"
projects[nodeblock][download][branch] = "7.x-1.x"

projects[wysiwyg][type] = "module"
projects[wysiwyg][download][type] = "git"
projects[wysiwyg][download][branch] = "7.x-2.x"
# maybe 372165

# no: needs (the fat) Ant to build
#libraries[tinymce][download][type] = "git"
#libraries[tinymce][download][url]  = "git://github.com/tinymce/tinymce.git"
#libraries[tinymce][download][branch] = "3.4.7"
libraries[tinymce][download][type] = "file"
libraries[tinymce][download][url]  = "http://github.com/downloads/tinymce/tinymce/tinymce_3.4.7.zip"

libraries[tinymce_i18n][download][type] = "file"
libraries[tinymce_i18n][download][url]  = "http://tinymce.moxiecode.com/i18n/index.php?ctrl=export&act=zip"
libraries[tinymce_i18n][download][request_type] = "post"
libraries[tinymce_i18n][download][data] = "la[]=fr&la_export=js&pr_id=7&submitted=Download"
libraries[tinymce_i18n][download][filename] = "tinymce_language_pack.zip"
#libraries[tinymce_i18n][destination] = "libraries/tinymce/jscripts/tiny_mce"
libraries[tinymce_i18n][destination] = "libraries/tinymce/jscripts"
libraries[tinymce_i18n][directory_name] = "tiny_mce"

# see media stuff
includes[] = "mirabel.media.make"

libraries[openlayers][download][type] = "file"
libraries[openlayers][download][url]  = "https://github.com/openlayers/ol2/releases/download/release-2.13.1/OpenLayers-2.13.1.tar.gz"

# includes[] = "mirabel.beta.make"

# # taxonomy/menus migration (future)
# includes[] = "mirabel.beta.make"

# migration
projects[taxonomy_xml][type] = "module"
projects[taxonomy_xml][download][type] = "git"
projects[taxonomy_xml][download][branch] = "7.x-1.x"
# "taxonomy_xml_lookup_services does not exist"
projects[taxonomy_xml][patch][] = "http://drupal.org/files/taxonomy_xml_lookup_services.patch"
# RDF compat'
projects[taxonomy_xml][patch][] = "http://drupal.org/files/1363584-1.patch"
# drush import/export
# projects[taxonomy_xml][patch][] = "http://drupal.org/files/1363612-taxonomy_xml.drush_.inc_.txt"
# wget the above

# special_menu_items: allow empty parent menu links
#  use <a href="#"> for the HTML tag for "nolink"

# see http://drupal.org/node/545452
# variable_set('install_profile', 'mirabel'); ?

# facultative todo: #1206340 (options-1206340-20.patch)
# mandatory todo: #1776632 (1776632-fix-tty-mess-2.patch)
