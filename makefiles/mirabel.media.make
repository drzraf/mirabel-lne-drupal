core = "7.x"
api = "2"

projects[media][type] = "module"
projects[media][download][type] = "git"
projects[media][download][branch] = "7.x-2.0-alpha4"

# inline not only <img>: #1283844
# projects[media][patch][] = "http://drupal.org/files/media_inline-test.patch"
# overruled by #1451316

# wysiwyg button
## projects[media][patch][] = "http://drupal.org/files/remove-legacy-form-autosubmit_1301774_1.patch"
## merged

# non-square thumbnail media browser: #1502060
## projects[media][patch][] = "http://drupal.org/files/cleanup-css-1502060-12.patch"
## merged

# buggy media browser
# projects[media][patch][] = "http://drupal.org/files/1417436-19.patch"

# media_fid_illegal_value #1142630 : merged


# see also https://www.drupal.org/node/951004
projects[media_multiselect][type] = "module"
projects[media_multiselect][download][type] = "git"
projects[media_multiselect][download][branch] = "master"
projects[media_multiselect][download][url] = "http://git.drupal.org/sandbox/fangel/1652676.git"

projects[multiform][type] = "module"
projects[multiform][download][type] = "git"
projects[multiform][download][branch] = "master"


# jplayer
projects[jplayer][type] = "module"
projects[jplayer][download][type] = "git"
projects[jplayer][download][branch] = "7.x-2.x"
# drupal coders sux so much
projects[jplayer][patch][] = "http://drupal.org/files/1405680-1-revert-audio-video.patch"

#libraries[jplayer][download][type] = "git"
#libraries[jplayer][download][url]  = "git://github.com/happyworm/jPlayer.git"
#libraries[jplayer][download][tag]  = "2.1.0"
libraries[jplayer][download][type] = "file"
libraries[jplayer][download][url]  = "http://jplayer.org/latest/jQuery.jPlayer.2.1.0.zip"


projects[styles][type] = "module"
projects[styles][download][type] = "git"
projects[styles][download][branch] = "7.x-2.x"
# usable link
projects[styles][patch] = "http://drupal.org/files/1414016-2-media-file-path.patch"
# usable link for large "format"
projects[styles][patch] = "http://drupal.org/files/1606032-link-large-format.patch"



projects[plupload][type] = "module"
projects[plupload][download][type] = "git"
projects[plupload][download][branch] = "7.x-1.x"
# media integration: #1476830
projects[plupload][patch][] = "http://drupal.org/files/media-upload-widget--1476830-2.patch"

# no: needs (the fat) Ant to build
#libraries[plupload][download][type] = "git"
#libraries[plupload][download][url]  = "git://github.com/moxiecode/plupload.git"
#libraries[plupload][download][branch] = "master"
libraries[plupload][download][type] = "file"
libraries[plupload][download][url]  = "http://github.com/downloads/moxiecode/plupload/plupload_1_5_4.zip"
