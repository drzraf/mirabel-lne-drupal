<?php

/*
  $i = user_load(18);
  mirabel_notify(array($i), "fresh-test-password");
*/
function mirabel_notify($accounts, $ppass) {
  foreach ($accounts as $account) {
    $params['account'] = $account;
    $params['plain_pass'] = $ppass;

    // todo http://code.alexreisner.com/articles/sending-email.html
    drupal_mail('mirabel', 'welcome', $account->mail, user_preferred_language($account), $params); // , NULL, false
  }
}

function mirabel_mail($key, &$message, $params) {
  $options['language'] = $message['language'];
  $data['user'] = $params['account'];

  switch($key) {
  case 'welcome':
    $message['subject'] =  _user_mail_text('register_no_approval_required_subject', $message['language'], $data);

    $text = t("[user:name],

Un compte vient d'être créé sur [site:name].

Vous pouvez désormais vous connecter à l'aide des identifiants ci-dessous
en vous rendant à l'adresse [site:login-url]

Il est conseillé de changer votre mot de passe lors de votre première connexion.

identifiant : [user:name]
mot de passe : !PASS

--  L'équipe de [site:name]", array('!PASS' => $params['plain_pass']), array('langcode' => $message['language']->language));

    $message['body'][0] = token_replace($text, $data, array('language' => $message['language'], 'sanitize' => FALSE));
    break;
  }
}
