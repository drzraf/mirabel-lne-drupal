<?php
/**
 * @file
 * flore54_features.features.inc
 */

/**
 * Implements hook_views_api().
 */
function flore54_features_views_api() {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function flore54_features_node_info() {
  $items = array(
    'assoc' => array(
      'name' => t('Association'),
      'base' => 'node_content',
      'description' => t('Une association, ses thématiques, actions, sa localisation, son site, ...'),
      'has_title' => '1',
      'title_label' => t('Intitulé'),
      'help' => '',
    ),
  );
  return $items;
}
