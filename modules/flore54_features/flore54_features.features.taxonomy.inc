<?php
/**
 * @file
 * flore54_features.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function flore54_features_taxonomy_default_vocabularies() {
  return array(
    'alltags' => array(
      'name' => 'Autres mots-clef',
      'machine_name' => 'alltags',
      'description' => 'Autres mots-clef libres',
      'hierarchy' => '1',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
    'tags' => array(
      'name' => 'Mots-clef autour de l\'écologie',
      'machine_name' => 'tags',
      'description' => '',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '-10',
    ),
  );
}
