<?php
/**
 * @file
 * flore54_features.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function flore54_features_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'vassoc';
  $view->description = 'View for associations';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'associations';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Associations adhérentes au réseau Mirabel-LNE';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['use_more_text'] = 'plus';
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Appliquer';
  $handler->display->display_options['exposed_form']['options']['reset_button_label'] = 'Réinitialiser';
  $handler->display->display_options['exposed_form']['options']['exposed_sorts_label'] = 'Trier par';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '20';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['expose']['items_per_page_label'] = 'Éléments par page';
  $handler->display->display_options['pager']['options']['expose']['items_per_page_options_all_label'] = '- Tout -';
  $handler->display->display_options['pager']['options']['expose']['offset_label'] = 'Décalage';
  $handler->display->display_options['pager']['options']['tags']['first'] = '« premier';
  $handler->display->display_options['pager']['options']['tags']['previous'] = '‹ précédent';
  $handler->display->display_options['pager']['options']['tags']['next'] = 'suivant ›';
  $handler->display->display_options['pager']['options']['tags']['last'] = 'dernier »';
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['hide_empty'] = TRUE;
  /* Entête: Global : Zone de texte */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['label'] = 'Recherche';
  $handler->display->display_options['header']['area']['empty'] = TRUE;
  $handler->display->display_options['header']['area']['content'] = 'Pour retrouver une association en particulier, utilisez cette <a href="/assocsf">page</a>.';
  $handler->display->display_options['header']['area']['format'] = 'filtered_html';
  /* Champ: Contenu : Titre */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Champ: Gestionnaire défectueux/manquant */
  $handler->display->display_options['fields']['field_assoc_geo3']['id'] = 'field_assoc_geo3';
  $handler->display->display_options['fields']['field_assoc_geo3']['table'] = 'field_data_field_assoc_geo3';
  $handler->display->display_options['fields']['field_assoc_geo3']['field'] = 'field_assoc_geo3';
  $handler->display->display_options['fields']['field_assoc_geo3']['label'] = 'géo3';
  $handler->display->display_options['fields']['field_assoc_geo3']['hide_alter_empty'] = FALSE;
  /* Champ: Contenu : Logo */
  $handler->display->display_options['fields']['field_assoc_image']['id'] = 'field_assoc_image';
  $handler->display->display_options['fields']['field_assoc_image']['table'] = 'field_data_field_assoc_image';
  $handler->display->display_options['fields']['field_assoc_image']['field'] = 'field_assoc_image';
  /* Champ: Gestionnaire défectueux/manquant */
  $handler->display->display_options['fields']['field_art_tag_1']['id'] = 'field_art_tag_1';
  $handler->display->display_options['fields']['field_art_tag_1']['table'] = 'field_data_field_art_tag_1';
  $handler->display->display_options['fields']['field_art_tag_1']['field'] = 'field_art_tag_1';
  $handler->display->display_options['fields']['field_art_tag_1']['label'] = '';
  $handler->display->display_options['fields']['field_art_tag_1']['element_label_colon'] = FALSE;
  /* Critère de tri: Contenu : Date de publication */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Critère de filtrage: Contenu : Publié */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 0;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Critère de filtrage: Contenu : Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'assoc' => 'assoc',
  );
  /* Critère de filtrage: Contenu : Membre de la fédération (field_assoc_adhesion) */
  $handler->display->display_options['filters']['field_assoc_adhesion_value']['id'] = 'field_assoc_adhesion_value';
  $handler->display->display_options['filters']['field_assoc_adhesion_value']['table'] = 'field_data_field_assoc_adhesion';
  $handler->display->display_options['filters']['field_assoc_adhesion_value']['field'] = 'field_assoc_adhesion_value';
  $handler->display->display_options['filters']['field_assoc_adhesion_value']['value'] = array(
    0 => '0',
    1 => '1',
  );
  $handler->display->display_options['filters']['field_assoc_adhesion_value']['expose']['operator_id'] = 'field_assoc_adhesion_value_op';
  $handler->display->display_options['filters']['field_assoc_adhesion_value']['expose']['label'] = 'Adhérent à Mirabel-LNE';
  $handler->display->display_options['filters']['field_assoc_adhesion_value']['expose']['operator'] = 'field_assoc_adhesion_value_op';
  $handler->display->display_options['filters']['field_assoc_adhesion_value']['expose']['identifier'] = 'field_assoc_adhesion_value';
  /* Critère de filtrage: Contenu : Mots-clef autour de l&#039;environnement (field_tags) */
  $handler->display->display_options['filters']['field_tags_tid']['id'] = 'field_tags_tid';
  $handler->display->display_options['filters']['field_tags_tid']['table'] = 'field_data_field_tags';
  $handler->display->display_options['filters']['field_tags_tid']['field'] = 'field_tags_tid';
  $handler->display->display_options['filters']['field_tags_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_tags_tid']['expose']['operator_id'] = 'field_tags_tid_op';
  $handler->display->display_options['filters']['field_tags_tid']['expose']['label'] = 'Mots-clef autour de l&#039;environnement (field_tags)';
  $handler->display->display_options['filters']['field_tags_tid']['expose']['operator'] = 'field_tags_tid_op';
  $handler->display->display_options['filters']['field_tags_tid']['expose']['identifier'] = 'field_tags_tid';
  $handler->display->display_options['filters']['field_tags_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_tags_tid']['vocabulary'] = 'tags';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['css_class'] = FALSE;
  $handler->display->display_options['css_class'] = 'mirabel-assoc-view';
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['defaults']['exposed_form'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'input_required';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Appliquer';
  $handler->display->display_options['exposed_form']['options']['reset_button_label'] = 'Réinitialiser';
  $handler->display->display_options['exposed_form']['options']['exposed_sorts_label'] = 'Trier par';
  $handler->display->display_options['exposed_form']['options']['text_input_required'] = 'Sélectionnez n\'importe quel filtre et cliquez sur Appliquer pour voir les résultats';
  $handler->display->display_options['exposed_form']['options']['text_input_required_format'] = 'filtered_html';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Champ: Contenu : Titre */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Champ: Contenu : Logo */
  $handler->display->display_options['fields']['field_assoc_image']['id'] = 'field_assoc_image';
  $handler->display->display_options['fields']['field_assoc_image']['table'] = 'field_data_field_assoc_image';
  $handler->display->display_options['fields']['field_assoc_image']['field'] = 'field_assoc_image';
  $handler->display->display_options['fields']['field_assoc_image']['label'] = '';
  $handler->display->display_options['fields']['field_assoc_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_assoc_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_assoc_image']['settings'] = array(
    'image_style' => 'thumbnail',
    'image_link' => '',
  );
  /* Champ: Contenu : Mots-clef autour de l'environnement */
  $handler->display->display_options['fields']['field_tags']['id'] = 'field_tags';
  $handler->display->display_options['fields']['field_tags']['table'] = 'field_data_field_tags';
  $handler->display->display_options['fields']['field_tags']['field'] = 'field_tags';
  $handler->display->display_options['fields']['field_tags']['label'] = 'Thématiques';
  $handler->display->display_options['fields']['field_tags']['delta_offset'] = '0';
  /* Champ: Contenu : Site Web */
  $handler->display->display_options['fields']['field_web']['id'] = 'field_web';
  $handler->display->display_options['fields']['field_web']['table'] = 'field_data_field_web';
  $handler->display->display_options['fields']['field_web']['field'] = 'field_web';
  $handler->display->display_options['fields']['field_web']['label'] = '';
  $handler->display->display_options['fields']['field_web']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_web']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_web']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_web']['delta_offset'] = '0';
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  /* Critère de tri: Contenu : Titre */
  $handler->display->display_options['sorts']['title']['id'] = 'title';
  $handler->display->display_options['sorts']['title']['table'] = 'node';
  $handler->display->display_options['sorts']['title']['field'] = 'title';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Critère de filtrage: Contenu : Publié */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 0;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Critère de filtrage: Contenu : Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'assoc' => 'assoc',
  );
  /* Critère de filtrage: Contenu : Membre de la fédération (field_assoc_adhesion) */
  $handler->display->display_options['filters']['field_assoc_adhesion_value']['id'] = 'field_assoc_adhesion_value';
  $handler->display->display_options['filters']['field_assoc_adhesion_value']['table'] = 'field_data_field_assoc_adhesion';
  $handler->display->display_options['filters']['field_assoc_adhesion_value']['field'] = 'field_assoc_adhesion_value';
  $handler->display->display_options['filters']['field_assoc_adhesion_value']['value'] = array(
    1 => '1',
  );
  $handler->display->display_options['filters']['field_assoc_adhesion_value']['expose']['operator_id'] = 'field_assoc_adhesion_value_op';
  $handler->display->display_options['filters']['field_assoc_adhesion_value']['expose']['label'] = 'Adhérent à Mirabel-LNE';
  $handler->display->display_options['filters']['field_assoc_adhesion_value']['expose']['operator'] = 'field_assoc_adhesion_value_op';
  $handler->display->display_options['filters']['field_assoc_adhesion_value']['expose']['identifier'] = 'field_assoc_adhesion_value';
  $handler->display->display_options['path'] = 'assocs';
  $handler->display->display_options['menu']['title'] = 'associations adhérentes';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $handler->display->display_options['menu']['context'] = 0;

  /* Display: Search Page */
  $handler = $view->new_display('page', 'Search Page', 'asso_search_page');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Associations de protection de l\'environnement en Lorraine';
  $handler->display->display_options['defaults']['css_class'] = FALSE;
  $handler->display->display_options['css_class'] = 'mirabel-assoc-view';
  $handler->display->display_options['display_description'] = 'Avec filtres exposés pour l\'adhésion et les mots-clef';
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['defaults']['exposed_form'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'input_required';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Appliquer';
  $handler->display->display_options['exposed_form']['options']['reset_button_label'] = 'Réinitialiser';
  $handler->display->display_options['exposed_form']['options']['exposed_sorts_label'] = 'Trier par';
  $handler->display->display_options['exposed_form']['options']['text_input_required'] = 'Sélectionnez n\'importe quel filtre et cliquez sur Appliquer pour voir les résultats';
  $handler->display->display_options['exposed_form']['options']['text_input_required_format'] = 'filtered_html';
  $handler->display->display_options['defaults']['header'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Champ: Contenu : Titre */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Champ: Contenu : Logo */
  $handler->display->display_options['fields']['field_assoc_image']['id'] = 'field_assoc_image';
  $handler->display->display_options['fields']['field_assoc_image']['table'] = 'field_data_field_assoc_image';
  $handler->display->display_options['fields']['field_assoc_image']['field'] = 'field_assoc_image';
  $handler->display->display_options['fields']['field_assoc_image']['label'] = '';
  $handler->display->display_options['fields']['field_assoc_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_assoc_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_assoc_image']['settings'] = array(
    'image_style' => 'thumbnail',
    'image_link' => '',
  );
  /* Champ: Contenu : Mots-clef autour de l'environnement */
  $handler->display->display_options['fields']['field_tags']['id'] = 'field_tags';
  $handler->display->display_options['fields']['field_tags']['table'] = 'field_data_field_tags';
  $handler->display->display_options['fields']['field_tags']['field'] = 'field_tags';
  $handler->display->display_options['fields']['field_tags']['label'] = 'Thématiques';
  $handler->display->display_options['fields']['field_tags']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_tags']['delta_offset'] = '0';
  /* Champ: Contenu : Site Web */
  $handler->display->display_options['fields']['field_web']['id'] = 'field_web';
  $handler->display->display_options['fields']['field_web']['table'] = 'field_data_field_web';
  $handler->display->display_options['fields']['field_web']['field'] = 'field_web';
  $handler->display->display_options['fields']['field_web']['label'] = '';
  $handler->display->display_options['fields']['field_web']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_web']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_web']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_web']['delta_offset'] = '0';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Critère de filtrage: Contenu : Publié */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 0;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Critère de filtrage: Contenu : Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'assoc' => 'assoc',
  );
  /* Critère de filtrage: Contenu : Membre de la fédération (field_assoc_adhesion) */
  $handler->display->display_options['filters']['field_assoc_adhesion_value']['id'] = 'field_assoc_adhesion_value';
  $handler->display->display_options['filters']['field_assoc_adhesion_value']['table'] = 'field_data_field_assoc_adhesion';
  $handler->display->display_options['filters']['field_assoc_adhesion_value']['field'] = 'field_assoc_adhesion_value';
  $handler->display->display_options['filters']['field_assoc_adhesion_value']['value'] = array(
    0 => '0',
    1 => '1',
  );
  $handler->display->display_options['filters']['field_assoc_adhesion_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_assoc_adhesion_value']['expose']['operator_id'] = 'field_assoc_adhesion_value_op';
  $handler->display->display_options['filters']['field_assoc_adhesion_value']['expose']['label'] = 'Adhésion';
  $handler->display->display_options['filters']['field_assoc_adhesion_value']['expose']['operator'] = 'field_assoc_adhesion_value_op';
  $handler->display->display_options['filters']['field_assoc_adhesion_value']['expose']['identifier'] = 'field_assoc_adhesion_value';
  /* Critère de filtrage: Contenu : Mots-clef autour de l&#039;environnement (field_tags) */
  $handler->display->display_options['filters']['field_tags_tid']['id'] = 'field_tags_tid';
  $handler->display->display_options['filters']['field_tags_tid']['table'] = 'field_data_field_tags';
  $handler->display->display_options['filters']['field_tags_tid']['field'] = 'field_tags_tid';
  $handler->display->display_options['filters']['field_tags_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_tags_tid']['expose']['operator_id'] = 'field_tags_tid_op';
  $handler->display->display_options['filters']['field_tags_tid']['expose']['label'] = 'Mots-clef autour de l&#039;environnement';
  $handler->display->display_options['filters']['field_tags_tid']['expose']['operator'] = 'field_tags_tid_op';
  $handler->display->display_options['filters']['field_tags_tid']['expose']['identifier'] = 'field_tags_tid';
  $handler->display->display_options['filters']['field_tags_tid']['expose']['multiple'] = TRUE;
  $handler->display->display_options['filters']['field_tags_tid']['reduce_duplicates'] = TRUE;
  $handler->display->display_options['filters']['field_tags_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_tags_tid']['vocabulary'] = 'tags';
  $handler->display->display_options['path'] = 'assocsf';
  $handler->display->display_options['menu']['title'] = 'associations adhérentes';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $handler->display->display_options['menu']['context'] = 0;
  $translatables['vassoc'] = array(
    t('Master'),
    t('Associations adhérentes au réseau Mirabel-LNE'),
    t('plus'),
    t('Appliquer'),
    t('Réinitialiser'),
    t('Trier par'),
    t('Asc'),
    t('Desc'),
    t('Éléments par page'),
    t('- Tout -'),
    t('Décalage'),
    t('« premier'),
    t('‹ précédent'),
    t('suivant ›'),
    t('dernier »'),
    t('Recherche'),
    t('Pour retrouver une association en particulier, utilisez cette <a href="/assocsf">page</a>.'),
    t('géo3'),
    t('Logo'),
    t('Adhérent à Mirabel-LNE'),
    t('Mots-clef autour de l&#039;environnement (field_tags)'),
    t('Page'),
    t('Sélectionnez n\'importe quel filtre et cliquez sur Appliquer pour voir les résultats'),
    t('Thématiques'),
    t('Search Page'),
    t('Associations de protection de l\'environnement en Lorraine'),
    t('Avec filtres exposés pour l\'adhésion et les mots-clef'),
    t('Adhésion'),
    t('Mots-clef autour de l&#039;environnement'),
  );
  $export['vassoc'] = $view;

  return $export;
}
