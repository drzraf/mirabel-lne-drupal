<?php

// annuaire => association
  
function mirabel_batch_index_remaining() {
  $batch = array(
		 'operations' => array(
				       array('mirabel_batch_print_nodes', array()),
				       // array('mirabel_batch_insert_nodes', array()),
				       ),
		 'finished' => 'mirabel_batch_index_finished',
		 'title' => t('Indexing'),
		 'init_message' => t('Preparing to submit content to Solr for indexing...'),
		 'progress_message' => t('Submitting content to Solr...'),
		 'error_message' => t('Solr indexing has encountered an error.'),
		 'file' => drupal_get_path('module', 'apachesolr') . '/apachesolr.admin.inc',
		 );
  batch_set($batch);
}
  
/**
 * Batch Operation Callback
 */
function mirabel_batch_index_nodes(&$context) {
  if (empty($context['sandbox'])) {
    try {
      $res = mysql_query('SELECT * FROM annuaire');
      if (!$res)
	throw new Exception(t("Can't grab any record in 'annuaire."));
    }
    catch (Exception $e) {
      watchdog('Mysql', $e->getMessage(), NULL, WATCHDOG_ERROR);
      return FALSE;
    }

    $context['sandbox']['progress'] = 0;
    $context['sandbox']['max'] = mysql_num_rows($res);
  }

  $context['sandbox']['progress'] += count($rows);
  $context['message'] = t('Indexed @current of @total nodes', array('@current' => $context['sandbox']['progress'], '@total' => $context['sandbox']['max']));

  // Inform the batch engine that we are not finished, and provide an
  // estimation of the completion level we reached.
  $context['finished'] = empty($context['sandbox']['max']) ? 1 : $context['sandbox']['progress'] / $context['sandbox']['max'];

  // Put the total into the results section when we're finished so we can
  // show it to the admin.
  if ($context['finished']) {
    $context['results']['count'] = $context['sandbox']['progress'];
  }
}

/**
 * Batch 'finished' callback
 */
function mirabel_batch_index_finished($success, $results, $operations) {
  $message = format_plural($results['count'], '1 item processed successfully.', '@count items successfully processed.');
  if ($success) {
    $type = 'status';
  }
  else {
    // An error occurred, $operations contains the operations that remained
    // unprocessed.
    $error_operation = reset($operations);
    $message .= ' '. t('An error occurred while processing @num with arguments :', array('@num' => $error_operation[0])) . print_r($error_operation[0], TRUE);
    $type = 'error';
  }
  drupal_set_message($message, $type);
}
