<?php

function mirabel_data($data_args) {
  return array('a');
}

function mirabel_operation_a($args) {
  var_dump('plop A', $args);
}

/**
 * Set and process a batch job with drush.
 *
 * @param $batch_args
 *     Whatever arguments you wish to pass to the batch job.
 */
function mirabel_drush_migr($batch_args) {
  // Set arguments for fetching the dataset and setting the operations. 
  // (Insert your logic here.)
  $data_args;
  $op_args;

  // Fetch the data to operate on using a function in mymodule.
  // Assume for this example that mirabel_data() returns a list of records to process.
  $dataset = mirabel_data($data_args);

  // Add operations to the batch job for all of $dataset.
  mirabel_drush_migr_operations($batch, $dataset);
   
  // Set and configure the batch.
  batch_set($batch);
  $batch =& batch_get();
  $batch['progressive'] = FALSE;
 
  // Process the batch.
  drush_backend_batch_process();
}

/**
 *  Create operations for mirabel_drush_migr.
 *  Assume we want to do two operations on each element of the dataset,
 *  mirabel_operation_a() and mirabel_operation_b().
 *
 * @param &$batch
 *     The batch job we are creating.
 * @param &$dataset
 *     The dataset to process. 
 *     (Assumed here to be an array with each row containing the data needed by mymodule.)
 * @param $op_args
 *     Whatever args you wish to pass to set the operations.
 */
function mirabel_drush_migr_operations(&$batch, &$dataset, $op_args) {

  // Set arguments for mirabel_operation_a and mirabel_operation_b. 
  // (Insert your logic here.)
  $op_a_additional_args;
  $op_b_additional_args;

  // Add the desired operations for each element in $dataset.
  foreach ($dataset as $element) {

    // These are functions in mymodule for operating on data in the format of $element:

    // mirabel_operation_a($element, $op_a_additional_args)
    $a_args = array($element, $op_a_additional_args);
    $batch['operations'][] = array('mirabel_operation_a', $a_args);

    // mirabel_operation_b($element, $op_b_additional_args)
    $b_args = array($element, $op_b_additional_args);
    //$batch['operations'][] = array('mirabel_operation_b', $b_args);   
  }
}
