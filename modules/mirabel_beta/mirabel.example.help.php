<?php
/*
  Page statique: première version
  hook_menu_alter pour charger une page php pour une URL et des
  paramètres donnés
*/

function mirabel_menu_alter(&$items) { 
  $items['monpath1'] = array(
    'title' => 'title1',
    'page callback' => '_mirabel_func1',
    'access arguments' => array('arg1'),
    'type' => MENU_SUGGESTED_ITEM,
  );
}

function _mirabel_func1($type = 'node') {
  die(__FILE__ . ' # here you can include some code');
}

/*
  Page statique: seconde version
  Le node doit exister au préalable
*/

function mirabel_preprocess_node(&$vars) {
  if($vars['title'] == 'the title of the page to override' ||
     drupal_get_path_alias($vars['uri']['path']) == 'alias of this page') {
    drupal_set_message(__FILE__ . ' # here you can include some code again');
  }
}


/*
  Page statique: troisième version
  Le node doit exister au préalable
*/

function mirabel_node_view($i) {
  if($i->nid == 33)
    drupal_set_message(__FILE__ . ' # here you can also include some code');
}

/*
  Page statique: quatrième version
  Le node doit exister au préalable
  @ se référer à sites/all/themes/mirabel/template.php
*/
// ou créer page--node--2.tpl.php


function mirabel_preprocess_node(&$vars) {
  if( isset($vars['uri']) && mirabel_is_path_aliased($vars['uri']['path'], NULL, 'map42') )
    var_dump("AAAAAAAa");
}



/*
 * http://drupal.org/project/pathfilter
 * http://drupalcode.org/project/pathfilter.git/blob/refs/heads/7.x-1.x:/pathfilter.module
 * http://drupal.org/node/106608
*/

/*
function mirabel_filter($text) {
  $text = ' ' . $text . ' ';
  $text = preg_replace_callback("/\[\[.*?]]/s", 'mirabel_token_to_markup', $text);

  return $text;
}
*/



/*
  Because:
  http://api.drupal.org/api/drupal/includes--path.inc/function/drupal_get_path_alias/7
  only returns 1 alias (see DatabaseStatementInterface::fetchAllKeyed)

  TODO: see drupal_lookup_path("alias","node/218")
*/
function mirabel_lookup_path_alias($path = '', $path_language = NULL) {
  global $language_url;
  $path_language = $path_language ? $path_language : $language_url->language;
  $args = array(
    ':system' => $path,
    ':language' => $path_language, 
    ':language_none' => LANGUAGE_NONE,
  );
  if ($path_language == LANGUAGE_NONE) {
    // Prevent PDO from complaining about a token the query doesn't use.
    unset($args[':language']);
    $result = db_query('SELECT alias FROM {url_alias} WHERE source = :system AND language = :language_none ORDER BY pid ASC', $args);
  }
  else {
    $result = db_query('SELECT alias FROM {url_alias} WHERE source = :system AND language IN (:language, :language_none) ORDER BY language ASC, pid ASC', $args);
  }
  return $result->fetchCol();
}

function mirabel_is_path_aliased($path, $language, $alias) {
  return array_search($alias, mirabel_lookup_path_alias($path, $language)) !== FALSE;
}
