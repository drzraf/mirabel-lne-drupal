<?php

/**
 * Implementation of hook_openlayers_maps().
 */
function mirabel_openlayers_maps() {
  $items = array();

  // Map with some behaviors
  $mirabel_map = new stdClass();
  $mirabel_map->api_version = 1;
  $mirabel_map->name = 'mirabel';
  $mirabel_map->title = t('Mirabel');
  $mirabel_map->description = t('Mirabel test Map.');
  $mirabel_map->data = array(
    'projection' => '4326',
    'projection' => '900913',
    'width' => 'auto',
    'default_layer' => 'osm_mapnik',
    'height' => '400px',
    'center' => array(
      'initial' => array(
        'centerpoint' => '6.57,48.71',
        'zoom' => '8'
      )
    ),
    'restrict' => array(
      'restrictextent' => 0,
      'restrictedExtent' => '',
    ),
    'options' => array(
      'displayProjection' => '4326',
      'maxExtent' => openlayers_get_extent('4326'),
    ),
    'behaviors' => array(
      'openlayers_behavior_panzoombar' => array(),
      'openlayers_behavior_layerswitcher' => array(),
      'openlayers_behavior_attribution' => array(),
      'openlayers_behavior_keyboarddefaults' => array(),
      'openlayers_behavior_navigation' => array(),
      'openlayers_behavior_fullscreen' => array(),
      'openlayers_behavior_mouseposition' => array(),
      'openlayers_behavior_dragpan' => array(),
      'openlayers_behavior_boxselect' => array(),
      'openlayers_behavior_permalink' => array(),
      'openlayers_behavior_scaleline' => array(),
      'openlayers_behavior_zoombox' => array(),
      'openlayers_behavior_zoomtomaxextent' => array(),
    ),
    'layers' => array(
      'osm_mapnik' => 'osm_mapnik',
    )
  );
  $items['mirabel'] = $mirabel_map;

  //return array( 'mirabel' => $mirabel_map );
    //'mirabel_formatter_map' => $mirabel_map
  return $items;
}
  /*
  // Create full preset array
  $widget = new stdClass();
  $widget->api_version = 1;
  $widget->name = 'mirabel_widget_map';
  $widget->title = t('Mirabel Widget Map');
  $widget->description = t('A Map Used for Mirabel Input');
  $widget->data = array(
    'projection' => '900913',
    'width' => '600px',
    'default_layer' => 'osm_mapnik',
    'height' => '400px',
    'center' => array(
      'initial' => array(
        'centerpoint' => '0,0',
        'zoom' => '2'
      )
    ),
    'options' => array(
      'maxExtent' => openlayers_get_extent('900913'),
    ),
    'behaviors' => array(
      'openlayers_behavior_panzoombar' => array(),
      'openlayers_behavior_mirabel' => array(),
      'openlayers_behavior_keyboarddefaults' => array(),
      'openlayers_behavior_navigation' => array(
        'navigation' => '',
        'zoomWheelEnabled' => FALSE,
      ),
    ),
    'layers' => array(
      'osm_mapnik',
    )
  );
  
  $formatter = new stdClass();
  $formatter->api_version = 1;
  $formatter->name = 'mirabel_formatter_map';
  $formatter->title = t('Mirabel Formatter Map');
  $formatter->description = t('A Map Used for Mirabel Output');
  $formatter->data = array(
    'projection' => '900913',
    'width' => '600px',
    'default_layer' => 'osm_mapnik',
    'height' => '400px',
    'center' => array(
      'initial' => array(
        'centerpoint' => '0,0',
        'zoom' => '2'
      )
    ),
    'options' => array(
      'maxExtent' => openlayers_get_extent('900913'),
    ),
    'behaviors' => array(
      'openlayers_behavior_panzoombar' => array(),
      'openlayers_behavior_keyboarddefaults' => array(),
      'openlayers_behavior_zoomtolayer' => array(
        'zoomtolayer' => 'mirabel_formatter',
        'point_zoom_level' => 5,
      ),
      'openlayers_behavior_navigation' => array(
        'navigation' => '',
        'zoomWheelEnabled' => FALSE,
      ),
    ),
    'layers' => array(
      'mirabel_formatter',
      'osm_mapnik',
    )
  );
  
  return array(
    'mirabel_widget_map' => $widget, 
    'mirabel_formatter_map' => $formatter, 
  );
}

/**
 * Implements hook_openlayers_behaviors().
 */
function mirabel_openlayers_behaviors() {
  return array(
    /*'openlayers_behavior_mirabel' => array(
      'title' => t('Mirabel'),
      'description' => t('Fuels the mirabel map-input form.'),
      'type' => 'layer',
      'path' => drupal_get_path('module', 'mirabel') .'/includes/behaviors',
      'file' => 'openlayers_behavior_mirabel.inc',
      'behavior' => array(
        'class' => 'openlayers_behavior_mirabel',
        'parent' => 'openlayers_behavior',
      ),
    ),*/
  );
}

/**
 * Formatter layers
 */
function mirabel_openlayers_layers() {
  $layers = array();
  /*$layer = new stdClass();
  $layer->api_version = 1;
  $layer->name = 'mirabel_formatter';
  $layer->title = 'Placeholder for Mirabel Formatter';
  $layer->description = '';
  $layer->data = array(
    'layer_type' => 'openlayers_layer_type_raw',
    'projection' => array('900913'),
    'features' => array()
  );
  $layers[$layer->name] = $layer;*/
  return $layers;
}


function mirabel_ctools_plugin_api($module, $api) {
  if ($module == "mirabel") {
    switch ($api) {
    case 'openlayers_maps':
      return array('version' => 1);

    case 'openlayers_layers':
      return array('version' => 1);

    case 'openlayers_styles':
      return array('version' => 1);

    }
  }
}
