<?php
/**
 * @file
 * mirabel_features.features.inc
 */

/**
 * Implements hook_views_api().
 */
function mirabel_features_views_api() {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function mirabel_features_node_info() {
  $items = array(
    'article' => array(
      'name' => t('Article'),
      'base' => 'node_content',
      'description' => t('Utilisez les <em>articles</em> pour des contenus possédant une temporalité tels que des actualités, des communiqués de presse ou des billets de blog.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'assoc' => array(
      'name' => t('Association'),
      'base' => 'node_content',
      'description' => t('Une association, ses thématiques, actions, sa localisation, son site, ...'),
      'has_title' => '1',
      'title_label' => t('Intitulé'),
      'help' => '',
    ),
    'book' => array(
      'name' => t('Book page'),
      'base' => 'node_content',
      'description' => t('<em>Books</em> have a built-in hierarchical navigation. Use for handbooks or tutorials.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
