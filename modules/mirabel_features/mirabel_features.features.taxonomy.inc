<?php
/**
 * @file
 * mirabel_features.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function mirabel_features_taxonomy_default_vocabularies() {
  return array(
    'alltags' => array(
      'name' => 'Autres mots-clef',
      'machine_name' => 'alltags',
      'description' => 'Autres mots-clef libres',
      'hierarchy' => '1',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
    'catun' => array(
      'name' => 'Catégorie 1',
      'machine_name' => 'catun',
      'description' => 'Mots-clef ne faisant pas spécifiquement partie de l\'écologie',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '-9',
    ),
    'format' => array(
      'name' => 'Format',
      'machine_name' => 'format',
      'description' => 'Pseudo-catégorie de contenu',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
    'tags' => array(
      'name' => 'Mots-clef autour de l\'écologie',
      'machine_name' => 'tags',
      'description' => '',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '-10',
    ),
    'themetags' => array(
      'name' => 'Thématiques autour de l\'environnement',
      'machine_name' => 'themetags',
      'description' => '',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
  );
}
