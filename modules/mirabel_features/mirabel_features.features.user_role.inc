<?php
/**
 * @file
 * mirabel_features.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function mirabel_features_user_default_roles() {
  $roles = array();

  // Exported role: adherent.
  $roles['adherent'] = array(
    'name' => 'adherent',
    'weight' => '2',
  );

  return $roles;
}
