var map;

function init(cli) {
    cli = cli == true ? true : false;

    var layer, vectorLayer, control;
    map = new OpenLayers.Map('map', {
	projection: new OpenLayers.Projection("EPSG:900913"),
        displayProjection: new OpenLayers.Projection("EPSG:4326"),
        controls: [
            new OpenLayers.Control.PanZoom(),
	    new OpenLayers.Control.MousePosition(),
	    new OpenLayers.Control.Attribution(),
            new OpenLayers.Control.Navigation( { zoomBoxEnabled: false } )
        ]
    });

    layer = new OpenLayers.Layer.OSM("osm", "", {
	//minZoomLevel: 7, // TODO
	maxZoomLevel: 12,
	numZoomLevels: 13 // TODO: 6
    });

    var defStyle = { strokeOpacity: 0.5, fillOpacity: 0.05 };
    var sty = OpenLayers.Util.applyDefaults(defStyle, OpenLayers.Feature.Vector.style["default"]);
    var tdefStyle = { fillOpacity: 0.5 };
    var tsty = OpenLayers.Util.applyDefaults(tdefStyle, OpenLayers.Feature.Vector.style["temporary"]);
    var sm = new OpenLayers.StyleMap({ 'default': sty, 'temporary': tsty });
    vectorLayer = new OpenLayers.Layer.Vector("GeoJSON", {
	projection: new OpenLayers.Projection("EPSG:4326"),
        strategies: [new OpenLayers.Strategy.Fixed()],
        protocol: new OpenLayers.Protocol.HTTP({
	    url:'/' + Drupal.settings.mirabel_map.path +
		'/cantons-4326.json',
            format: new OpenLayers.Format.GeoJSON()
        }),
	styleMap: sm
    });

    depLayer = new OpenLayers.Layer.Vector("dep", {
	projection: new OpenLayers.Projection("EPSG:4326"),
        strategies: [new OpenLayers.Strategy.Fixed()],
        protocol: new OpenLayers.Protocol.HTTP({
	    url:'/' + Drupal.settings.mirabel_map.path +
		'/departements-4326.json',
            format: new OpenLayers.Format.GeoJSON()
        }),
	style: {
	    strokeColor: "black",
	    strokeOpacity: 1,
	    strokeWidth: 1,
	    fillOpacity: 0
	}
    });
    
    /* TODO:
       instance selectControlClicks SelectFeature
       then use  selectControlClicks.events.on() conditionnally
       (if possible)
    */
    selectControlClicksConfig = {
        multipleKey: "shiftKey",
        toggleKey: "ctrlKey",
	clickout: true
    }

    /**
     * admin widget: update the pseudo-form, see updateCantonsForm()
     **/
    if(! cli) {
	selectControlClicksConfig.onSelect = function(feature) {
	    Drupal.mirabel.updateCantonsForm(vectorLayer.selectedFeatures);
	};
	selectControlClicksConfig.onUnselect = function(feature) {
	    Drupal.mirabel.updateCantonsForm(vectorLayer.selectedFeatures);
	}
    }

    /**
     * client widget: create popups
     **/
    else {
        var onPopupClose = function(evt) {
            selectControlClicks.unselect(selectedFeature);
        };

	selectControlClicksConfig.onSelect = function(feature) {
	    if(!feature)
		return;
            selectedFeature = feature;
            popup = new OpenLayers.Popup.Anchored("popup",
						     feature.geometry.getBounds().getCenterLonLat(),
						     null,
						     Drupal.mirabel.createPopup(feature).html(),
						     null, true, onPopupClose);
	    popup.autoSize = true;
            feature.popup = popup;
            map.addPopup(popup);
        };

	selectControlClicksConfig.onUnselect = function(feature) {
            map.removePopup(feature.popup);
            feature.popup.destroy();
            feature.popup = null;
        };
    }

    // really instanciate the SelectFeature, now, from selectControlClicksConfig
    var selectControlClicks = new OpenLayers.Control.SelectFeature(vectorLayer,
								   selectControlClicksConfig);

    var selectControlHover = new OpenLayers.Control.SelectFeature(vectorLayer, {
	hover: true,
	highlightOnly: true,
	renderIntent: "temporary",
    });

    if(cli && false) {
	var fn = function(obj) {
	    if(obj.type == "featureunhighlighted")
		return;
	};

	selectControlHover.events.on({
	    "featurehighlighted": fn,
	    "featureunhighlighted": fn
	});
    }
    if(! cli) {
	selectControlHover.events.on({
	    'featurehighlighted': function(feature) {
		jQuery("#mapinfobox").text(feature.feature.attributes.NOM_CHF);
	    }
	});
    }

    selectControlClicks.handlers.feature.stopDown = false;
    selectControlHover.handlers.feature.stopDown = false;

    map.addControl(selectControlHover);
    selectControlHover.activate();

    map.addControl(selectControlClicks);
    selectControlClicks.activate();

    /*vectorLayer.events.on({
	"featureselected": function(e) {  drag.activate(); }
	'hoverfeature': function(feature) { $('counter').innerHTML = this.selectedFeatures.length; }
    });*/

    /*
    select = new OpenLayers.Layer.Vector("Selection", {
	styleMap: new OpenLayers.Style(OpenLayers.Feature.Vector.style["select"])
    });
    */

    map.addLayers([layer, depLayer, vectorLayer]);
    
/*
    control = new OpenLayers.Control.SelectFeature(
	vectorLayer,
	{
            //box: true,
            hover: true,
	    click: true,
            multipleKey: "shiftKey",
            toggleKey: "ctrlKey",
	}
    );
    map.addControl(control);
    control.activate();
*/

    /*
    var drag = new OpenLayers.Control.DragFeature(layer);
    map.addControl(drag);
    drag.activate();
    drag.onComplete = function(f) {
	drag.deactivate();
	selectControlClicks.activate();
    };
    */

    vectorLayer.events.register(
	'featuresadded',
	map,
	function() {
	    var extent = vectorLayer.getDataExtent();
	    this.zoomToExtent(extent);
	    this.setOptions({restrictedExtent: extent});
	    if(cli)
		Drupal.mirabel.initcantonsfeatures();
	});
}

(function ($) {
    Drupal.mirabel = Drupal.mirabel || {};

    /**
     * admin widget only:
     * initialize the <select> with the
     * list of associations
     */
    Drupal.mirabel.initselect = function() {
	var aselect = $("#map-assolist");
	var assocs = Drupal.settings.mirabel;
	for(var i in assocs) {
	    aoption = $("<option />")
		.attr('value', assocs[i].nid)
		.text(assocs[i].title);
	    if(assocs[i].adhesion == 1)
		aoption.css('font-weight', 'bold');
	    aselect.append(aoption);
	}
    };

    /**
     * admin widget only:
     * when another <select> option is selected
     * update the selected features on the map
     * accordingly
     */
    Drupal.mirabel.changeasso = function () {
	var nid = $("#map-assolist").val();
	var asso = Drupal.settings.mirabel[nid];

	if(!asso) {
	    $("#map-assodesc").empty();
	    $("#map-assoville").empty();
	    $("#map-assoloc").empty();
	    $("#map-assoimg").empty();
	    $("#edit-cantons").val('');
	    return;
	}

	$("#map-assodesc").html(
	    $("<a/>").attr("href", asso.path).text(asso.title)
	);

	if(asso.adhesion == 1)
	    $("#map-assodesc").css('font-weight', 'bold');

	if(asso.ville)
	    $("#map-assoville")
	    .text('(' + asso.ville + ')');

	$("#map-assoloc").text(asso.deps.join());

	// clean cantons field
	$("#edit-cantons").val('');
	if(asso.imgpath) {
	    $("#map-assoimg").html(
		$("<img />")
		    .attr('src', asso.imgpath)
		    .addClass("assomap")
	    );
	} else {
	    $("#map-assoimg").empty();
	}

	var features;
	var vLayer = map.getLayersByName("GeoJSON")[0];
	var vControl = map.getControlsBy("hover", false)[0];

	vControl.unselectAll();
	if(!asso.canton) {
	    Drupal.mirabel.updateCantonsForm([]);
	    return;
	}
	for (i in asso.canton) {
	    features = vLayer.getFeaturesByAttribute("ID_GEOFLA", asso.canton[i]);
	    vControl.select(features[0]);
	}
    }

    /**
     * admin widget only:
     * called when an OL feature is (un)selected
     * update the pseudo-form (including the hidden field)
     */
    Drupal.mirabel.updateCantonsForm = function (features) {
	$("#txtedit-cantons").empty();
	$("#edit-cantons").val('');
	if(features.length == 0 ||
	   ! $("#map-assodesc").text())
	    return;

	var txt = new Array(), ids = new Array();
	for (i in features) {
	    txt.push(features[i].data.NOM_CHF +
		     ' (' + features[i].data.ID_GEOFLA + ')');
	    ids.push(features[i].data.ID_GEOFLA);
	}
	$("#edit-cantons").val(ids.join());

	$("#txtedit-cantons").empty();
	ul = $('<ul/>').appendTo("#txtedit-cantons");
	jQuery.each(txt, function(i, v) {
	    ul.append("<li>" + v + "</li>");
	});
    }

    /**
     * admin widget only:
     * ajax callback to the admin backend
     * to update association admarea
     */
    Drupal.mirabel.updateCantons = function () {
	var values = $("#edit-cantons").val();
	var nid = $("#map-assolist").val();

	// commented: also allow removing all features
	// if(! values) return false;
	$.get(
	    '/admin/assoadd/' + $("#map-assolist").val(),
	    { canton: values },
	    function(data) {
		if(data == false) {
		    $("#map-updstatus").text("erreur").css('color', 'red').delay(3000).fadeOut(1500);
		    //console.error("err:", data);
		}
		else {
		    Drupal.settings.mirabel[nid].canton = data;
		    $("#map-updstatus").text("OK").css('color', 'green').delay(3000).fadeOut(1500);
		}
/*	    }).error(function() {
		$("#map-updstatus").text("erreur").css('color', 'red').delay(3000).fadeOut(1500); // jquery > 1.4 only */
	    });
    }

    /**
     * build a geofla-indexed list
     * from the nid-indexed list
     */
    Drupal.mirabel.initcantons = function() {
	var asso, i, j, geofla;

	arr = Drupal.settings.mirabel_map.cantons;
	for(i in Drupal.settings.mirabel) {
	    asso = Drupal.settings.mirabel[i];
	    for (j in asso.canton) {
		geofla = asso.canton[j];
		if(! arr[geofla])
		    arr[geofla] = [];
		arr[geofla].push(asso.nid);
	    }
	}
    }

    /**
     * additional feature's attribute
     * specifying that at least one association
     * is associated to this feature
     */
    Drupal.mirabel.initcantonsfeatures = function() {
	var i, j, geofla, lookup;

	// highlight feature whose geofla is associated
	// with associations
	lookup = {
	    0: {fillOpacity: 0.05},
	    1: {
		fillOpacity: 0.3,
		strokeColor: "black",
		fillColor: "red"
	    }
	}
	vLayer = map.getLayersByName("GeoJSON")[0];
	vLayer.styleMap.addUniqueValueRules("default", "fill", lookup);

	for (i in vLayer.features) {
	    geofla = vLayer.features[i].attributes.ID_GEOFLA;
	    j = Drupal.settings.mirabel_map.cantons[geofla] ? 1 : 0;
	    vLayer.features[i].attributes.fill = j;
	}
	vLayer.redraw();
    }

    /**
     * for the client widget:
     * build the popup about the association in
     * a given feature and return the html() to OL
     */
    Drupal.mirabel.createPopup = function(feature) {
	var sett, geofla, nids, div, ul, li, a;
	div = $("<div/>").addClass("plop");
	ul = $('<ul/>').appendTo(div);
	geofla = feature.attributes["ID_GEOFLA"];
	// here we use the alternate list
	// built during initialization in initcantons()
	nids = Drupal.settings.mirabel_map.cantons[geofla];
	$.each(nids, function(i, v) {
	    sett = Drupal.settings.mirabel[v];
	    li = $("<li/>");
	    if(sett.sigle)
		li.append(sett.sigle + ' : ');
	    a = $("<a/>")
		.text(sett.title)
		.attr("href", sett.path);
	    li.append(a);

	    if(sett.url) {
		var url = $("<a/>")
		    .text("(site web)")
		    .attr("href", sett.url)
		    .css("font-style", "italic");
		li.append(' ').append(url);
	    }

	    ul.append(li);
	});
	return div;
    }

})(jQuery);
