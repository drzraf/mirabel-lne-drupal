<?php

// http://www.webtoolkit.info/php-random-password-generator.html
function generatePassword($length=9, $strength=0) {
  $vowels = 'aeuy';
  $consonants = 'bdghjmnpqrstvz';
  if ($strength & 1) {
    $consonants .= 'BDGHJLMNPQRSTVWXZ';
  }
  if ($strength & 2) {
    $vowels .= "AEUY";
  }
  if ($strength & 4) {
    $consonants .= '23456789';
  }
  if ($strength & 8) {
    $consonants .= '@#$%';
  }
 
  $password = '';
  $alt = time() % 2;
  for ($i = 0; $i < $length; $i++) {
    if ($alt == 1) {
      $password .= $consonants[(rand() % strlen($consonants))];
      $alt = 0;
    } else {
      $password .= $vowels[(rand() % strlen($vowels))];
      $alt = 1;
    }
  }
  return $password;
}

/*
//TESTING:
$user = new stdClass();
$user->name = "testuser";
$edit["pass"] = "your_fresh_pass";
$user->mail = "raphael.droz+test@gmail.com";
$user->status = 1;
user_save($user, $edit);
mirabel_notify($user, $edit["pass"]);
*/

function _mirabel_asso_clean_sigle($sigle) {
  if($sigle == 'CSL (54 et 55)')
    $sigle = 'csl5455';
  elseif($sigle == 'MIRABEL-LNE')
    $sigle = 'mirabel';
  elseif($sigle == 'GECNAL du Warndt')
    $sigle = 'gecnal';
  elseif($sigle == 'AT')
    $sigle = 'at55';

  $sigle = strtr(strtolower($sigle), '()', '  ');
  $sigle = preg_replace('/(\s|\.)+/', '', $sigle);
  return $sigle;
}


function _mirabel_create_user_from_sigle($sigle, $email) {
  $user =  new stdClass();
  $user->name = $sigle;
  //$user->mail = $email;
  $user->mail = 'root@localhost';
  $user->status = 1;

  // TODO: $user->pass for dry-run only
  $user->pass = $edit['pass'] = generatePassword(7, 2);
  //user_save($user, $edit);

  if(! $user) {
    drush_log('error creating user ' . $user->name, 'warning');
    return;
  }
  echo "\t user created: " . $user->name . " / " . $user->pass . "\t(" . $user->mail . ")\n";
  //mirabel_notify($user, $edit["pass"]);
  return $user;

}

function _mirabel_asso_attach_image($in) {
  if($in == 'logos/null.gif')
    return NULL;

  $file =  new stdClass();
  $file->uri = BASEPATH . '/annuaire/' . $in;
  $file->{OLDPATH_FILEFIELD}[LANGUAGE_NONE][0]['value'] = '/annuaire/' . $in;
  $a = file_copy($file, MIRABEL_DESTDIR . '/logos-asso/' . basename($in), FILE_EXISTS_REPLACE);
  if($a) {
    $a->filemime = file_get_mimetype($a->uri);
    return (array)$a;
  }
  return NULL;
}

// to DELETE all associations from DB with
// drush @beta ev "require_once('$(pwd)/asso-utils.php');_mirabel_delete_assoc();"
function _mirabel_delete_assoc() {
  $query = new EntityFieldQuery();
  $result = $query
    ->entityCondition('entity_type', 'node')
    ->propertyCondition('type', 'assoc')
    ->execute();
  if(!$result)
    return;
  node_delete_multiple(array_keys($result['node']));
}


// find NULL images
function _mirabel_find_null_images() {
  $query = new EntityFieldQuery;
  $result = $query
    ->entityCondition('entity_type', 'file')
    ->entityCondition('bundle', 'image')
    ->propertyCondition('uri', MIRABEL_DESTDIR . '/logos-asso/null.gif')
    ->deleted(FALSE)
    ->range(0,1)
    ->execute();

  if(!$result)
    return;

  $null_image = intval(current(current($result))->fid);
  $query = new EntityFieldQuery;
  $result = $query
    ->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'assoc')
    ->fieldCondition('field_assoc_image', 'fid', $null_image)
    ->deleted(FALSE)
    ->range(0,10)
    ->execute();
    
  //var_dump($result);
  var_dump(entity_extract_ids("node", current($result)));
}

function _mirabel_assoc_cp_to_dep($cp) {
  if(preg_match('/54|meurthe/i', $cp))
    return 'Meurthe-et-Moselle';
  elseif(preg_match('/88|vosges/i', $cp))
    return 'Vosges';
  elseif(preg_match('/55|meuse/i', $cp))
    return 'Meuse';
  elseif(preg_match('/57|moselle/i', $cp))
    return 'Moselle';
  else
    return 'Lorraine';
}
