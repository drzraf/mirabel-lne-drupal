<?php

global $_mirabel_charset_override;

$_mirabel_charset_override = array(
  '/siteweb/dossiers/formation.php' => 'windows-1252',
  '/themes/eau/dce/2008/dossiers/contexte.htm' => 'windows-1252',
  '/themes/eau/dce/2008/dossiers/presse.htm' => 'windows-1252',
  '/themes/eau/dce/2008/dossiers/projet.htm' => 'windows-1252',
  '/themes/dechets/campagneprevention/ecolabel/index_bilanecolabel.htm' => 'windows-1252',
  '/themes/dechets/campagneprevention/stoprayon/index.htm' => 'windows-1252',
  '/themes/formulaire/prevention_dechets/stoppubnational.htm' => 'windows-1252',
  '/themes/eau/dce/actu/actu.htm' => 'windows-1252',
  '/gds/gds.htm' => 'windows-1252',
  '/themes/sante/prse/presentation.htm' => 'windows-1252',
  '/themes/sante/prse/index.htm' => 'iso8859-1',
  '/ulcos/ulcos.htm' => 'windows-1252',
  '/siteweb/dossiers/captage.php' => 'windows-1252',
  '/doc/download/cp/CP_eolien_LNE-CSL-COL-URCPIE_juillet04.htm' => 'windows-1252',
  '/velo.htm' => 'windows-1252',
  '/themes/elections/elections2007/accueil.htm' => 'windows-1252',
  '/themes/elections/elections2007/presse/presse_A32.htm' => 'windows-1252',
);

function _mirabel_get_charset($html, $abspath) {
  global $_mirabel_charset_override;
  $charset = array();

  if(preg_match('!^<meta .*charset=(.*?)>!im', $html, $charset))
    $charset = trim($charset[1], "/ \"");

  if(isset( $_mirabel_charset_override[$abspath] ) )
    $charset = $_mirabel_charset_override[$abspath];
 
  if( strstr($abspath, 'eolien/enquete/formationCEpicardie.htm') ||
      strstr($abspath, 'eolien/enquete/exemple_observations.htm') ||
      strstr($abspath, '/themes/elections/photos/epinal/') ||
      strstr($abspath, '/themes/transports/colloque/galerie/') )
    $charset = 'windows-1252';

  if(strstr($abspath, 'ecolabel/bilan_ht.htm') ||
     strstr($abspath, 'ecolabel/bilan_menu.htm') ||
     strstr($abspath, 'ecolabel/bilan_centre.htm')) {
    $charset = 'utf-8';
  }

  if($charset)
    return $charset;

  // error about this are handled later
  if(@htmlentities($html, ENT_QUOTES, 'utf-8', FALSE))
    return 'utf-8';
  if(htmlentities($html, ENT_QUOTES, 'iso8859-1', FALSE))
    return 'iso8859-1';

  trigger_error("unknown charset for $abspath", E_USER_ERROR);
  var_dump($html);
  die("\n");
}

function _mirabel_process_entities(&$html, $charset) {
  global $_mirabel_warnings;
  $html = htmlentities($html, ENT_QUOTES, $charset, FALSE);
  $html = html_entity_decode($html, ENT_QUOTES, 'UTF-8');
  $html = preg_replace('!<meta [^>]+>!i', "<meta http-equiv=\"content-type\" content=\"charset=utf-8\" />", $html);

  // sadly
  if(!preg_match('!^<(meta|META)!m', $html)) {
    $html = preg_replace('!<head>!i', "<head>\n<meta http-equiv=\"content-type\" content=\"charset=utf-8\" />", $html);
    drush_log("no <meta> tag!", 'status');
    //$_mirabel_warnings++;
  }
}
/*
function _mirabel_preprocess_meta_bis(&$html, $charset) {
  $html = str_replace("\r", "", $html);
  if(preg_match('!^<meta .*charset=(.*?)>!im', $html))
  $html = preg_replace("!charset=${charset}[^\" ]*!", "charset=\"utf-8\"", $html);
}
*/