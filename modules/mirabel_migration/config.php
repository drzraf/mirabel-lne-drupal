<?php

/**
   Note: if drush cries:
   db_query("UPDATE {system} SET weight = 10 WHERE name = 'mirabel_migration'")
*/

// USER GLOBALS
global
// regexp: these extensions define files
$_mirabel_file_ext_rx,
// available vocabularies to search terms from
  $_mirabel_vocs,
// dry-run mode, DB is not touched
  $_mirabel_dry_run,
// name of the field which contains fallback taxonomy
  $_mirabel_catchall_voc,
// menu name root page are referenced in
  $_mirabel_book_menu,
// dossiers
  $_mirabel_dossiers;

define('MIRABEL_DESTDIR', 'public://oldlinks');
define('HTTPURL', 'http://mirabel.lne.free.fr');
define('HTTPURL2', 'http://(www\.)?mirabel-lne.com');
// no trailing / !
define('BASEPATH', '/var/www/mirabel-lne.free.fr');
define('OLDPATH_FILEFIELD', 'compaturi');
// max level:
// DCE: 9
// /themes/elections: 17
define('MAXDEPTH', 17);

$_mirabel_file_ext_rx = '(jpg|png|bmp|gif|pdf|zip|rar|doc|gif|pps|ppt|rm|ram)';

// + omitted: "format"
$_mirabel_vocs = array(
  taxonomy_get_tree(taxonomy_vocabulary_machine_name_load('tags')->vid),
  taxonomy_get_tree(taxonomy_vocabulary_machine_name_load('catun')->vid),
);

// taxonomy where terms are inserted
$_mirabel_catchall_voc = 'alltags';


$pathdce = array( '/dce/vdo' );
$tagsdce = array(
  'eau', 'sdage', 'dce', 'phytopharmaceutique', 'phytosanitaire', 'nitrate',
  'pollution diffuse agricole', 'Rhin Meuse', 'Seine Normandie',
  'consultation', 'débat public', 'hydrographique', 'directive cadre eau');
$pathdechet = array ('/doc/outils/posters',
		     '/themes/formulaire/prevention_dechets',
		     '/themes/dechets/tri',
		     '/video');

$pathener = array ('/doc/download/cp', '/themes/_maj/dossiers', '/themes/_maj/liens');
$pathagri = array ('/doc/download/cp', '/doc/outils/posters', '/themes/_maj/dossiers', '/themes/_maj/liens', '/themes/ogm', '/themes/agri');
$pathtransp = array('/themes/transports', '/velo.htm');
$pathsm = array('/themes/transports/sm');
$pathpres = array('/doc/apne');
$pathelec = array('/themes/elections/photos/epinal');

$man_excl = array( '/themes/agri',
		   '/themes/ogm',
		   '/themes/eau',
		   '/themes/dechets',
		   '/themes/energie/eolien');
$excl = array_unique(array_filter(array_merge($man_excl, $pathtransp, $pathsm, $pathelec),
				  function($arg) { return (strpos($arg, '/themes') === 0); }));


$_mirabel_dossiers = array(
  // 0
  array( '/presentation/index.htm', array(), $pathpres ),
  array( '/themes/eau/dce/index.htm', $tagsdce, $pathdce ),  // maxdepth 9
  '/foret/foret.htm' => array('forêt'),
  '/siteweb/dossiers/captage.php' => array('eau', 'captage'),
  '/siteweb/dossiers/formation.php' => array(),
  // 5
  array( '/themes/dechets/campagneprevention/index.htm', array('déchets'), $pathdechet ),
  '/siteweb/theme/eolien.php' => array('énergie', 'éolien'),
  array( '/themes/energie/index.htm', array('énergie'), $pathener ),
  array( '/themes/agri/index.htm', array('agriculture', 'ogm'), $pathagri ),
  array( '/siteweb/dossiers/transports.php', array('transport', 'vélo'), $pathtransp),
  // 10
  array( '/siteweb/dossiers/pnse.php', array('santé'),  array('/themes/sante/prse') ),
  //'/apne/adelp/index.htm' => array('pollution', 'industrie'),
  '/ulcos/ulcos.htm' => array('co2', 'carbone', 'moselle'),
  '/gds/gds.htm' => array('gaz', 'schistes', 'énergie', 'pétrole'),
  '/siteweb/dossiers/hambregie.php' => array('gaz', 'énergie'),
  array('/siteweb/dossiers/sm.php', array('eau', 'saônne', 'moselle', 'canal'), $pathsm),
  '/siteweb/charte_fredon/index.php' => array('jardin', 'agriculture'),
  // 16
  '/siteweb/pat/index.php' => array('pat', 'bois'),
  '/fndva/' => array('urbanisation', 'plu', 'habitat'),
  '/grenelle/index.php' => array('grenelle'),
  '/grenelle/telecharger.php' => array('grenelle'),
  array('/themes/elections/elections2007/index.htm', array('démocratie', 'politique', 'élections'), $pathelec),
  array('/themes/', array(), array(), /*$excl*/)
);

$_mirabel_book_menu = 'menu-dossier';

if(function_exists('drush_get_context'))
  $_mirabel_dry_run = drush_get_context('DRUSH_SIMULATE');
else
  $_mirabel_dry_run = TRUE;


if(! $_mirabel_dry_run) {
  echo "starts in 7 seconds\n";
  wfCountDown(7);
}

function wfCountDown( $n ) {
  for ( $i = $n; $i >= 0; $i-- ) {
    if ( $i != $n ) {
      echo str_repeat( "\x08", strlen( $i + 1 ) );
    }
    echo $i;
    flush();
    if ( $i ) {
      sleep( 1 );
    }
  }
  echo "\n";
}
