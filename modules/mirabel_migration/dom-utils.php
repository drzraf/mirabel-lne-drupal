<?php

function _mirabel_dom_count_tags($dom, $tag) {
  $i = 0;
  $xmlNodes = $dom->getElementsByTagName($tag);
  foreach($xmlNodes as $xmlNode)
    $i++;
  return $i;
}

function _mirabel_preprocess_dom(&$dom, $page) {
  $body = $onload = $anchor = false;

  $body = $dom->getElementsByTagName('body')->item(0);
  if($body)
    $onload = $body->getAttribute('onload');
  if($onload)
    $anchor = _mirabel_dom_get_extract_jshref($onload);
  if($anchor) {
    $newnode = $dom->createElement('a', 'reference');
    $newnode->setAttribute('href', $anchor);
    $body->appendChild($newnode);
  }
  if($onload)
    $body->removeAttribute('onload');

  while (_mirabel_dom_count_tags($dom, 'script')) {
    $script = $dom->getElementsByTagName('script')->item(0);
    $script->parentNode->removeChild($script);
  }

  /*while (_mirabel_dom_count_tags($dom, 'frameset')) {
    $frameset = $dom->getElementsByTagName('frameset')->item(0);
    while (_mirabel_dom_count_tags($frameset, 'frame')) {
      $frame = $frameset->getElementsByTagName('frame')->item(0);
      _mirabel_preprocess_frame($dom, $frame, $page);
    }
    }*/

  while (_mirabel_dom_count_tags($dom, 'frame')) {
    $frame = $dom->getElementsByTagName('frame')->item(0);
    _mirabel_preprocess_frame($dom, $frame, $page);
  }

  while (_mirabel_dom_count_tags($dom, 'iframe')) {
    $iframe = $dom->getElementsByTagName('iframe')->item(0);
    _mirabel_preprocess_frame($dom, $iframe, $page);
  }

  $html = preg_replace("!<noframes>.*?</noframes>!mis", "", $dom->saveHTML());
  $html = preg_replace("!<(/)?frameset.*?>!i", "", $html);
  $html = preg_replace('/\s+:(active|visited|link) {.*}/', '', $html);
  $dom->loadHTML($html);
  _mirabel_preprocess_dom_map($dom, $page);
  _mirabel_preprocess_options($dom);
}

/*
 stripped by tidy before we go Dom()
function _mirabel_preprocess_dom_table($dom, $page) {
  foreach($dom->getElementsByTagName('map') as $e) {
    if( ($bg = $e->getAttribute('background')) ) {
      $e->setAttribute('style',
		       $e->setAttribute('style') . ';' .
		       'background:' . $bg );
    }
  }
}
*/

function _mirabel_preprocess_dom_map($dom, $page) {
  while (_mirabel_dom_count_tags($dom, 'map')) {
    $map = $dom->getElementsByTagName('map')->item(0);
    $div = $dom->createElement('div', '');
    foreach($map->getElementsByTagName('area') as $area) {
      $src = $txt = $area->getAttribute('href');
      $a = $dom->createElement('a', $txt);
      $a->setAttribute('href', $src);
      $span = $dom->createElement('span', '');
      $span->appendChild($a);
      $div->appendChild($span);
    }
    $map->parentNode->insertBefore($div, $map);
    $map->parentNode->removeChild($map);
  }
}

function _mirabel_preprocess_options(&$dom) {
  if(! _mirabel_dom_count_tags($dom, 'select'))
    return;

  $selnode = $dom->getElementsByTagName('select')->item(0);
  $ul = $dom->createElement('ul', '');

  $opts = $selnode->getElementsByTagName('option');
  foreach($opts as $opt) {
    $li = $dom->createElement('li', '');
    if($opt->getAttribute('value')) {
      $a = $dom->createElement('a', $opt->nodeValue);
      $a->setAttribute('href', $opt->getAttribute('value'));
    } else {
      $a = $dom->createTextNode($opt->nodeValue);
    }
    $li->appendChild($a);
    $ul->appendChild($li);
  }
  $selnode->parentNode->replaceChild($ul, $selnode);
  // try again (multiple <select> in the page ?)
  return _mirabel_preprocess_options($dom);
}

function _mirabel_preprocess_frame($dom, &$frame, $page) {
  global $_mirabel_warnings;

  $src = $subpage =  $frame->getAttribute('src');
  if(! _mirabel_frame_fs_interpolation($subpage, $page)) {
    if($src != 'http://pp.free.fr/iframe.htm') {
      drush_log($src . ' is an invalid frame destination', 'warning');
      $_mirabel_warnings++;
    }
    $frame->parentNode->removeChild($frame);
    return;
  }
  
  $abspath = _mirabel_abspath($subpage);
  if(! _mirabel_respect_root($abspath) ) {
    drush_log("frame $abspath outside root", 'warning');
    $newnode = $dom->createElement('a', 'reference');
    $newnode->setAttribute('href', $subpage);
    $frame->parentNode->replaceChild($newnode, $frame);
    $_mirabel_warnings++;
    return;
  }
  pad("frame subprocess: $src (=> $subpage)");
  $subhtml = _mirabel_migrate_get_page($subpage);
  $charset = _mirabel_get_charset($subhtml, $abspath);
  _mirabel_process_entities($subhtml, $charset);
  // unused anymore
  //_mirabel_preprocess_meta_bis($subhtml, $charset);
  //var_dump($subpage, $subhtml);
  $newdom = new DOMDocument();
  $newdom->loadHTML($subhtml);
  $newnode = $newdom->getElementsByTagName('body')->item(0);
  foreach($newnode->childNodes as $child) {
    $ourchild = $dom->importNode($child, true);
    $frame->parentNode->insertBefore($ourchild, $frame);
  }
  $frame->parentNode->removeChild($frame);
}

function _mirabel_trim_anchor($txt) {
  return preg_replace("/[[:blank:]\n]+/",
		      ' ',
		      trim(preg_replace("/[[:blank:]\n]+/s", ' ', $txt)));
}

function _mirabel_dom_get_extract_onclick($val) {
  if(preg_match('/na_open_window/', $val))
    return preg_replace("|na_open_window\('\w+', '(.*?)'.*|",
			'$1', $val);
  return NULL;
}

function _mirabel_dom_get_extract_jshref($val) {
  if(preg_match('/na_open_window/', $val)) {
    $lnk = preg_replace("|^.*na_open_window\('\w+',\s*'(.*?)'.*$|",
			'$1', str_replace('%20', ' ', $val));
    return preg_replace('|^file:(.*)|', '$1', $lnk);
  }
  if(preg_match('/GoToSld/', $val))
    return preg_replace("|.*GoToSld\('([^']+)'.*|",
			'$1', str_replace('%20', ' ', $val));
  return NULL;
}

function _mirabel_get_link_title($dom, $xmlNode) {
  $lnkdst = $xmlNode->getAttribute('href');
  $lnktxt = _mirabel_trim_anchor($xmlNode->nodeValue);

  // simple strip
  if($lnktxt && ! preg_match('/^\s*$/uD', $lnktxt))
    return $lnktxt;
  //var_dump("no simple txt");

  // dig into the <a> markup itself
  $fullnode = dom_to_array($xmlNode);
  $lnktxt = recursiveArraySearch($fullnode);
  if($lnktxt) {
    return $lnktxt;
  }
  //var_dump("no nested txt");

  /*
    if we are not trying the search the DOM for another link already,
    then try to grab another <a>
    whose href is the same but the text non-null
  */
  if(! $dom)
    return null;

  $xmlNodes = $dom->getElementsByTagName('a');
  foreach($xmlNodes as $xmlNode2) {
    if($xmlNode2->isSameNode($xmlNode))
      continue;
    if($xmlNode2->getAttribute('href') != $lnktxt)
      continue;
    //var_dump("alternative anchor found");
    $newtxt = _mirabel_get_link_title(null, $xmlNode2);
    if($newtxt)
      return $newtxt;
  }

  return NULL;
  // manual search (nested <a>'s ... no comment, it should probably not happen with
  // the PHP DOM implementation
  pad("(PARSE: manual search of <a>'s)"); 
  // not supported before php 5.3.6 (squeeze is 5.3.3)
  $html = $dom->saveHTML($xmlNode);
  preg_match('|<a\shref="' . $lnkdst . '"[^>]+>.*?</a>.*?</a>|mis', $html, $matches);
  $lnktxt = _mirabel_trim_anchor(strip_tags($matches[0]));
  if($lnktxt)
    return $lnktxt;

  //var_dump("no other link");
  return NULL;
}

function recursiveArraySearch($haystack) {
  $aIt = new RecursiveArrayIterator($haystack);
  $it = new RecursiveIteratorIterator($aIt);
  while($it->valid()) {
    if ($it->key() == '_value' AND trim(trim($it->current(), "\n\xc2\xa0"))) {
      die('a');
      return $it->current();
    }
    $it->next();
  }
  return false;
}

function dom_to_array($root)
{
  $result = array();

  if ($root->hasAttributes())
    {
      $attrs = $root->attributes;

      foreach ($attrs as $i => $attr)
	$result[$attr->name] = $attr->value;
    }

  $children = $root->childNodes;

  if(! $children)
    return $result;

  if ($children->length == 1)
    {
      $child = $children->item(0);

      if ($child->nodeType == XML_TEXT_NODE)
        {
	  $result['_value'] = $child->nodeValue;

	  if (count($result) == 1)
	    return $result['_value'];
            else
	      return $result;
        }
    }

  $group = array();

  for($i = 0; $i < $children->length; $i++)
    {
      $child = $children->item($i);

      if (!isset($result[$child->nodeName]))
	$result[$child->nodeName] = dom_to_array($child);
        else
	  {
            if (!isset($group[$child->nodeName]))
	      {
                $tmp = $result[$child->nodeName];
                $result[$child->nodeName] = array($tmp);
                $group[$child->nodeName] = 1;
	      }

            $result[$child->nodeName][] = dom_to_array($child);
	  }
    }

  return $result;
}

// unused, debugging purpose
/*
function mydeep($node, &$tab) {
  $found = FALSE;
  $xmlNodes = $node->getElementsByTagName('a');
  foreach($xmlNodes as $xmlNode) {
    mydeep($xmlNode, $tab);
    $tab['href'][] = $xmlNode->getAttribute('href');
    $tab['txt'][] = $xmlNode->nodeValue;
    $found = TRUE;
  }
  return $found;
}

function dom2array_full($node){
  $result = array();
  if($node->nodeType == XML_TEXT_NODE) {
    $result = $node->nodeValue;
  }
  else {
    if($node->hasAttributes()) {
      $attributes = $node->attributes;
      if(!is_null($attributes)) 
	foreach ($attributes as $index=>$attr) 
	  $result[$attr->name] = $attr->value;
    }
    if($node->hasChildNodes()){
      $children = $node->childNodes;
      for($i=0;$i<$children->length;$i++) {
	$child = $children->item($i);
	if($child->nodeName != '#text')
	  if(!isset($result[$child->nodeName]))
	    $result[$child->nodeName] = dom2array($child);
	  else {
	    $aux = $result[$child->nodeName];
	    $result[$child->nodeName] = array( $aux );
	    $result[$child->nodeName][] = dom2array($child);
	  }
      }
    }
  }
  return $result;
}

function dom2array($node) {
  $res = array();
  print $node->nodeType.'<br/>';
  if($node->nodeType == XML_TEXT_NODE){
    $res = $node->nodeValue;
  }
  else{
    if($node->hasAttributes()){
      $attributes = $node->attributes;
      if(!is_null($attributes)){
	$res['@attributes'] = array();
	foreach ($attributes as $index=>$attr) {
	  $res['@attributes'][$attr->name] = $attr->value;
	}
      }
    }
    if($node->hasChildNodes()){
      $children = $node->childNodes;
      for($i=0;$i<$children->length;$i++){
	$child = $children->item($i);
	$res[$child->nodeName] = dom2array($child);
      }
    }
  }
  return $res;
}
*/