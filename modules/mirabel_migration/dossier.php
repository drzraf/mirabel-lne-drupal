<?php


require_once('utils.php');
require_once('fs-utils.php');
require_once('voc-utils.php');
require_once('file-utils.php');
require_once('node-utils.php');

require_once('dom-utils.php');
require_once('charset-utils.php');

function _mirabel_migrate_go( $page = NULL, $bid = NULL, $mlid = NULL,
			      $tags = array(), $accepteduri = array(), $refuseduri = array() ) {
  global $_mirabel_pad, $_mirabel_level, $_mirabel_pages_done,
    $_mirabel_root, $_mirabel_temp_roots,
    $_mirabel_accepteduri, $_mirabel_refuseduri,
    $_mirabel_dry_run, $_mirabel_warnings;

  $abspath = _mirabel_abspath($page);

  if($_mirabel_level == 0) {
    if($page{0} != '/' && file_exists(BASEPATH.'/'. $page)) {
      trigger_error('root page path should start with a /', E_USER_ERROR);
      $_mirabel_warnings++;
      die();
    }

    $_mirabel_temp_roots = array();
    $_mirabel_root = $page;
    if($accepteduri)
      $_mirabel_accepteduri = '!^(' . implode('|', $accepteduri) . ')!';

    if($refuseduri)
      $_mirabel_refuseduri = '!^(' . implode('|', $refuseduri) . ')!';
  }

  // if not part of the original call (the root) and not whitelisted
  elseif(! _mirabel_respect_root($abspath) ) {
    drush_log("$abspath outside root ($_mirabel_root)", 'status');
    // caller will increment
    //$_mirabel_warnings++;
    return _mirabel_book_load_by_old_path($abspath);
  }

  $subnode = _mirabel_book_load_by_old_path($abspath);
  if($subnode) {
    return $subnode;
  }

  // STOPPING CONDITIONS
  if($_mirabel_level >= MAXDEPTH) {
    trigger_error("won't go beyond " . MAXDEPTH . ' sub-pages level', E_USER_WARNING);
    $_mirabel_warnings++;
    return NULL;
  }

  $html = _mirabel_migrate_get_page($page);
  $sum = md5($html);

  // not found by path above, maybe by checksum ?
  $subnode = _mirabel_book_load_by_old_path($abspath, $sum);
  if($subnode)
    return $subnode;

  $_mirabel_pages_done[$sum] = $abspath;

  // pad output
  $_mirabel_pad++;
  $_mirabel_level++;

  if(! $html ) {
    trigger_error('NODE body empty! ... skip', E_USER_WARNING);
    $_mirabel_warnings++;
    $_mirabel_pad--;
    $_mirabel_level--;
    // XXX: what to do here ?
    return NULL;
  }

  /**
   * create a stub node:
   * As we may have to recurse deeply, we need two things:
   * - keep our node simple (a small memory footprint)
   * - start recursion quickly
   * (ie: processing local links before local files)
   *
   * But as the subnodes need to know which parent they are attached to,
   * we need a node_save() here so that Drupal gives us a couple (nid,bid)
   *
   * Rougly we need a title, a body (for HTML substitutions to happen) + nid/blid
   */

  $node = _mirabel_stub_node($abspath);
  //pad("NODE stub created (" . $node->nid . ")");

  _mirabel_config_node($node, $bid, $mlid);

  // also register the nid
  $_mirabel_pages_done[$node->nid] = $abspath;

  // this nid is useful in some cases, here the level is 1 already
  if($_mirabel_level == 1 && ! isset($_mirabel_pages_done["root"]))
    $_mirabel_pages_done['root'] = intval($node->nid);

  libxml_use_internal_errors(true);
   
  if(!$html) {
    trigger_error("preprocess HTML failed", E_USER_ERROR);
    die();
  }

  // this function may die()
  $charset = _mirabel_get_charset($html, $abspath);
  //pad("INFO: found charset $charset, processing entities");
  _mirabel_process_entities($html, $charset);
  _mirabel_clean_html($html);
  _mirabel_dom_tidy_start($html);

  $dom = new DOMDocument();
  $dom->preserveWhiteSpace = false;
  $dom->loadHTML($html);

  //pad("INFO: preprocess DOM");
  $_mirabel_pad++;
  _mirabel_preprocess_dom($dom, $page);
  //var_dump($dom->saveHTML());die();
  $_mirabel_pad--;

  if (!$dom) {
    foreach (libxml_get_errors() as $error) {
      //var_dump($error);
      // handle errors here
    }
    //libxml_clear_errors();
  }

  // add a title to the node
  _mirabel_fetch_title($node, $dom, $html, $page);

  pad("{$node->nid} \"{$node->title}\" ($charset)");
  if($node->path['pathauto'] == 1)
    pad("*aliased");
  if(! is_null($bid))
    pad("*child page ({$node->book['mlid']}) of $bid/$mlid");
  else
    pad("*new page ({$node->book['mlid']}) of new book ({$node->book['bid']})");

  // needed here because it store the title in the DB which is checked
  // to avoid duplicates (what is ensured in hook_validate())
  if(! $_mirabel_dry_run)
    node_save($node);

  // local files processing
  /* will need the above title set as a fallback description
     for anchor to parent whose text is null,
     eg: [parent-fid=parent-title] */
  //pad("NODE: link processing:");
  _mirabel_process_local_images($node, $abspath, $dom);

  // is needed twice (at least) to ensure there is no buggy
  // dom element left
  _mirabel_process_clean_links($dom);

  //var_dump($dom->saveHTML());die();
  $_mirabel_pad++;
  _mirabel_process_links($node, $abspath, $tags, $dom);
  $_mirabel_pad--;

  pad('NODE: update:');
  $_mirabel_pad++;
  _mirabel_update_node($node, $page, $tags);
  $_mirabel_pad--;

  /*
    local files processing:
    - identification
    - file creation
    - attach to the node
    - substitute references with [fid:] into the HTML through DOM
  */
  //pad("NODE: find files and remotes resources:");
  _mirabel_process_local_files($node, $abspath, $dom);
  // remote links (mainly for curiosity and script monitoring)
  $remote_links = _mirabel_get_remote_links($dom);
  if($remote_links) {
    pad(sprintf('found %d remote links', count($remote_links)));
    pad(var_export($remote_links, true));
  }

  /* to insert the whole HTML,
     but the image-background and other CSS prop' mess around
  */
  $html = $dom->saveHTML();

  //list($html, $style) = _mirabel_html_cleanup($dom);
  $node->body[$node->language][0]['value'] = $html;

  if(! $_mirabel_dry_run) { // because of the 'status' notice about node->changed
    // node_validate() trickery
    $fake_form = array();
    $node->changed = $node->created;
    node_validate($node, NULL, $fake_form);

    if(form_get_errors()) {
      _drush_log_drupal_messages();
      form_clear_error();
      die("error\n");
    }
  }

  // save node
  node_submit($node);
  // submit resets $node->created...
  if(file_exists(BASEPATH . $abspath))
    $node->created = filemtime(BASEPATH . $abspath);
  else {
    trigger_error("can't get timestamp for $abspath", E_USER_ERROR);
    $_mirabel_warnings++;
    die();
  }
  //if(file_exists($page)) $node->created = filemtime($page);

  if(! $_mirabel_dry_run) {
    // note #998590, which can be tested with node_view($node, "teaser");
    node_save($node);
    // resave with above modifications
    //book_node_insert($node);
  }

  /*if(preg_match('/mirabel\.lne\.free\.fr/', $html)) {
    var_dump($node->{OLDPATH_FILEFIELD}, $html);
    die();
    }*/
  //var_dump($node);
  $_mirabel_pad--;
  $_mirabel_level--;

  return $node;
}

function _mirabel_process_clean_links(&$dom) {
  global $_mirabel_warnings;
  $loop = 0;

startprocC:
  $cleanedup = 0;
  $xmlNodes = $dom->getElementsByTagName('a');
  foreach($xmlNodes as $xmlNode) {
    //pad("[a-cleanup]: " . $dom->saveHTML($xmlNode));
    $href = $xmlNode->getAttribute('href');
    $onclick = $xmlNode->getAttribute('onclick');
    $name = $xmlNode->getAttribute('name');
    //pad("found <a> ($href) [cleanup]");
    // fixes HTML <a name="Q"> or <a title="X">, <a onclick>
    if(! $href || $href == '#') {
      if($onclick) {
	$anchor = _mirabel_dom_get_extract_onclick($onclick);
	if(! $anchor) {
	  trigger_error("{$href}: onClick attr is invalid", E_USER_WARNING);
	  $_mirabel_warnings++;
	}
	// always remove this attribute to avoid looping over infinitely
	$xmlNode->removeAttribute('onclick');
	$xmlNode->setAttribute('href', $anchor);
	$cleanedup++;
	continue;
      }
    }

    if(!$href) {
      $anchor = null;
      if($name)
	$anchor = trim($name, '#');
      if(! $anchor && $xmlNode->getAttribute('title'))
	$anchor = $xmlNode->getAttribute('title');
      if($anchor) {
	$newnode = $dom->createElement('span', $xmlNode->nodeValue);
	$newnode->setAttribute('id', $anchor);
	$xmlNode->parentNode->replaceChild($newnode, $xmlNode);
	$cleanedup++;
	continue;
      }
      // TODO: what's up here ?
      continue;
    }

    if(stripos($href, 'javascript:') === 0) {
      $anchor = _mirabel_dom_get_extract_jshref($href);
      if($anchor) {
	$xmlNode->setAttribute('href', $anchor);
	$cleanedup++;
	continue;
      }
      trigger_error("{$href}: javascript:href attr is invalid", E_USER_WARNING);
      $_mirabel_warnings++;
      var_dump($href);die('test345'); //test
      $xmlNode->removeAttribute('href');
    }
  } // end foreach
  if($cleanedup) {
    $loop++;
    goto startprocC;
  }
  if($loop > 2)
    trigger_error("{$loop} loops to cleanup the links", E_USER_WARNING);
}

function _mirabel_process_links(&$booknode, $abspath, $tags, &$dom) {
  global $_mirabel_file_ext_rx, $_mirabel_root, $_mirabel_warnings;
  $count_ref_found = $count_new_pages = $count_linked_times = 0;

  if(is_dir(BASEPATH . $abspath))
    $base = $abspath;
  else
    $base = dirname($abspath);

  $blacklist = array();

  /* when you touch the dom the foreach counter becomes invalid
     and unpretictable behaviors happens. Thus we need to reprocess the whole.
     That's why these loop should rather use xmlNodes->length
     or while () loops */
startproc:
  $xmlNodes = $dom->getElementsByTagName('a');
  foreach($xmlNodes as $xmlNode) {
    $href = $xmlNode->getAttribute('href');
    if(empty($href)) {
      trigger_error("curious link:", E_USER_WARNING);
      pad($dom->saveHTML($xmlNode));
      continue;
    }

    if(strpos($href, '/node/') === 0)
      continue;
    if($href{0} == '?' || $href{0} == '#' || $href == 'Thumbs.db' || $href == '//')
      continue;
    // we already determined this location was invalid
    if(in_array( $href, $blacklist) )
      continue;
    if($href == $base)
      continue;
    if(strpos($href, 'mailto:') !== FALSE ||
       strpos($href, '/icons/') !== FALSE)
      continue;

    //pad("found <a> $lnktxt ($href)");
    $lnktxt = $lnkdst = '';
    if($xmlNode->nodeValue)
      $lnktxt = trim(preg_replace("/\s+/s", ' ', $xmlNode->nodeValue));
    $lnkdst = _mirabel_abspath($href, $base);
    // not needed since htmltidy does it for us
    //$xmlNode->setAttribute('href', htmlspecialchars($lnkdst, ENT_QUOTES, "UTF-8", FALSE) );

    if(preg_match(';\.' . $_mirabel_file_ext_rx . '(#.+)?$;i', $lnkdst ) || $lnkdst{0} == '#' )
      continue;
    if(stripos($lnkdst, 'http://') === 0 && ! preg_match('|' . HTTPURL . '|', $lnkdst) )
      continue;

    // skips links to the root FQDN
    if($lnkdst == '/forum' || $href == HTTPURL || preg_match(';^'.HTTPURL2.'/*$;', $href)) {
      $xmlNode->setAttribute('href', '#');
      continue;
    }

    // cleans #anchor
    if(strpos($lnkdst, basename($abspath) . '#') === 0) {
      $xmlNode->nodeValue = str_replace(basename($abspath),
					 '',
					 $xmlNode->nodeValue);
      continue;
    }

    $anchor_children = 0;
    if(! $lnktxt || preg_match('/^\s+$/uD', $lnktxt)) {
      $anchor_children = $xmlNode->childNodes->length;
      pad(sprintf("a-link \"BLANK(%d)/%s\" > \"%s\"", $anchor_children, $lnktxt, $lnkdst));
      // XXX: utf8_encode() linkdst ? rather no
      if(! $anchor_children)
	$xmlNode->nodeValue = basename($lnkdst);
    }
    else
      pad("a-link '$lnktxt' -> '$lnkdst')");

    $count_ref_found++;
    $fixeddst = $lnkdst;

    if(! _mirabel_fs_interpolation($fixeddst, $base)) {
      //var_dump($base);die();
      trigger_error($lnkdst . " is an invalid anchor location", E_USER_WARNING);
      $_mirabel_warnings++;
      continue;
    }

    //var_dump("_______", $lnkdst, $fixeddst, $base, _mirabel_abspath($fixeddst, $base));

    if($abspath == _mirabel_abspath($fixeddst)) {
      /*drush_log('self-link: remove', 'status');
	$xmlNode->parentNode->removeChild($xmlNode);*/
      $xmlNode->parentNode->replaceChild($dom->createTextNode($lnktxt), $xmlNode);
      goto startproc;
    }

    $rootlink = _mirabel_is_link_to_root(_mirabel_abspath($fixeddst, $base));

    if($rootlink == 1) {
      $xmlNode->parentNode->removeChild($xmlNode);
      goto startproc;
    }
    elseif($rootlink == 2) {
      $rootid = _mirabel_compaturi_to_bid($_mirabel_root);
      $newtxt = _mirabel_dom_anchor_to_nid($rootid, $lnktxt);
      $xmlNode->parentNode->replaceChild($dom->createTextNode($newtxt), $xmlNode);
      goto startproc;
    }

    // RECURSE HERE !
    // the current node mlid should be the "plid" of the children
    $subnode = 
      _mirabel_migrate_go($fixeddst,
			  $booknode->book['bid'],
			  $booknode->book['mlid'],
			  $tags);


    if(!$subnode || !$subnode->nid) {
      trigger_error('no NID found for this path', E_USER_WARNING);
      $_mirabel_warnings++;
      $blacklist[] = $href;
      continue;
    }

    if($lnktxt || $anchor_children == 0) {
      //var_dump("a", $dom->saveHTML($xmlNode), $subnode->nid);
      $newtxt = _mirabel_dom_anchor_to_nid(
	$subnode->nid,
	$lnktxt ? $lnktxt : (isset($subnode->title) ? $subnode->title : NULL));
      $xmlNode->parentNode->replaceChild($dom->createTextNode($newtxt), $xmlNode);
    }
    else {
      foreach(array('onclick', 'onmouseout', 'onmouseover', 'target') as $attr)
	$xmlNode->removeAttribute($attr);
      //var_dump("b", $dom->saveHTML($xmlNode), $subnode->nid);
      $xmlNode->setAttribute('href', "/node/{$subnode->nid}");
    }
    $count_linked_times++;
    goto startproc;
  }

  pad("found $count_ref_found references to others pages");
  if($count_ref_found) {
    pad("fetched $count_new_pages new pages");
    pad("substitued $count_linked_times references");
  }
}

function _mirabel_get_remote_links($dom) {
  $remote_links_matches = array();
  $xmlNodes = $dom->getElementsByTagName('a');
  foreach($xmlNodes as $xmlNode) {
    $lnkdst = $xmlNode->getAttribute('href');
    $xmlNode->removeAttribute('target');
    if(_is_url_foreign($lnkdst))
       $remote_links_matches[] = $lnkdst;
  }
  return $remote_links_matches;
}

function _is_url_file($arg) {
  global $_mirabel_file_ext_rx;

  return ! preg_match('/\.' . $_mirabel_file_ext_rx . '(#[a-z]+)?$/i', $arg);
}

// link not matching our domain are returned
function _is_url_foreign($arg) {
  $arg = strip_tags($arg);
  return ( ! preg_match('|'. HTTPURL .'|', $arg) && preg_match('|http://|', $arg) );
}

function _mirabel_process_local_files(&$booknode, $abspath, &$dom, $count = 0) {
  global $_mirabel_file_ext_rx, $_mirabel_warnings, $_mirabel_dry_run;

  $count_ref_found = $count_new_files = $count_linked_times = 0;
  $blacklist = array();

  if(is_dir(BASEPATH . $abspath))
    $base = $abspath;
  else
    $base = dirname($abspath);

startprocL:
  // FILE LINKS
  $xmlNodes = $dom->getElementsByTagName('a');
  foreach($xmlNodes as $xmlNode) {
    $lnkdst = $xmlNode->getAttribute('href');
    $lnktxt = _mirabel_get_link_title($dom, $xmlNode);

    // we already determined this location was invalid
    if( in_array( $lnkdst, $blacklist) )
      continue;
    if(strpos($lnkdst, '/icons/') !== FALSE)
      continue;

    if(! $lnkdst ) {
      if(! $xmlNode->getAttribute('onclick')) {
	drush_log(sprintf('curious empty link %s (%s)',
			  ($lnktxt ? ': ' . $lnktxt : ''),
			  $dom->saveHTML($xmlNode)),
		  'warning');
	$_mirabel_warnings++;
      }
      // TODO: else
      continue;
    }

    if(! $lnktxt || preg_match('/^\s+$/uD', $lnktxt))
      $lnktxt = basename($lnkdst);
    //pad("found <a> $lnktxt ($lnkdst)");
    if( $lnkdst{0} == '#' || ! preg_match(';\.' . $_mirabel_file_ext_rx . '(#.+)?$;i', $lnkdst ))
      continue;
    if( stripos($lnkdst, 'http://') === 0 && ! preg_match(';' . HTTPURL . ';i', $lnkdst ) )
      continue;

    pad("file \"$lnktxt\" -> \"$lnkdst\"");
    $count_ref_found++;
    $fixeddst = $lnkdst;

    if(! check_res_exists($fixeddst, $base)) {
      trigger_error($lnkdst . ' is an invalid file location', E_USER_WARNING);
      $_mirabel_warnings++;
      $blacklist[] = $lnkdst;
      continue;
    }

    $file = _mirabel_create_file($fixeddst, $lnkdst, $count_new_files);
    if($file) {
      if($_mirabel_dry_run)
	pad(sprintf('dryrun: linking to %d [%s]', $file->fid, $file->uri));
      else
	pad("\tfid = " . $file->fid);

      _mirabel_attach_file($booknode, $file);

      $newnode = _mirabel_file_ref_subst($dom, $lnktxt, $file);
      $xmlNode->parentNode->replaceChild($newnode, $xmlNode);
      $count_linked_times++;
      goto startprocL;
    }
    else {
      trigger_error("can't create nor retrieve the file \"$lnkdst\"", E_USER_WARNING);
      $_mirabel_warnings++;
      die("a\n"); // TODO
    }
  }
  
  pad( "found $count_ref_found references to local ressources");
  if($count_ref_found) {
    pad( "added $count_new_files new files");
    pad( "substitued $count_linked_times references");
  }
}


function _mirabel_process_local_images(&$booknode, $abspath, &$dom, $count = 0) {
  global $_mirabel_warnings, $_mirabel_dry_run;
  $count_ref_found = $count_new_files = $count_linked_times = 0;
  $imgfound = array();

  if(is_dir(BASEPATH . $abspath))
    $base = $abspath;
  else
    $base = dirname($abspath);

startprocI:
  $xmlNodes = $dom->getElementsByTagName('img');
  foreach($xmlNodes as $xmlNode) {
    $lnkdst = $xmlNode->getAttribute('src');
    $lnktxt = $xmlNode->getAttribute('alt');
    // pad("found <a> $lnktxt ($lnkdst)");
    //if( preg_match(';^/fid/\d+$;', $lnkdst )) continue;
    if( ! preg_match(';\.(png|gif|jpg|bmp|jpeg)$;i', $lnkdst ))
      continue;
    if ( stripos($lnkdst, 'http://') === 0 && ! preg_match(';' . HTTPURL . ';i', $lnkdst ) )
      continue;
    if(strpos($lnkdst, '/icons/') !== FALSE)
      continue;

    //pad("image \"$lnktxt\" ($lnkdst)");
    $imgfound[] = basename($lnkdst);

    $count_ref_found++;
    $fixeddst = $lnkdst;

    if(! check_res_exists($fixeddst, $base)) {
      //trigger_error("{$lnkdst} is an invalid image location", E_USER_WARNING);
      //$_mirabel_warnings++;
      continue;
    }

    $file = _mirabel_create_file($fixeddst, $lnkdst, $count_new_files);
    /*if($file && $_mirabel_dry_run)
      pad(sprintf('dryrun: linking to %d [%s]', $file->fid, $file->uri));*/
    //_mirabel_attach_file($booknode, $file);

    // TODO: sadly TinyMce wysiwyg does not support
    // style setup through the "style" attribute
    $newnode = $dom->createElement('img');
    $newnode->setAttribute('src', '/fid/' . $file->fid);
    if( ($alt = $xmlNode->getAttribute('alt')) )
      $newnode->setAttribute('alt', $lnktxt);
    if( ($width = $xmlNode->getAttribute('width')) )
      $newnode->setAttribute('width', $width);
    if( ($height = $xmlNode->getAttribute('height')) )
      $newnode->setAttribute('height', $height);
    $xmlNode->parentNode->replaceChild($newnode, $xmlNode);

    $count_linked_times++;
    goto startprocI;
  }

  if($xmlNodes->length) {
    pad( sprintf("found %d references to local images: [%s]", $count_ref_found, implode(',', $imgfound)));
    if($count_ref_found)
      pad( sprintf( "added %d new images, %d references substitued", $count_new_files, $count_linked_times) );
  }
}
