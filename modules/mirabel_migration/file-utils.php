<?php

require_once('media-utils.php');

function _mirabel_file_load_by_old_path($abspath) {
  global $_mirabel_dry_run;
  $file = NULL;

  // XXX: below to test without inserting
  if($_mirabel_dry_run) {
    global $_mirabel_pages_done;
    if( ($fid = array_search($abspath, $_mirabel_pages_done, FALSE)) !== FALSE) {
      pad("FILE XXX: '$abspath' already registered under FID " . $fid );
      $file = new stdClass();
      $file->fid = $fid;
      return $file;
    }
  }

  // hardcoded, from field_info_bundles('file')
  $file_bundles = array ( 'application', 'audio', 'image', 'text', 'video', 'default'  );
  $query = new EntityFieldQuery;
  $result = $query
    ->entityCondition('entity_type', 'file')
    ->entityCondition('bundle', $file_bundles, 'IN')
    ->fieldCondition(OLDPATH_FILEFIELD, 'value', $abspath)
    ->deleted(FALSE)
    // may be some stale file entities during tests
    // fetch the latest
    ->entityOrderBy('entity_id', 'DESC')
    ->range(0,1)
    ->execute();

  if(! empty($result)) {
    $fid = key($result['file']);
    $efile = file_load($fid);
    if($efile) {
      pad(sprintf('FILE: "%s" already registered under FID %d', $abspath, $fid) );
      $file = $efile;
    }
  }
  return $file;
}

/*
  - the OLDPATH_FILEFIELD file field stores the path as specified in
    the HTML markup so that we can change/enhance the filesystem
    matching rules later.
  - $description only store the basename of the file as the nodeValue
    itself may not be usable as a file description
  - $inc is incremented if a new file was created
*/
function _mirabel_create_file($path, $description = NULL, &$inc) {
  global $_mirabel_dry_run;

  $abspath = _mirabel_basename($path);
  $file = _mirabel_file_load_by_old_path($abspath);

  if(! $file) {
    $file = new stdClass();
    $file->status = FILE_STATUS_PERMANENT;

    // needed ?
    //$file->filemime = file_get_mimetype($path);
    $file->display = 1;

    if($description)
      $file->mirabel_file_description[LANGUAGE_NONE][0]['value'] = $description;

    // ISO char cause problems here
    $s_abspath = $abspath;
    if(! mb_check_encoding($abspath, 'UTF-8'))
      $s_abspath = iconv('ISO-8859-1', 'UTF-8', $abspath);
    $file->{OLDPATH_FILEFIELD}[LANGUAGE_NONE][0]['value'] = $s_abspath;

    $link_name = _mirabel_munge_filename(basename($abspath));
    /* 
       useless for us
       $link_name = file_munge_filename($link_name,
       variable_get('upload_extensions_default',
       'jpg jpeg gif png txt html doc xls pdf ppt pps odt ods odp'));
    */
    
    $dest = file_create_filename($link_name, MIRABEL_DESTDIR);
    $link_name = basename($dest);
    //if(basename($abspath) != $link_name)
    //pad('FILE: renamed from "' . basename($abspath) . "\" to $link_name");

    $file->uri = $dest;
    $file->filename = $link_name;
    if(! $_mirabel_dry_run ) {
      // XXX: copy
      //  drupal_realpath($file->uri)
      if(!file_exists($file->uri)) {
	system("ln -vf '$path' '". MIRABEL_DESTDIR . '/' . $link_name . "'");
	system("chmod 644 '" . MIRABEL_DESTDIR . '/' . $link_name . "'");
      }
      $file = file_save($file);
    }
    else {
      $file->fid = rand(500,990);
    }
    $inc++;
  }
  return $file;
}

/*
  will attach a file to the node if not already attached.
  Attachment will happen if the above _mirabel_create_file()
  incremented $inc
 */
function _mirabel_attach_file(&$node, $file) {
  /* needed when the first file to attach
     already exists in the filesystem */
  if(! isset($node->field_fichier))
    $node->field_fichier = array($node->language => array());

  foreach($node->field_fichier[$node->language] as $f) {
    if($f['fid'] == $file->fid)
      return FALSE;
  }
  if(isset($file->filemime) && 
     ($file->filemime == 'image/gif' || $file->filemime == 'image/jpeg'))
    $file->display = 0;
  else {
    // see bug #1327062
    $file->display = 1;
  }

  $node->field_fichier[$node->language][] = (array)$file;
  return TRUE;
}

function _mirabel_munge_iconv_filename($filename) {
  //setlocale(LC_CTYPE, "fr_FR.UTF-8");
  if(! mb_check_encoding($filename, 'UTF-8')) {
    $filename = iconv('ISO-8859-1', 'ASCII//TRANSLIT', $filename);
    drush_log('munge: iconv() found an iso8859-1 filename', 'status');
  }
  else
    $filename = iconv('UTF-8', 'ASCII//TRANSLIT', $filename);
  //setlocale(LC_CTYPE, "C");
  return $filename;
}

function _mirabel_munge_filename($filename) {
  $filename = _mirabel_munge_iconv_filename($filename);
  $filename = str_replace( array( 'pdf.pdf', 'JPG', 'JPEG', 'GIF'),
			   array( 'pdf',     'jpg', 'jpeg', 'gif'),
			   $filename);
  $filename = preg_replace('/  +/', ' ', $filename);
  $filename = preg_replace('/ *- */', '-', $filename);
  $filename = strtr($filename, '? +', '---');
  $filename = str_replace('_-', '-', $filename);
  $filename = str_replace('-_', '-', $filename);
  $filename = str_replace(array('[',']','&'), '', $filename);
  $filename = trim($filename);
  return $filename;
}

function _mirabel_file_ref_subst($dom, $current_text, $file) {
    $newtxt = '[fid:' . $file->fid;
    if($current_text)
      $newtxt .= ('=' . $current_text . ']');
    /* poor fallback, file->description is probably
       basename(file) itself, see the above  _mirabel_create_file() */
    elseif(isset($file->description))
      $newtxt .= ('=' . $file->description . ']');
    else
      $newtxt .= ']';

    return $dom->createTextNode($newtxt);
}


function _mirabel_press_attach_file($file_url) {
  $a = null;
  $sn = MIRABEL_DESTDIR . '/press/' . _mirabel_munge_filename(basename($file_url));

  if(file_exists($sn)) {
    $query = new EntityFieldQuery;
    $result = $query
      ->entityCondition('entity_type', 'file')
      ->propertyCondition('uri', $sn)
      ->range(0,1)
      ->execute();
    if($result) {
      echo "$sn already registered\n";
      $a = current(entity_load('file', array(key($result['file']))));

      // repair
      if(! isset($a->display) )
	$a->display = 1;
      if(! isset($a->description) )
	$a->description = basename($a->uri);

      return file_save($a);
    }
  }

  if(! $a ) {
    $file = new stdClass();
    $file->uri = $file_url;
    $a = file_copy($file, $sn, FILE_EXISTS_REPLACE);

    if ( ! $a ) {
      trigger_error("can't copy $file_url to $sn", E_USER_ERROR);
      return null;
    }
    $a->status = FILE_STATUS_PERMANENT;
    $a->filemime = file_get_mimetype($a->uri);
    $a->display = 1;
    $a->description = basename($a->uri);
    $a = file_save($a);
  }

  return (array)$a;
}

function _mirabel_article_attach_image($in, $tt) {
  if(! $in)
    return NULL;

  $file =  new stdClass();
  if(file_exists('file://' . BASEPATH ))
    $file->uri = 'file://' . BASEPATH . "/news_${tt}/imgs/" . $in;
  else
    $file->uri = HTTPURL . "/news_${tt}/imgs/" . $in;

  //var_dump($file->uri); return;
  $a = null;
  $sn = MIRABEL_DESTDIR . '/pictures/' . basename($in);
  if(file_exists($sn)) {
    $query = new EntityFieldQuery;
    $result = $query
      ->entityCondition('entity_type', 'file')
      ->propertyCondition('uri', $sn)
      ->range(0,1)
      ->execute();

    if($result)
      $a = current(entity_load('file', array(key($result['file']))));
  }
  if(! $a ) {
    $a = file_copy($file, $sn, FILE_EXISTS_REPLACE);
    if($a) {
      $a->status = FILE_STATUS_PERMANENT;
      $a->filemime = file_get_mimetype($a->uri);
      $a = file_save($a);
    }
  }
  return $a;
}

/*
function _mirabel_attach_file(&$main_node, $file) {
  // attach to node
  $main_node->field_fichier[$node->language][0] += $file;

  // TODO field_description ?
  $path_to_replace = basename($file->description);
  $count = 0;
  if($path_to_replace) {
    $main_node->body[$main_node->language][0]['value'] =
      preg_replace(
	';<[^>]+' . $path_to_replace . '[^>]+>;',
	'[fid:' . $file->fid . ']',
	$main_node->body[$main_node->language][0]['value'],
	-1,
	$count
      );
  }

  if($count) {
    echo "replaced $count times.\n";
    var_dump($main_node);
  }
  else
    echo "not replaced.\n";
}
*/
