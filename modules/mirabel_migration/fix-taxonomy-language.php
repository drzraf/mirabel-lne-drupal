<?php

// see bug #1404000
// select nid, title from d_taxonomy_index NATURAL JOIN d_node where tid = 24;
$nids = db_query("SELECT nid FROM {taxonomy_index} where tid = 24")->fetchCol();

foreach($nids as $v) {
  _mirabel_fix_wrong_taxo_lang($v);
}

function _mirabel_fix_wrong_taxo_lang($nid) {
  $node = node_load($nid);
  echo "\t{$nid}\t{$node->title}\n";
  $taxo = $node->field_tagscatun;

  // needed
  $node->path['pathauto'] = 0;
  $node->path['alias'] = '';

  if( ! in_array($nid, array(671, 674)) ) {  
    $node->field_tagscatun[LANGUAGE_NONE] = $node->field_tagscatun[$node->language] = array();
    echo "flush catun\n";
    //node_save($node);
  }
  elseif( (! empty($taxo[LANGUAGE_NONE]) && ! isset($node->field_tagscatun[$node->language])) ||
	  (! empty($taxo[LANGUAGE_NONE]) && ! array_diff($taxo[LANGUAGE_NONE], $node->field_tagscatun[$node->language])) ) {
    $node->field_tagscatun[$node->language] = $node->field_tagscatun[LANGUAGE_NONE];
    $node->field_tagscatun[LANGUAGE_NONE] = array();
    echo "change indice\n";
    //node_save($node);
  } else {
    echo "NO CHANGE!\n";
  }
}

/* PATHAUTO:
   path ID to keep:
   ( 287, 288, 289, 290, 551, 549 )
*/