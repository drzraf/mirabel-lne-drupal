<?php

/**
   HTTPURL2: used in insert-article.php only
 */
function _mirabel_basename($path) {
  if( preg_match('!^(' . implode(')|(', array(HTTPURL, HTTPURL2, BASEPATH) ) . ')!', $path) )
    return preg_replace('!^(' . HTTPURL . '|' . HTTPURL2 . '|' . BASEPATH . ')?/*!', '/', $path);
  return $path;
}

/*
  $root: optionnal, used as a prefix for relative path
*/
function _mirabel_abspath($path, $root = NULL) {
  if(! $path) {
    drush_log("empty params given ".__FUNCTION__, "status");
    return '';
  }
  $basename = _mirabel_basename($path);
  if($root) {
    // canonicalize, way 1 (eg: $path=index.html)
    $tmpname = realpath(BASEPATH.'/'.$root.'/'.$basename);
    if($tmpname && file_exists($tmpname))
      return str_replace(BASEPATH, '', $tmpname);
    // canonicalize, way 1 (eg: $path=$base/../x/y.html)
    if(strpos($basename, $root) === 0) {
      $tmpname = realpath(BASEPATH.'/'.$basename);
      if($tmpname && file_exists($tmpname))
	return str_replace(BASEPATH, '', $tmpname);
    }
  }
  else {
    $tmpname = realpath(BASEPATH.'/'.$basename);
    if($tmpname && file_exists($tmpname))
      return str_replace(BASEPATH, '', $tmpname);
  }
  return $basename;
}

function _mirabel_migrate_get_page($page) {
  if(strstr($page, '.php') ||
     $page[strlen($page)-1] == '/') {
    pad("=== fetching '$page' (web)");
    if(preg_match('!' . HTTPURL . '!', $page))
      $html = file_get_contents($page);
    else
      $html = file_get_contents(HTTPURL . '/' . _mirabel_basename($page));
  }

  elseif(strstr($page, BASEPATH) === FALSE) {
    pad("=== opening '$page' (local)");
    if(preg_match('!^(' . HTTPURL . '|' . BASEPATH . ')!', $page))
      $html = file_get_contents($page);
    else
      $html = file_get_contents(BASEPATH . '/' . $page);
  }

  else {
    pad("=== opening '$page' (file)");
    $html = file_get_contents($page);
  }
  return $html;
}

function check_res_exists(&$item, $obase) {
  $base = realpath(BASEPATH . '/' . $obase);

  $saved_item = $item;
  $saved_item_base = _mirabel_basename($item);

  if($saved_item_base == '/video/realplayer.zip')
    return FALSE;
  if($saved_item_base == 'notes_flag.gif')
    return FALSE;

  if(preg_match('|#[a-z]+$|i', $item))
    $item = preg_replace(';#.+$;', '', $item);

  $item = str_replace("%20", " ", $item);
  if($item{0} == '/') {
    $item = realpath(BASEPATH . '/' . $item);
  }
  // url ? => BASEPATH
  elseif(strstr($item, 'http://')) {
    $item = str_replace(HTTPURL, BASEPATH, $item);
  }
  // relative (with ../ or anything else) ?
  // concat with BASEPATH
  if(!realpath($item)) {

    // when fetching frames, multiple bases are possible and store in this global variable
    global $_mirabel_temp_roots;
    foreach($_mirabel_temp_roots as $possible_bases) {
      if(file_exists(realpath(BASEPATH . '/' . $possible_bases . '/' . $item))) {
	$item = realpath(BASEPATH . '/' . $possible_bases . '/' . $item);
	return TRUE;
      }
    }

    if(file_exists(realpath($base . '/' . $item)))
      $item = realpath($base . '/' . $item);
    elseif(file_exists(realpath(dirname($base) . '/' . $item))) {
      //pad("guessed a new place in $base/.. for $item");
      $item = realpath(dirname($base) . '/' . $item);
    }
    elseif(strstr($item, 'projetPRSE.pdf')) {
      pad("fixed a new place (/zancien/activites/sante/prse/projetPRSE.pdf) for $saved_item");
      $item = realpath(BASEPATH . '/zancien/activites/sante/prse/projetPRSE.pdf');
    }
    elseif(strstr($item, '/doc/')) {
      $test = realpath(BASEPATH . preg_replace('|^.*?/doc/|', '/doc/', $item));
      if(!$test)
	$test = realpath(BASEPATH . preg_replace('|.*/doc/|', '/zancien/doc/', $item));
      if($test) {
	//pad("guessed a new place in /doc for $item");
	$item = $test;
	return TRUE;
      }
    }
    elseif(strstr($item, '/evenements/')) {
      //pad(preg_replace(';^.*/evenements/;', '', $item));
      if(file_exists(realpath(BASEPATH . '/themes/eau/dce/2005/evenements/' .
			      preg_replace(';^.*/evenements/;', '', $item)))) {
	pad("guessed a new place in /themes/.../evenements for $item");
	$item = realpath(BASEPATH . '/themes/eau/dce/2005/evenements/' .
			 preg_replace(';^.*/evenements/;', '', $item));
      }
    }
    elseif(strstr($item, 'cp_dce_mirabel_170505.pdf')) {
      pad("fixed a new place (/themes/eau/dce/2005/download_dce/cp_dce_mirabel_170505.pdf) for $saved_item");
      $item = realpath(BASEPATH . '/themes/eau/dce/2005/download_dce/cp_dce_mirabel_170505.pdf');
    }
    elseif(strstr($item, 'Petition-collectif-moselle-est.pdf')) {
      pad("fixed a new place (/themes/eau/dce/2005/download_dce/Petition-collectif-moselle-est.pdf) for $saved_item");
      $item = realpath(BASEPATH . '/themes/eau/dce/2005/download_dce/Petition-collectif-moselle-est.pdf');
    }
    elseif(strstr($item, 'expo-enjeuxdce.zip')) {
      pad("fixed a new place (/themes/eau/dce/2005/expo/expo-enjeuxdce.zip) for $saved_item");
      $item = realpath(BASEPATH . '/themes/eau/dce/2005/expo/expo-enjeuxdce.zip');
    }
    elseif(strstr($item, 'DOSSIERS/gazdeschiste')) {
      //pad("fixed a new place (/gds) for $saved_item");
      $item = preg_replace(';.*DOSSIERS/gazdeschiste;',
			   realpath(BASEPATH . '/gds'),
			   $item);
    }
    elseif(strstr($item, 'images/accueil_htm_smartbutton')) {
      //pad("fixed a new place (/images/accueil) for $saved_item");
      $item = preg_replace(';images/accueil;',
			   realpath(BASEPATH . '/zancien/v4/images') .
			   '/home',
			   $item);
    }
    // other cases come here
  }


  if(! file_exists($item)) {
    // same attempt with the urldecode()'d version
    $dec = urldecode($saved_item);
    if($dec != $saved_item) {
      $item = $dec;
      return check_res_exists($item, $obase);
    }

    global $_mirabel_temp_roots, $_mirabel_warnings;
    drush_log("$saved_item does not exists:", "warning");
    //caller will increment
    //$_mirabel_warnings++;
    pad("W: base=$base; "
	. ( $_mirabel_temp_roots ? "; array= " . implode(',', $_mirabel_temp_roots) : '' )
	. ")");
    $item = $saved_item;
    return FALSE;
  }
  $item_base = _mirabel_basename($item);
  /*if ($item_base != $saved_item_base)
    pad("fixed: " . basename($saved_item_base)." got a new place: " . dirname($item_base));*/
  return TRUE;
}

function _mirabel_valid_file($str) {
  $p = realpath($str);
  if($p && file_exists($p) && ! is_dir($p))
    return $p;
  return FALSE;
}

function _mirabel_valid_dir($str) {
  $p = realpath($str);
  if($p && file_exists($p) && is_dir($p))
    return $p . '/';
  return FALSE;
}

/*
  $base should be the absolute path from the root
  of the www directory.
  In no case there should be HTTPURL or BASEPATH in it,
  so use _mirabel_basename() before calling this function.
 */

function _mirabel_fs_interpolation(&$item, $base) {
  // valid directories, fallback if "not a file"
  $fallback = FALSE;
  
  /*$accepteduri = $data['accepteduri'];
    $base = $data['base'];
    ...
    foreach($accepteduri as $v) {
    if(strstr($arg, $v))
    return TRUE;
    }*/
  $saved_base = $base;
  $base = realpath(BASEPATH . '/' . $base);

  // absolute relative to website root, no $base needed
  $saved_item = $item;
  // anchor...
  if(preg_match('|#[a-z]+$|i', $item)) {
    $item = preg_replace(';#.+$;', '', $item);
  }
  // XXX!
  //if($item{0} == '/' && ! preg_match('|\.php$|', $item))
  //realpath(BASEPATH . '/' . $item);

  // url ? => BASEPATH
  if(strstr($item, 'http://')) {
    $item = realpath(str_replace(HTTPURL, BASEPATH, $item));
  }
  elseif(preg_match('|\.php(\?.*)?$|', $item)) {
    // php files should be kept with a leading '/' and a trailing .php to be interpreted ...
    $item = str_replace(BASEPATH, '', $base) . '/' . $item;
    return TRUE;
  }

  $guesses = array(
    BASEPATH . '/' . $item,
    $base . '/' . $item,
    $base . '/../' . $item,
    $base . '/' . dirname($item) . '/../' . basename($item),
    $base . '/' . basename($item));

  global $_mirabel_temp_roots;
  if($_mirabel_temp_roots) {
    //debug: var_dump($_mirabel_temp_roots);
    foreach($_mirabel_temp_roots as $v) {
      $guesses[] = BASEPATH . '/' . $v . '/' . $item;
      reset($_mirabel_temp_roots);
    }
  }

  // relative (with ../ or anything else) ?
  // concat with BASEPATH
  foreach($guesses as $guess) {
    //echo "========= $guess\n";
    $p = _mirabel_valid_file($guess);
    if($p) {
      $item = $p;
      return TRUE;
    }
    if(! $fallback)
      $fallback = _mirabel_valid_dir($guess);
  }

  $map = array(
    'evenements/' => '/../2005/vdo/',
    'activites/dechets/tri/pourquoitrier.htm' => '/themes/dechets/tri/pourquoitrier.htm',
    'dce/indexthem.htm' => '/themes/eau/dce/2005/indexthem.htm',
    '../../doc/outils/posters/sacs_plastiques.htm' => '/doc/outils/posters/sacs_plastiques.htm',
    '../../doc/outils/posters/stop_pub.htm' => '/doc/outils/posters/stop_pub.htm',
  '/siteweb/dossiers/transports.htm' => '/themes/transports/transports.htm');
  foreach($map as $k => $v) {
    if(strstr($item, $k) && _mirabel_valid_file(BASEPATH.'/'.$v)) {
      $item = $v;
      return TRUE;
    }
    if(strstr($item, $k) && file_exists(realpath($base.'/'.$v.'/'.$item))) {
      $item = $base.'/'.$v.'/'.$item;
      return TRUE; 
    }
    if(strstr($item, $k) && file_exists(realpath(BASEPATH.'/'.$v.'/'.$item))) {
      $item = $v.'/'.$item;
      return TRUE; 
    }
  }

  $map = array(
    'themes/eau/dce/vdo' => 'themes/eau/dce/2005/vdo',
    'dce/actu/actu.htm' => 'themes/eau/dce/actu/actu.htm',
    'dce/vdo/' => 'themes/eau/dce/2005/vdo/',
    '/dce/lexique' => 'themes/eau/dce/2005/lexique.htm',
    '../activites/vieassociative' => '/zancien/activites/vieassociative',
    '/plan' => '/zancien/plan',
    '../../../activites/sante/prse' => '/zancien/activites/sante/prse',
    '/ogm' => '/themes/ogm',
  );
  foreach($map as $k => $v) {
    if(strstr($item, $k)) {
      if( _mirabel_valid_file(BASEPATH.
			      '/'.
			      str_replace($k, $v, $item))) {
	$item = str_replace($k, $v, $item);
	return TRUE;
      }
      if( _mirabel_valid_file(BASEPATH.
			      '/'.
			      str_replace($k, $v, $item).
			      '/index.htm')) {
	$item = str_replace($k, $v, $item).'/index.htm';
	return TRUE;
      }
    }
  }

  if(_mirabel_valid_file($item) === FALSE) {
    if(( $item = _mirabel_valid_dir(BASEPATH . '/' . $item)))
      return TRUE;
    if($fallback) {
      $item = $fallback;
      return TRUE;
    }

    $dec = urldecode($saved_item);
    if($dec != $saved_item) {
      $item = $dec;
      return _mirabel_fs_interpolation($item, $saved_base);
    }

    global $_mirabel_warnings;
    drush_log("$saved_item does not exists", 'warning');
    //caller will increment
    //$_mirabel_warnings++;
    return FALSE;
  }
  return TRUE;
}


function _mirabel_frame_fs_interpolation(&$item, $page){
  global $_mirabel_temp_roots;
  $abs = $item = _mirabel_abspath($item, dirname($page));

  $guesses = array(
    BASEPATH . '/' . $item,
    BASEPATH . '/' . dirname(_mirabel_abspath($page)) . '/' . $item
  );
  foreach($guesses as $guess) {
    $item = _mirabel_valid_file($guess);
    if($item) {
      // XXX: this path works now, it may be useful later
      // eg: in _mirabel_fs_interpolation()
      $_mirabel_temp_roots[] = dirname($abs);
      $_mirabel_temp_roots = array_unique($_mirabel_temp_roots);
      return TRUE;
    }
  }

  $guesses = array();
  if($_mirabel_temp_roots) {
    foreach($_mirabel_temp_roots as $v) {
      $guesses[] = BASEPATH . '/' . $v . '/' . $abs;
      reset($_mirabel_temp_roots);
    }
  }

  foreach($guesses as $guess) {
    //echo "=> $guess\n";
    $item = _mirabel_valid_file($guess);
    if($item)
      return TRUE;
  }
  return FALSE;
}

/* links to the root node (arrows & co).
   we don't need these anymore as we use
   Drupal menus now, so in most cases: remove these links */
function _mirabel_is_link_to_root($path) {
  global $_mirabel_pages_done, $_mirabel_root;

  if($path != $_mirabel_root)
    return 0;

  if($_mirabel_root == '/themes/dechets/campagneprevention/index.htm' ||
     $_mirabel_root == '/themes/eau/dce/2005/medias.htm'
    //$_mirabel_root != '/themes/eau/dce/2005/liens.htm'
  ) {
    drush_log("useless link back to root: strip", "status");
    return 1;
  }

  drush_log("useless link back to root: tweak", "status");
  return 2;
}

// later ?
// http://stackoverflow.com/questions/2267074
function _mirabel_merge_path_overlap($left, $right) {
  $l = strlen($right);
  // keep checking smaller portions of right
  while($l > 0 && substr($left, $l * -1) != substr($right, 0, $l))
    $l--;

  return $left . substr($right, $l);
}

function _mirabel_respect_root($path) {
  global $_mirabel_root, $_mirabel_accepteduri, $_mirabel_refuseduri;

  return (
    (
      // path match the original /root
      strpos($path, dirname($_mirabel_root), 0) !== FALSE ||
      // or path is part of allowed outsiders
      ($_mirabel_accepteduri && preg_match($_mirabel_accepteduri, $path))
    ) && (
      // no exclusion
      ! $_mirabel_refuseduri ||
      // (or not part of excluded)
      ! preg_match($_mirabel_refuseduri, $path)
    )
  );
}
