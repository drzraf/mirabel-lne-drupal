<?php
// args: <fids>

// IMAGE INLINING
// see media.browser.inc
// contains media_browser();


// but see 
// http://drupalcode.org/project/migrate_extras.git/blob/refs/heads/7.x-2.x:/media.inc
// array() struct + json_encode()

//$result = $query->entityCondition("entity_type", "node")->propertyCondition("type", "article")->execute();

module_load_include("inc", "media", "includes/media.browser");

$argv = array_filter(drush_get_arguments(),
		     create_function('$val', 'return is_numeric($val);'));
$fids = array_values(array_unique($argv));

if(! $fids)
  $fids = array("202");

/* way1 */
/*
$files = file_load_multiple($fids);
foreach ($files as $file) {
  media_browser_build_media_item($file);
}
drupal_json_output(array('media' => array_values($files)));
*/

// way 2:
$_GET['fid'] = $fids;
media_browser();
// generate Drupal json settings strings
echo drupal_get_js('header');

// | sed -i '/^</d'

/*
// optionnal prefix:
var jsdom = require('jsdom');                                                                                                                      var window = document = jsdom.createWindow()

// prefix
var $ = require('jQuery');

// then; with context
require('drupal.js');
require('wysiwyg.js');

// otherwise
Drupal = {};
Drupal.media = Drupal.media || {};
*/

// Drupal.wysiwyg.plugins.media.insertMediaFile()
/*
function insertMediaFile (mediaFile, viewMode, formattedMedia, options, wysiwygInstance) {

    this.initializeTagMap();
    // @TODO: the folks @ ckeditor have told us that there is no way
    // to reliably add wrapper divs via normal HTML.
    // There is some method of adding a "fake element"
    // But until then, we're just going to embed to img.
    // This is pretty hacked for now.
    //
    var imgElement = $(this.stripDivs(formattedMedia));
    this.addImageAttributes(imgElement, mediaFile.fid, viewMode, options);

    var toInsert = this.outerHTML(imgElement);
    // Create an inline tag
    var inlineTag = Drupal.wysiwyg.plugins.media.createTag(imgElement);
    // Add it to the tag map in case the user switches input formats
    Drupal.settings.tagmap[inlineTag] = toInsert;
    wysiwygInstance.insert(toInsert);
}
*/



// then
// NODE_PATH=/usr/local/lib/node_modules node init.js

/*	@see
<img class="media-image img__fid__259 img__view_mode__media_original" alt="" src="http://beta.mirabel-lne.com/sites/beta.mirabel-lne.com/files/aerm100_0.jpg">

	@see: 
select * from drupal_beta_field_data_body where entity_id = 217;
*/




?>
