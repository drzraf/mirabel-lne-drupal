<?php

/*
  later:
  - deals with jplayer
  - the long-term <video> markup to use
  - + tinymce compat'


@see http://groups.drupal.org/node/8707

@bug http://drupal.org/node/1210344

#MediaElement / Media » Issues
#Issue getting Media, Styles and WYSIWYG working together with MediaElement.js
@bug http://drupal.org/node/1062948   ! need fix !

#AudioField » Issues
#Future support for media integration?
@http://drupal.org/node/1218330

#Drag'n'Drop Uploads » Issues
@see http://drupal.org/node/667934
@see http://drupal.org/node/800258

#see
http://drupal.org/project/html5_tools
http://drupal.org/project/elements

@imgassist:
http://drupal.org/node/841568

#Issues
#Support Uploading Multiple Files for HTML5 Browsers
http://drupal.org/node/625958

@see http://drupal.org/project/webform
@see http://drupal.org/project/webfm
@see http://drupal.org/project/imce
@see http://drupal.org/project/imce_wysiwyg
@see http://drupal.org/project/inline

#video
@bug http://drupal.org/node/1041004
@bug http://drupal.org/node/1216206

*/

require_once('config.php');
require_once('utils.php');
require_once('node-utils.php');
require_once('file-utils.php');
require_once('media-utils.php');
require_once('voc-utils.php');
require_once('asso-utils.php');

ini_set('include_path', ini_get('include_path') . ':/root');
require_once('connect-mirabel.php');
Database::addConnectionInfo('raw', 'default', $other_database);

// useful only if php cli dropped to www-data priv'
$dest = MIRABEL_DESTDIR . '/audio/';
if(! file_prepare_directory($dest))
  die("$dest can't be toyed");
$dest = MIRABEL_DESTDIR . '/pictures/';
if(! file_prepare_directory($dest))
  die("$dest can't be toyed");

if(! file_exists(BASEPATH))
  die("problem with existence of " . BASEPATH);

$taxo_id = array_pop(taxonomy_get_term_by_name('Article'))->tid;

global $_mirabel_vocs, $_mirabel_catchall_voc;
$_mirabel_catchall_tree = taxonomy_get_tree(
  taxonomy_vocabulary_machine_name_load($_mirabel_catchall_voc)->vid
);

$tables = array( '54', '55', '57', '88', 'adelp', 'lne' );
$count_article = 0;
foreach($tables as $tcode) {

  db_set_active('raw');
  $res = db_select('articles_' . $tcode, 'a')
    ->fields('a')
    //->condition('id_article', 24, '=')
    //->range(0,1)
    ->execute();
  db_set_active();

  //print $query->countQuery();

  foreach ($res as $key => $input) {
    $node = new stdClass();
    $node->type = 'article';
    $node->language = 'fr';
    $node->name = 'admin';
    $node->promote = '1';
    $node->comment = '1';
    $node->path['pathauto'] = '0';
    $node->path['alias'] = '';

    // for updates
    //$node->nid = 10;
    //$node->created = time();
    node_object_prepare($node);

    $node->title = $input->titre_article;
    echo '==== ' . $node->title . ' (' . $input->id_article . ") / ${tcode}\n";
    // disabled for now
    if(($nid = _mirabel_article_exists($node->title)) ) {
      drush_log('already exists under NID ' . $nid, 'warning');
      continue;
    }

    $node->body[$node->language][0]['format']  = 'full_html';

    $texte = $input->texte_article;
    $texte = preg_replace('!(\\n+)!', "<br/>", $texte);
    _mirabel_dom_tidy_start($texte);
    $html = preg_replace('!<head>!i', "<head>\n<meta http-equiv=\"content-type\" content=\"charset=utf-8\" />", $texte);

    $dom = new DOMDocument();
    $dom->preserveWhiteSpace = false;
    $dom->loadHTML($html);

    foreach($dom->getElementsByTagName('a') as $atag)
      $atag->removeAttribute('target');

    if( ($image = _mirabel_article_attach_image($input->photo_article, $tcode)) ) {
      // needed for inline display ...
      $image->display = 1;
      $node->field_fichier[$node->language][] = (array)$image;
    }

    // attach $texte to $node->body[$node->language][0]['value']
    // but also attach files to the node
    _mirabel_media_objects_attach($node, $dom);
    _mirabel_process_article_links($dom);

    $html = $dom->saveHTML($dom->getElementsByTagName('body')->item(0));
    $html = preg_replace(';^</?body>$;im', '', $html);

    // no compaturi for articles, add a comment about this into the body's
    $html .= "\n" . sprintf('<!-- compaturi: db=%s, record=%d -->', $tcode, $input->id_article);

    $node->body[$node->language][0]['value'] = $html;

    $author = '';
    if(preg_match('/CLCV|Flore 54|PAVE|EDEN|Amis de la Terre|F54|ADELP/',
		  $texte, $author_match)) {
      $i=0;
      while(! $author && isset($author_match[$i])) {
	if(preg_match('/clcv|pave|eden|adelp/i', $author_match[$i]))
	  $author = strtolower($author_match[$i]);
	elseif(preg_match('/ami/i', $author_match[$i]))
	  $author = 'at55';
	$i++;
      }
    }
    if(! $author)
      $author = 'flore54';

    //print "attached: author: " . $author . "\n";
    //echo $texte . "\n"; 
    //var_dump($node->created, $input->date_article); continue;

    $node->field_territoire[LANGUAGE_NONE][0]['value'] = _mirabel_assoc_cp_to_dep($tcode);

    $tid_applied = _mirabel_match_article_with_terms(
      $input->titre_article . ' ' . $input->texte_article,
      $_mirabel_vocs[0], // tags
      3
    );
    sort($tid_applied);
    $node->field_tags[LANGUAGE_NONE] = $tid_applied;

    $tid_applied = _mirabel_match_article_with_terms(
      $input->titre_article . ' ' . $input->texte_article,
      $_mirabel_vocs[1], // categories
      1
    );
    sort($tid_applied);
    $node->field_tagscatun[LANGUAGE_NONE] = $tid_applied;

    $tid_applied = _mirabel_match_article_with_terms(
      $input->titre_article . ' ' . $input->texte_article,
      $_mirabel_catchall_tree, // catchall (if this voc already contains terms)
      2
    );
    sort($tid_applied);
    $node->field_tagsup[LANGUAGE_NONE] = $tid_applied;

    $node->field_content_type[LANGUAGE_NONE][]['tid'] = $taxo_id;
    $node->date = $input->date_article;
    // set created using strtotime()
    node_submit($node);
    // submit resets $node->created...
    //$node->created = strtotime($input->date_article);

    //$node->nid = 8;
    //$node->vid = 7;

    //print drupal_render($mira_art_attr[0]);die();

    global $_mirabel_dry_run;
    if(! $_mirabel_dry_run) {
      node_save($node);
      printf("== save: %d\n", $node->nid);
    }
    $count_article++;
  } // end each record for a table
} // end each table

db_query('UPDATE {node} SET changed = created');
die("$count_article articles inserted\n");



// see _mirabel_file_ref_subst
function _mirabel_node_ref_subst($dom, $current_text, $oid) {
  $newtxt = '[oid:' . $oid;
  if($current_text)
    $newtxt .= ('=' . $current_text . ']');
  else
    $newtxt .= ']';

  return $dom->createTextNode($newtxt);
}

// see _mirabel_process_links
function _mirabel_process_article_links(&$dom) {
startproc:
  $xmlNodes = $dom->getElementsByTagName('a');
  foreach($xmlNodes as $xmlNode) {
    $lnktxt = $lnkdst = '';
    if($xmlNode->nodeValue)
      $lnktxt = trim(preg_replace("/\s+/s", ' ', $xmlNode->nodeValue));

    $href = $xmlNode->getAttribute('href');
    $lnkdst = _mirabel_abspath($href);
    if($lnkdst{0} != '/')
      continue;
    //pad("found <a> $lnktxt ($href) / $lnkdst");

    $newnode = _mirabel_node_ref_subst($dom, $lnktxt, $lnkdst);
    $xmlNode->parentNode->replaceChild($newnode, $xmlNode);
    goto startproc;
  }
}

/* 
Add fields programmatically:
http://www.heididev.com/programmatically-creating-video-fields
<p><video autoplay="autoplay" controls="controls" height="160" preload="auto" width="240"><source src="file:///tmp/0606-1437-HelicoCoquelicots.avi" type="video/mp4">&nbsp;</source></video></p>
*/
