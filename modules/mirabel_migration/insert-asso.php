<?php

require_once('config.php');
require_once('asso-utils.php');
require_once('voc-utils.php');

/*
  alpha, nom, statut

  done:
  sigle, web, logo
  departement, rue, cp, ville

  contact

  (ident ?)
  tel

  TODO: "fax", "email"

  themes, links, home, groups, mél, ...

  specific code
  "themes" => array( "themes",	"",	"" ),
  "themes" are split between "field_assoc_tag_<X>"
*/


/*
  won't work with
  drush @beta --php-options="-d $(php -i|sed -n '/^include_path/s; =>.*=> ;=/root:;p')" scr
*/
ini_set('include_path', ini_get('include_path') . ':/root');
require_once('connect-mirabel.php');
Database::addConnectionInfo('raw', 'default', $other_database);

$dest = MIRABEL_DESTDIR . '/logos-asso/';
if(! file_prepare_directory($dest))
  die("$dest can't be toyed");

// catchall vocabulary
$vid_insert = taxonomy_vocabulary_machine_name_load($_mirabel_catchall_voc)->vid;
global $_mirabel_catchall_tree;
$_mirabel_catchall_tree = taxonomy_get_tree($vid_insert);

foreach(array('Éducation', 'Territoire') as $term) {
  if(array_pop(taxonomy_get_term_by_name($term))) {
    continue;
  }
  $oterm = (object)array('name' => $term, 'vid' => $vid_insert);
  if(! $_mirabel_dry_run)
    taxonomy_term_save($oterm);
}

// output
$nodes = $queued = array();

// templace
$nodeo = new stdClass();
$nodeo->type = 'assoc';
$nodeo->language = 'fr';
$nodeo->name = 'admin';
$nodeo->comment = '1';
$nodeo->promote = 0;
$nodeo->path['pathauto'] = '0';
$nodeo->path['alias'] = '';

$byname = $byweb = $bysigle = array();
_mirabel_asso_loadcsv("/tmp/new-asso-import.csv", $byname, $byweb, $bysigle);
$bynamedb = $bywebdb = $bysigledb = array();
db_set_active('raw');
// debug: ->condition('alpha', 'C')
$res = db_select('annuaire', 'a')->fields('a')->execute();
db_set_active();
_mirabel_asso_loaddb($res, $bynamedb, $bywebdb, $bysigledb);

/*
  all nodes from the most current data: the CSV
*/
foreach($byname as $node2) {
  $node = $nodeo;
  node_object_prepare($node);
  $node = (object) array_merge((array) $node, (array) $node2);

  $quicktitle = strtolower(drupal_clean_css_identifier($node->title));
  $quicksigle = _mirabel_asso_clean_sigle($node->field_sigle['fr'][0]['value']);
  $web = FALSE;
  if(isset($node->field_web))
    $web = preg_replace(';http://([^/]*).*$;', '$1', $node->field_web['fr'][0]['value']);

  // match from the old database which contains tags and image,
  // let's grab them
  if(isset($bynamedb[$quicktitle]) || ($web && isset($bywebdb[$web])) || ($quicksigle && isset($bysigledb[$quicksigle]))) {
    if(isset($bynamedb[$quicktitle])) {
      $nodei = $bynamedb[$quicktitle];
      echo "OK: {$node->title}\n";
    }
    elseif($web && isset($bywebdb[$web])) {
      $nodei = $bywebdb[$web];
      echo "OK: {$node->title}: web: {$web}\n";
    }
    elseif($quicksigle && isset($bysigledb[$quicksigle])) {
      $nodei = $bysigledb[$quicksigle];
      echo "OK: {$node->title}: sigle: {$quicksigle}\n";
    }
    $node->field_tags[LANGUAGE_NONE] = $nodei->field_tags[LANGUAGE_NONE];
    if($nodei->field_assoc_image)
      $node->field_assoc_image = $nodei->field_assoc_image;
  }

  $queued[$quicktitle] = true;
  $nodes[] = $node;
}

/*
  additional asso from the old DB
*/
foreach($bynamedb as $node2) {
  $node = $nodeo;
  node_object_prepare($node);
  $node = (object) array_merge((array) $node, (array) $node2);

  $quicktitle = strtolower(drupal_clean_css_identifier($node->title));
  $quicksigle = _mirabel_asso_clean_sigle($node->field_sigle['fr'][0]['value']);
  $web = FALSE;

  if( isset($byname[$quicktitle]) ||
      ($web && isset($byweb[$web])) ||
      ($quicksigle && isset($bysigle[$quicksigle]))) {
  }
  else {
    echo "ADD: {$node->title}\n";
    $nodes[] = $node;
  }

}

foreach($nodes as $node) {
  echo "==== " . $node->title . "\n";
  //continue;
  $fake_form = array();
  node_validate($node, NULL, $fake_form);

  //var_dump($node);die();//continue;
  if(form_get_errors()) {
    _drush_log_drupal_messages();
    form_clear_error();
    continue;
  }

  node_submit($node);
  //continue;
  /* setup uid + validated */
  if(! $_mirabel_dry_run)
    node_save($node);
}


function _mirabel_asso_loaddb($db, &$byname, &$byweb, &$bysigle) {
  global $_mirabel_vocs, $_mirabel_catchall_tree;

  foreach ($db as $input) {
    $node = new stdClass();
    $node->title = $input->nom;
    $title2 = strtolower(drupal_clean_css_identifier($node->title));

    $node->field_sigle['fr'][0]['value'] = $input->sigle;
    $node->field_web['fr'][0]['value'] = $input->web;
    $node->field_assoc_image = array();

    if( ($image = _mirabel_asso_attach_image($input->logo)) )
      $node->field_assoc_image['fr'][0] = $image;

    $node->field_assoc_address[LANGUAGE_NONE][0] =
      array(
	'country' => 'FR',
	'administrative_area' => NULL,
	'locality' => $input->ville,
	'postal_code' => $input->cp,
	'thoroughfare' => $input->rue,
	'name_line' => $input->contact,
	'premise' => ''
      );

    $node->field_assoc_adhesion['fr'][0] = '0';

    // TODO: tetras vosges: departement is NULL => should be 88
    if($input->nom == 'GROUPE TETRAS VOSGES')
      $input->cp = 88;
    $node->field_territoire[LANGUAGE_NONE][0]['value'] = _mirabel_assoc_cp_to_dep($input->cp);
  
    if($input->email && $input->sigle) {
      $sigle = _mirabel_asso_clean_sigle($input->sigle);
      //_mirabel_create_user_from_sigle($sigle, $input->email);
    }

    $words = explode(',' , $input->themes);
    setlocale(LC_CTYPE, 'fr_FR.UTF-8');
    $words = array_map(function($word) {
	$word = preg_replace(array( '/^risques.*/' ),
			     array( 'industrie' ),
			     str_replace('?', 'e', trim($word)));
	return strtolower(iconv('UTF-8', 'ASCII//TRANSLIT', $word));
      },
      $words);
    setlocale(LC_CTYPE, 'C');

    $tid_applied = _mirabel_assoc_match_word_with_terms($words, $_mirabel_vocs);
    sort($tid_applied);
    $node->field_tags[LANGUAGE_NONE] = $tid_applied;
    
    $tid_applied = _mirabel_assoc_match_word_with_terms($words, array($_mirabel_catchall_tree));
    sort($tid_applied);
    $node->field_tagsup[LANGUAGE_NONE] = $tid_applied;


    $web2 = preg_replace(';http://([^/]*).*$;', '$1', $input->web);
    $byweb[$web2] = $node;
    $bysigle[$sigle] = $node;
    $byname[$title2] = $node;
  }
}

function _mirabel_asso_loadcsv($file, &$byname, &$byweb, &$bysigle) {
  $row = 1;

  if (($handle = fopen($file, 'r')) === FALSE)
    exit(1);

  while (($data = fgetcsv($handle, 5000, "\t")) !== FALSE) {
    $num = count($data);
    //echo "== $num fields in line $row:\n";
    if($num != 13) {
      var_dump($data);
      die();
    }
    $row++;
    if($row == 2)
      continue;
    $asso = new stdClass();

    $asso->title = $data[0];
    $title2 = strtolower(drupal_clean_css_identifier($asso->title));

    foreach(explode(',', $data[1]) as $v)
      $asso->field_territoire[LANGUAGE_NONE][]['value'] = _mirabel_assoc_cp_to_dep(trim($v));
    //$asso->territoire = $data[1];

    $sigle = $data[2];
    $sigle2 = _mirabel_asso_clean_sigle($sigle);
    $asso->field_sigle['fr'][0]['value'] = $data[2];

    $web = $data[7];
    if($web)
      $asso->field_web['fr'][0]['value'] = trim(trim($web), '/');
    $web2 = preg_replace(';http://([^/]*).*$;', '$1', $web);

    $mail = $data[10];
    if($mail == 'I')
      $mail = $data[3];
    $mail = trim($mail);

    $tel = $data[11];
    if($tel == 'I')
      $tel = $data[6];
    $tel = str_replace('.', ' ', $tel);

    $adresse = $adresseO = $data[12];
    if($adresse == 'I')
      $adresse = $data[5];

    $asso->field_assoc_adhesion['fr'][0] = $data[9];
    if($mail)
      $asso->field_web['fr'][1]['value'] = trim($mail);

    if(isset($adresse)) {
      $cp = preg_replace('/.* ([0-9 ]{5,6}).*/', '$1', $adresse);
      $cp = trim(preg_replace('/ /', '', $cp));

      $ville = trim(preg_replace('/.* [0-9 ]{5,6}[, ](.*)$/', '$1', $adresse));
      $ville = ucwords(strtolower($ville));

      $street = trim(preg_replace('/^(.*)\d{2} *\d{3}[ ,].+$/', '$1', $adresse), ' ,-–+');
      $street = preg_replace('/\s+/', ' ', $street);
      if($adresseO == 'I' && $data[4])
	$add_name = $data[4];
      else
	$add_name = '';

      $asso->field_assoc_address[LANGUAGE_NONE][0] =
	array(
	  'country' => 'FR',
	  'administrative_area' => NULL,
	  'locality' => $ville,
	  'postal_code' => $cp,
	  'thoroughfare' => $street,
	  'name_line' => $add_name,
	  'premise' => $tel
	);

    }
    $byname[$title2] = $asso;
    if($web2)
      $byweb[$web2] = $asso;
    if($sigle2)
      $bysigle[$sigle2] = $asso;
  }
  fclose($handle);
}
