// use with
// NODE_PATH=/usr/local/lib/node_modules node insert-attachment.js

jQuery = require('jQuery');
require('http://beta.mirabel-lne.com/misc/drupal.js?lsne0i');

jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"bartik","theme_token":"_8MoLz0Gfn59plFOBSxl8jk71d4nak5O1Z9KYaxtEL0","js":{"misc\/jquery.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"public:\/\/languages\/fr_Jg9lJkTvlxm2tACJfsSLFYQmn-kK5U9UD7J8PkJfE_M.js":1}},"media":{"selectedMedia":[{"fid":"202","uid":"0","filename":"05c601b8efe3d77966734222c9f680a1.jpg","uri":"public:\/\/pictures\/05c601b8efe3d77966734222c9f680a1.jpg","filemime":"image\/jpeg","filesize":"66298","status":"1","timestamp":"1317733995","type":"image","preview":"\u003cdiv class=\"media-item\"\u003e\u003cdiv class=\"media-thumbnail\"\u003e\u003cimg src=\"http:\/\/beta.mirabel-lne.com\/sites\/beta.mirabel-lne.com\/files\/styles\/square_thumbnail\/public\/pictures\/05c601b8efe3d77966734222c9f680a1.jpg\" alt=\"\" \/\u003e\u003cdiv class=\"label-wrapper\"\u003e\u003clabel class=\"media-filename\"\u003e05c601b8efe3d77966734222c9f680a1.jpg\u003c\/label\u003e\u003c\/div\u003e\u003c\/div\u003e\u003c\/div\u003e","url":"http:\/\/beta.mirabel-lne.com\/sites\/beta.mirabel-lne.com\/files\/pictures\/05c601b8efe3d77966734222c9f680a1.jpg"}]}});


console.log($.Drupal);
function insertMediaFile (mediaFile, viewMode, formattedMedia, options, wysiwygInstance) {

    this.initializeTagMap();
    // @TODO: the folks @ ckeditor have told us that there is no way
    // to reliably add wrapper divs via normal HTML.
    // There is some method of adding a "fake element"
    // But until then, we're just going to embed to img.
    // This is pretty hacked for now.
    //
    var imgElement = $(this.stripDivs(formattedMedia));
    this.addImageAttributes(imgElement, mediaFile.fid, viewMode, options);

    var toInsert = this.outerHTML(imgElement);
    // Create an inline tag
    var inlineTag = Drupal.wysiwyg.plugins.media.createTag(imgElement);
    // Add it to the tag map in case the user switches input formats
    Drupal.settings.tagmap[inlineTag] = toInsert;
    console.log(toInsert);
}


insertMediaFile(
    Drupal.settings.media[0],
    "media_original",
    formattedMedia.type,
    {"format": "media_original"},
    formattedMedia.options,
    NULL
);
