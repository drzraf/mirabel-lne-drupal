<?php

require_once('config.php');
require_once('file-utils.php');
require_once('voc-utils.php');

$dest = MIRABEL_DESTDIR . '/press/';
if(! file_prepare_directory($dest))
  die("$dest can't be toyed");

$in = fopen('php://stdin', 'r');
while( ( $line1 = fgets($in, 255) ) !== false ) {
  if( ( $line2 = fgets($in, 255) ) !== false ) {
    _mirabel_insert_press($line1, $line2);
    // empty line
    fgets($in, 10);
  }
  else
    break;
}
fclose($in);


function _mirabel_insert_press($line1, $line2) {
  static $taxo_id;
  $taxo_id = array_pop(taxonomy_get_term_by_name('Communiqué de presse'))->tid;

  // subject
  $subj1 = substr(trim($line1), 11);

  // test preexistence
  $query = new EntityFieldQuery;
  $result = $query
    ->entityCondition('entity_type', 'node')
    ->propertyCondition('type', 'article')
    ->propertyCondition('title', $subj1)
    ->range(0,1)->execute();
  if($result) {
    //echo "(II) node already exist: skip\n";
    return;
  }
  echo "== $subj1\n";  
  $node = new stdClass();
  $node->type = 'article';
  $node->language = 'fr';
  $node->name = 'admin';
  $node->comment = '1';
  $node->promote = '0';
  $node->path['pathauto'] = '0';
  $node->path['alias'] = '';
  node_object_prepare($node);


  $node->title = $subj1;
  $node->body[$node->language][0]['format']  = 'full_html';
  $node->body = array();
  // Communiqué de presse
  $node->field_content_type[LANGUAGE_NONE][]['tid'] = $taxo_id;

  // date
  $time1 = strptime($line1, "%d/%m/%y");
  $time2 = mktime(12, null, null, $time1['tm_mon'] + 1, $time1['tm_mday'], 1900 + $time1['tm_year']);

  // taxonomy
  global $_mirabel_vocs;
  $tid = _mirabel_match_article_with_terms($subj1, $_mirabel_vocs[0]); // tags
  if(empty($tid)) {
    // FIXME
    echo "no taxo: setting 0\n";
    $node->field_tags[LANGUAGE_NONE][]['tid'] = '0';
  }
  else
    $node->field_tags[LANGUAGE_NONE] = $tid;

  $tid2 = _mirabel_match_article_with_terms($subj1, $_mirabel_vocs[1]); // catégorie 1
  if($tid2) {
    $node->field_tagscatun[LANGUAGE_NONE] = $tid2;
  }

  $attachment_url = trim(trim($line2), "<>");
  //$file_url = str_replace(HTTPURL, BASEPATH, $attachment_url);
  $file_url = str_replace("HTTPURL", BASEPATH, $attachment_url);

  if(! file_exists($file_url)) {
    $file_url = str_replace("HTTPURL", BASEPATH . '/zancien/', $attachment_url); 
    if(! file_exists($file_url))
      trigger_error("problem with file $file_url\n", E_USER_ERROR);
  }

  $node->field_fichier[$node->language][0] = (array)_mirabel_press_attach_file($file_url);

  node_submit($node);
  // submit resets $node->created...
  $node->created = $time2;
  node_save($node);
}
