<?php

require_once('config.php');
require_once('fs-utils.php');
require_once('file-utils.php');
/*
  preg_replace_callback(
  ';<object[^>]*dewplayer.swf\?son=([^"&>]+)[^>]*>.*?</object>;',
  '_mirabel_article_attach_audio',
  $texte);
*/

function _mirabel_media_object_attach_callback($url) {
    if(! $url) {
      trigger_error("wrong url: $url.", E_USER_ERROR);
      return FALSE;
    }

    $file = _mirabel_article_create_audio_file($url);
    if(! $file) {
      trigger_error("not attached $url.", E_USER_ERROR);
      return FALSE;
    }
    return $file;
}

function _mirabel_dom_object_filename($domobj) {
  $url = $domobj->getAttribute('data');
  if($url)
    return $url;
  // embed
  $url = $domobj->getAttribute('src');
  if($url)
    return $url;
  
  $subdomobj = $domobj->getElementsByTagName('embed')->item(0);
  if($subdomobj)
    return _mirabel_dom_object_filename($subdomobj);

  foreach( $domobj->getElementsByTagName('param') as $param )
    if($param->getAttribute('name') == 'FileName')
      return $param->getAttribute('value');
  return FALSE;
}

function _mirabel_media_objects_attach(&$node, $dom) {
startproc:  
  foreach($dom->getElementsByTagName('object') as $aobj) {
    if(! ($url = _mirabel_dom_object_filename($aobj)) ) {
      trigger_error("can't get URL for object.", E_USER_ERROR);
      continue;
    }
    $url = preg_replace(';^dewplayer\.swf\?son=([^"&>]+).*$;', '$1', $url);
    //$height = $aobj->getAttribute('height');
    //$width = $aobj->getAttribute('width');

    if(! ($file = _mirabel_media_object_attach_callback($url)) )
       continue;

    $string = _mirabel_do_media_string($file);
    $new = $dom->createTextNode($string);
    $aobj->parentNode->replaceChild($new, $aobj);
    $file->display = 0;
    $node->field_fichier[$node->language][] = (array)$file;
    goto startproc;
  }
  
}

function _mirabel_do_media_string($file) {
  $image_info = array(
    'type' => 'media',
    'view_mode' => 'media_original',
    'fid' => $file->fid,
    'attributes' => array(
      'alt' => '',
      'title' => '',
      'class' => 'media-image',
      'typeof' => 'foaf:Image',
      'wysiwyg' => 1,
    ),
  );
  return '[[' . drupal_json_encode($image_info) . ']]';
}


function _mirabel_do_audio_string($file) {
  return '<audio controls="true">' .
    '<source src="' . file_create_url($file->uri) . '" type="' . $file->filemime . '" />' . "\n" .
    'Audio non-supporté par votre navigateur' .
    '</audio>';
}


/*
  find . -name *.mp3 > /tmp/mp3s.txt
  while read f; do
  # see http://mywiki.wooledge.org/BashFAQ/089
  	[[ ! -f "${f/.mp3/.ogg}" ]] && echo ffmpeg -i "$f" ${f/.mp3/.ogg}" < /dev/null && \
	ffmpeg -i "$f" -acodec libvorbis "${f/.mp3/.ogg}";
	done < /tmp/mp3s.txt

*/
function _mirabel_article_convert_audio($oldpath, $newpath) {
  drush_log("conversion: $oldpath => $newpath", "status");
  return system("ffmpeg -i '" . $oldpath . "' -acodec libvorbis '" . $newpath . '"');
}

function _mirabel_article_search_file($uri) {
  $query = new EntityFieldQuery;
  $result = $query
    ->entityCondition('entity_type', 'file')
    ->propertyCondition('uri', MIRABEL_DESTDIR . '/audio/' . basename($uri))
    ->range(0,1)
    ->execute();
  //    var_dump($result);die();
  if($result)
    return current(entity_load('file', array(key($result['file']))));
  return FALSE;
}

function _mirabel_media_change_extension($name) {
  return basename(
    preg_replace(
      array(";\.mp3$;", ";\.(wmv|asf)$;"),
      array(".ogg", ".ogv"),
      $name)
  );
}

function _mirabel_article_create_audio_file($local_uri) {
  $local_uri = _mirabel_basename($local_uri);
  $oggname = _mirabel_munge_filename(_mirabel_media_change_extension($local_uri));
  $dest = MIRABEL_DESTDIR . '/audio/' . $oggname;
  $plaindest = drupal_realpath($dest);

  $file = _mirabel_article_search_file($plaindest);
  if($file)
    return $file;

  if(file_exists($plaindest)) {
    $file = new stdClass();
    $file->uri = $dest;
    $file->{OLDPATH_FILEFIELD}[LANGUAGE_NONE][0]['value'] = $local_uri;
    pad(__FUNCTION__ . ": file_save {$file->uri}");
    $file = file_save($file);
    if($file)
      return $file;
    trigger_error("can't register $plaindest.", E_USER_ERROR);
    return FALSE;
  }

  // eg: BASEPATH/ogg/file.ogg if we ffmpeg somewhere else before scp'ing.
  $source = BASEPATH . '/ogg/' . _mirabel_media_change_extension($local_uri);
  if( file_exists($source) ) {
    $fileS = new stdClass();
    $fileS->uri = $source;
    pad("file_copy {$fileS->uri}");
    $filed = file_copy($fileS, MIRABEL_DESTDIR . '/audio/' . basename($fileS->uri), FILE_EXISTS_ERROR);
    // default (0 = TEMP) forbids fileS to appear in the media library
    $filed->status = FILE_STATUS_PERMANENT;
    // mime is not always correctly setup
    $filed->filemime = file_get_mimetype($filed->uri);
    $filed->{OLDPATH_FILEFIELD}[LANGUAGE_NONE][0]['value'] = $local_uri;
    pad("file_save {$filed->uri}");
    $filed = file_save($filed);
    if($filed) {
      echo "attached: ${source}\n";
      return $filed;
    }
    trigger_error("can't register $plaindest.", E_USER_ERROR);
    return FALSE;
  }

  trigger_error("no OGG source: $local_uri (" . basename($source) . ").", E_USER_WARNING);
  return FALSE;    

  /*
    $a = _mirabel_article_convert_audio($local_uri, $source);
    if($a)
    return __FUNCTION__($local_uri);
  */
  /*
  if( ! $a ) {
    if( file_exists(MIRABEL_DESTDIR . '/audio/' . basename($local_uri) ) ) {
      $file->uri = $local_uri;
      $result = $query->entityCondition('entity_type', 'file')
	->propertyCondition('uri', MIRABEL_DESTDIR . '/audio/' . basename($file->uri))
	->range(0,1)
	->execute();
      if($result)
	$a = current(entity_load('file', array(key($result['file']))));
      if(! $a) {
	$a = new Stdclass();
	$a->fid = NULL;
	$a->uri = MIRABEL_DESTDIR . '/audio/' . basename($local_uri);
	$a = file_save($file);
      }
    }
    if( ! $a && file_exists($local_uri) ) {
      $file->uri = $local_uri;
      $a = file_copy($file, MIRABEL_DESTDIR . '/audio/'. basename($file->uri), FILE_EXISTS_ERROR);
      $a->status = FILE_STATUS_PERMANENT;
      $a->filemime = file_get_mimetype($a->uri);
      file_save($a);
    }
    if( ! $a ) {
      global $_mirabel_warnings;
      drush_log("can't find file $local_uri", "warning");
      $_mirabel_warnings++;
      return NULL;
    }
  }
  // needed to attach this file as a File Field
  $a->display = 1;
  $a->description = basename($a->uri);
  */
}

// @see: http://drupal.org/node/368388
function _mirabel_article_attach_audio($matches) {
  $local_url = preg_replace(';' . HTTPURL . ';', BASEPATH, $matches[1]);
  $file = _mirabel_article_create_audio_file($local_url);
  return _mirabel_do_audio_string($a);
}