<?php

function mirabel_migration_drush_command() {
  $items = array();
  
  $items['mirabel-import'] = array(
    'description' => 'Mirabel Import Dossiers',
    'aliases' => array('imira'),
    'required-arguments' => TRUE,
    //'drupal dependencies' => 'mirabel_migration',
    'arguments' => array(
      'dossier'        => "dossier to import",
    )
  );

  return $items;
}

function drush_mirabel_migration_mirabel_import($dossier = NULL) {
  require_once('config.php');
  global $_mirabel_dossiers, $_mirabel_book_menu;

  if(is_null($dossier)) {
    drush_log('no dossier specified', 'error');
    return;
  }

  if(array_search($_mirabel_book_menu, variable_get('menu_options_book', array('main-menu'))) === FALSE) {
    trigger_error("{$_mirabel_book_menu} is not an allowed book menu, wouldn't be populated.", E_USER_ERROR);
    return;
  }

  if(is_numeric($dossier)) {
    $i = 0;
    while($i++ < min(intval($dossier), count($_mirabel_dossiers))) 
      next($_mirabel_dossiers);
    $result = array(key($_mirabel_dossiers) => current($_mirabel_dossiers));
  }
  elseif($dossier == 'all')
    $result = $_mirabel_dossiers;
  elseif(isset($_mirabel_dossiers[$dossier]))
    $result = array($dossier => $_mirabel_dossiers[$dossier]);
  else
    return;

  _mirabel_migration_operate_them($result);
  _mirabel_migration_finished(TRUE, NULL, NULL);
  return;

  // TODO: later
  $batch = array(
    'title' => t('Importing dossiers'),
    'operations' => array(
      array('_mirabel_migration_operate', $result),
    ),
    'finished' => '_mirabel_migration_finished',
  );

  batch_set($batch);
  $batch =& batch_get();
  var_dump($batch);die();
  $batch['progressive'] = FALSE;
  drush_backend_batch_process();
}
