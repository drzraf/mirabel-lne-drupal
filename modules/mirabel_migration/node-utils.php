<?php

/**
 * $book: value to copy into 'book[bid]' of the new node
 * fetchable from book_get_books()
 * eg, $book = 220;

 * $parent_page: value to copy into 'book[plid]' of the new node
 * fetchable from book_toc($book, $level)
 * (which provides the mlid of each pages in this $book)
 * eg, $parent_page = 1025;
 **/

function _mirabel_stub_node($abspath) {
  global $_mirabel_dry_run;

  $node = new stdClass();
  $node->type = 'book';
  $node->language = 'fr';
  $node->name = 'admin';

  node_object_prepare($node);
  $node->{OLDPATH_FILEFIELD}[LANGUAGE_NONE][0]['value'] = $abspath;

  if($_mirabel_dry_run) {
    // no title processing happens now
    // see _mirabel_update_node()
    $id = _mirabel_new_nid();
    $node->nid = $id;
  }
  else {
    node_save($node);
  }

  if(! $node->nid) {
    trigger_error("stub NOT inserted", E_USER_ERROR);
    die("\n");
  }

  return $node;
}

function _mirabel_config_node(&$node, $book, $parent_page) {
  global $_mirabel_dry_run;

  $node->body[$node->language][0]['format'] = 'full_html';
  // default value should be fine anyway
  $node->comment = "1";
  $node->path['pathauto'] = 0;
  $node->path['alias'] = '';
  // child ...
  if(! is_null($book)) {
    // integrate with an existing book
    $node->book['bid'] = $book;
    $node->book['plid'] = $parent_page;
  }
  // or mother of pages ?
  else {
    // registered on the front page
    $node->promote = 1;
    // now that we have a nid, we can create a book
    $node->book['bid'] = $node->nid;
    $node->book['plid'] = 0;
  }

  if($_mirabel_dry_run) {
    // fake mlid; try to avoid php notices
    $node->book['mlid'] = rand(2500, 3000);
  } else {
    // Drupal will create the mlid we will reuse for children
    node_save($node);
  }
    
  if(! isset($node->book['mlid']))
    die('mlid uncaught error');
}

function _mirabel_fetch_title(&$node, $dom, $html, $path) {
  /* basic title heuristic
     (remember that since stub_node(), $node->title is empty */
  $basename = basename($path);

  $title = $dom->getElementsByTagName('title');
  if($title->length >= 1) {
    $title = $title->item(0)->nodeValue;
  }
  if(! $title && preg_match('|<title>(.*?)</title>|i', $html, $title)) {
    $title = $title[1];
  }
  if(! $title && $basename)
    $title = preg_replace('/\..*?$/', '', $basename);
  if(! $title || $title == 'index' )
    $title = basename(dirname($path));
  if(! $title && ! $basename) {
    global $_mirabel_warnings;
    drush_log('no available title for ' . $path, 'warning');
    $_mirabel_warnings++;
    $title = $path;
  }

  $title = str_replace('_', ' ', $title);
  if(preg_match('/^[A-Z ]+$/', $title))
    $title = strtolower($title);
  $title = ucwords($title);
  
  if(! _mirabel_title_slug_free($title, $node->nid ) ) {
    $suffix = '-' . preg_replace('/\..*?$/', '', $basename);
    drush_log("title \"$title\" already exists; suff. \"$suffix\") !", 'status');
    $title .= $suffix;
  }
  if(! _mirabel_title_slug_free($title, $node->nid ) ) {
    $i = 0;
    while(! _mirabel_title_slug_free($title . '-' . $i, $node->nid ) && $i < 10) {
      drush_log("title \"{$title}-{$i}\" already exists !!", 'warning');
      $i++;
    }
    $title .= '-' . $i;
  }

  $node->title = $title;
  if($node->path['pathauto'] == 1) {
    //$node->path['alias'] = 'dossier/'.$node->title;
  }

}

function _mirabel_update_node(&$node, $file_path, $tags) {
  global $_mirabel_book_menu, $_mirabel_level, $_mirabel_pad;

  /* $_mirabel_level increment happens before we
     reach _mirabel_update_node() */
  if( $_mirabel_level == 1 ) {
    // for safety but default should be fine
    // _mirabel_stub_node() already promotes root nodes
    pad("promote");
    $node->promote = 1;

    // no API for menus, select already existing one
    $menuid = db_query("SELECT mlid FROM {menu_links} WHERE link_title = :title AND menu_name = :menu",
		       array(':title' => $node->title, ':menu' => $_mirabel_book_menu))->fetchAssoc();
    if(! $menuid ) {
      pad("create a new link in menu");

      $node->menu['enabled'] = 1;
      $node->menu['link_title'] = $node->title;
      //$node->menu['menu_parent'] = $_mirabel_book_menu . ':0';
      $node->menu['menu_name'] = $_mirabel_book_menu;
      $node->menu['description'] = '';
    }
  }
  else
    $node->promote = 0;

  // taxonomy terms
  if($tags) {
    global $_mirabel_vocs, $_mirabel_catchall_voc;
    static $vid_insert;
    $vid_insert = taxonomy_vocabulary_machine_name_load($_mirabel_catchall_voc)->vid;
    $nomatch = $nomatch2 = $newterms = array();
    // thématique environnement
    $node->field_tags[LANGUAGE_NONE] =
      _mirabel_match_words_with_voc($tags, $_mirabel_vocs[0], $nomatch);
    // try to match unmatched keywords against the 'other keyword' voc
    $node->field_tagscatun[LANGUAGE_NONE] =
      _mirabel_match_words_with_voc($nomatch, $_mirabel_vocs[1], $nomatch2);
    $newterms = _mirabel_voc_add_words($vid_insert, $nomatch2);
    // catchall
    $node->field_tagsup[LANGUAGE_NONE] = $newterms;
  }
  return $node;
}

// compaturi = OLDPATH_FILEFIELD
function _mirabel_compaturi_to_bid($abspath) {
  return intval(array_pop(db_query("SELECT entity_id FROM {field_data_compaturi} WHERE compaturi_value = :compat AND deleted = 0 AND bundle = 'book'",
				   array(':compat' => $abspath))->fetchCol()));
}

function _mirabel_book_load_by_old_path($abspath, $sum = NULL) {
  global $_mirabel_dry_run;
  $book = null;

  // dry run mode
  if($_mirabel_dry_run) {
    // search by path
    global $_mirabel_pages_done;
    if( ($refs = array_keys($_mirabel_pages_done, $abspath))) {
      if(! is_numeric(current($refs)))
	next($refs);
      $nid = current($refs);
      // only a sum, no NID
      if(!$nid) {
	//var_dump($_mirabel_pages_done, $refs, $abspath);
	//die(" no NID !");
	return null;
      }
      $book = new stdClass();
      pad("NODE XXX: '$abspath' already registered under NID " . $nid);
      $book->nid = $nid;
      return $book;
    }
    // search by SUM without inserting
    if( ($refs = array_keys($_mirabel_pages_done, $abspath))) {
      if(is_numeric(current($refs)))
	next($refs);
      $nid = current($refs);

      $book = new stdClass();
      pad("NODE XXX-SUM: \"$abspath\" already registered under NID " . $nid);
      $book->nid = $nid;
      return $book;
    }
    //return NULL;
    // let's fallback on database, even in dry-run mode
  }

  $query = new EntityFieldQuery;
  $result = $query
    ->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'book')
    ->fieldCondition(OLDPATH_FILEFIELD, 'value', $abspath)
    ->deleted(FALSE)
    ->range(0,1)
    ->execute();
  if(! empty($result)) {
    $nid = intval(current(current($result))->nid);
    if($nid) {
      $book = node_load($nid);
      if($book)
	pad("NODE: \"$abspath\" already registered under NID " . $nid );
      else {
	var_dump($result);
	trigger_error('entity without node ! die !', E_USER_ERROR);
	die();
      }
    }
    else {
      trigger_error('entity without node (bis) ! die !', E_USER_ERROR);
      var_dump($result);
      die();
    }
  }
  return $book;

}


function _mirabel_title_slug_free($title, $nid) {
  $query = new EntityFieldQuery;
  $ret = $query->entityCondition('entity_type', 'node')
    ->propertyCondition('type', 'book')
    ->propertyCondition('title', $title)
    ->propertyCondition('nid', $nid, '<>')
    ->deleted(FALSE)
    ->range(0,1)->execute();
  return empty($ret);
}

function _mirabel_article_exists($title) {
  $query = new EntityFieldQuery;
  if( ($result = $query
       ->entityCondition('entity_type', 'node')
       ->entityCondition('bundle', 'article')
       ->propertyCondition('title', $title)
       ->deleted(FALSE)
       ->range(0,1)
       ->execute() ) &&
      ($nid = intval(current(current($result))->nid)) &&
      node_load($nid) )
    return $nid;
  return FALSE;
}


function _mirabel_new_nid() {
  global $_mirabel_pages_done;
  $i = rand(200,1600);
  while (isset($_mirabel_pages_done[$i]))
    $i = rand(200,1600);
  return $i;
}

/**
 * $sub: array('node' => $subnode, 'uri' => $sub-node-path), see go() returned value
 * $main_node: node whose links have to be preg_replace'd()
 **/
/*
function attach_subnodes(&$main_node, $sub) {
  foreach($sub as $subnode) {
    $uri = $subnode['uri'];
    $subnode = $subnode['node'];

    $html = $main_node->body[$main_node->language][0]['value'];

    $main_node->body[$main_node->language][0]['value'] = preg_replace (
      '|' . $uri . '|' ,
      "XXXXXXXXXXXXXXXXXXX", //$subnode->nid,
      $html );
  }
}
*/

/* From http://drupalcode.org/project/book_delete.git/blob/HEAD:/book_delete.module
   eg:

   tmysql -N <<<"select DISTINCT bid FROM drupal_beta_book;" | while read f; do \
   drush @beta ev "_mirabel_book_delete($f);"; \
   done
   or to delete all by type:
   drush @beta en devel_generate
   drush @beta --kill --types=assoc genc 0
   drush @beta --kill --types=book genc 0
*/
function _mirabel_book_delete($bid) {
  if(!is_numeric($bid))
    return;
  $nids = db_query("SELECT nid FROM {book} WHERE bid = :bid AND nid <> bid ORDER BY nid ASC", array(':bid' => $bid))->fetchCol();

  if (!empty($nids)) {
    var_dump($nids);
    node_delete_multiple($nids);
    node_delete($bid);
  } else {
    node_delete($bid);
  } 
}
