<?php

function pad($str) {
  global $_mirabel_pad, $_mirabel_level;
  $lines = explode("\n", $str);
  foreach($lines as $v) {
    $prefix = '';
    if(strpos($v, '===') === 0)
      $prefix = $_mirabel_level;
    echo $prefix . str_pad($v . "\n", strlen($v) + 1 + $_mirabel_pad, " ", STR_PAD_LEFT);
  }
}

function _mirabel_dom_anchor_to_nid($nid, $txt = NULL) {
  $newtxt = ' [nid:' . $nid .  ( $txt ? '=' . $txt : '' ) . '] ';
  return $newtxt;
}

function repl_html_select($str) {
  $a = preg_replace('|<option value="([^"]+)"[^>]*>([^<]+)</option>|i',
		    '<li><a href="\1">\2</a></li>',
		    $str);
  $a = preg_replace('|^.*?<li>|ms', '<ul><li>', $a);
  $a = preg_replace('|</select>.*|ms', '</ul>', $a);
  return implode($a);
}


function _mirabel_clean_html(&$html) {
  $texte = preg_replace('!>(&nbsp;)+<!', '> <', $html);
  // no 'u' flag, it easily strips too much
  $texte = preg_replace('!>[\t\s]+<!', '> <', $texte);
  $texte = preg_replace('!background="(.*?)"!', 'style="background:$1"', $texte);
  // TODO: attach

  // because Drupal replace <body>'s "\n" by <br/> at
  // output time
  $texte = str_replace("\n", ' ', $texte);

  /*  $texte = preg_replace('!namo_npi="[^"]+"!i', '', $texte);
  $texte = preg_replace('|^.*?(<body)|is', '\1', $texte);
  $texte = preg_replace('|^(<object[\s].*?</object>)|mis', '', $texte);
  $texte = preg_replace('!onmouse(over|out)="namosw[^"]+"!i', '', $texte);
  $texte = preg_replace('!on(change|load)="namo[^"]+"!i', '', $texte);
  $texte = preg_replace('!(target="(_blank|centre|_self)")!i', '', $texte);
  $texte = preg_replace('|^(<script.*?</script>)|mis', '', $texte);
  $texte = preg_replace_callback('|<select\s.*?</select>|mis', 'repl_html_select', $texte);
  $texte = preg_replace('/^\n+|^[\t\s]*\n+/m','',$texte);*/

  $html = $texte;
}

function _mirabel_dom_tidy_start(&$html) {
  $config = array(
    'drop-proprietary-attributes' => true,
    'drop-empty-paras' => true,
    'logical-emphasis' => true,
    // because some URL contains double spaces
    'literal-attributes' => true,
    'fix-uri'	=> true,
    'clean'	=> true,
    'word-2000'	=> true,
    'wrap'	=> false,
    'quiet'	=> true,
    'hide-comments' => true,
    'css-prefix' => 'tidy',
    /* bare = false:
       echo '’'|tidy -b -utf8 2>/dev/null|sed -n '/body/{n;p;q}'|xxd -p
       http://tidy.sourceforge.net/docs/quickref.html#bare
       and
       http://tidy.sourceforge.net/docs/tidy_man.html
       are inconsistent
    */
  );
  _mirabel_dom_tidy($html, $config);
}

/*
  unused: not worst the cost,
  depends on whether literal-attributes affected the cleanup
 */
function _mirabel_dom_tidy_end(&$html) {
  $config = array(
    // commented because it implies numeric-entities=yes
    // 'doctype'	=> 'omit',
    'break-before-br'	=> true,
    'vertical-space'	=> true,
    'indent'	=> true,
    'markup'	=> true,
  );
  _mirabel_dom_tidy($html, $config);
}

function _mirabel_dom_tidy(&$html, $config = array()) {
  global $_mirabel_warnings;
  $tidy = new tidy;
  $tidy->parseString($html, $config, 'utf8');
  $tidy->cleanRepair();
  if($tidy->errorBuffer && (! isset($config['quiet']) || $config['quiet'] !== TRUE ) ) {
    trigger_error("tidy error: " . $tidy->errorBuffer, E_USER_WARNING);
    $_mirabel_warnings++;
  }
  $html = $tidy->value;
}

function _mirabel_helper_filter_links($arr) {
  $fx = create_function('$arg', 'return ($arg{0} != "#");');
  return array_unique(array_filter(array_filter($arr, $fx )));
}

function _mirabel_html_cleanup($dom) {
  global $_mirabel_dry_run;

  $bodies = $dom->getElementsByTagName('body');

  if($bodies->length != 1) {
    trigger_error("HTML error: 0 or multiple <body>", E_USER_ERROR);
    die();
  }

  // pretty print HTML
  if($_mirabel_dry_run) {
    $html = $dom->saveHTML();
    _mirabel_dom_tidy_end($html);
  } else {
    // useless if a DomNode is given to saveHTML
    // $dom->formatOutput = true;
    $html = $dom->saveHTML($bodies->item(0));

    // save the styles, in case...
    $style = '';
    $styles = $dom->getElementsByTagName('style');
    for($i = 0; $i < $styles->length; $i++)
      $style .= $dom->saveHTML($styles->item($i));
  }

  return array($html, $style);
}
