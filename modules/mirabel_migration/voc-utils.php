<?php
/*
  returns the vid of the vocabulary which is the default one
  for the $field_name attached to the "book" entity
  (which does not exist by default)
 */
function _mirabel_get_default_voc($field_name) {
  $vsettings = taxonomy_field_settings_form(
    field_info_field($field_name),
    field_info_instance('node', $field_name, 'book'),
    FALSE);
  $voc = taxonomy_vocabulary_machine_name_load(
    $vsettings['allowed_values'][0]['vocabulary']['#default_value']
  );
  return intval($voc->vid);
}

function _mirabel_get_field_of_voc($entity, $bundle, $voc_name) {
  foreach(field_info_instances($entity, $bundle) as $k) {
    if($k['display']['default']['module'] == 'taxonomy') {
      $g = field_info_field($k['field_name']);
      if($g['settings']['allowed_values'][0]['vocabulary'] == $voc_name)
	return $k['field_name'];
    }
  }
  trigger_error("vocabulary $voc_name is not part of $entity/$bundle");
  die();
  return FALSE;
}

function _mirabel_match_word_with_voc($word, $voc) {
  foreach ($voc as $oterm) {
    $term = strtolower(iconv('UTF-8', 'ASCII//TRANSLIT',$oterm->name));
    if(stripos($term, $word) !== FALSE)
      return $oterm;
  }
  return FALSE;
}

/**
 * $words: array of strings which may match existing terms
	(otherwise they will be inserted)
 * $voc_search: vocabulary existing terms are fetched from
 * $vid_insert: optional vocabulary where terms are inserted into if they
 	don't match any term from $vids_search

 **/
function _mirabel_match_words_with_voc($words, $voc, &$refused) {
  $applied = array();
  $refused = array();

  // XXX: see bug http://drupal.org/node/614124
  setlocale(LC_CTYPE, "fr_FR.UTF-8");
  foreach($words as $oword) {
    $word = strtolower(iconv('UTF-8', 'ASCII//TRANSLIT', $oword));
    $term = _mirabel_match_word_with_voc($word, $voc);
    if($term)
      $applied[$term->tid] = (array)$term;
    else
      $refused[] = $oword;
  }
  setlocale(LC_CTYPE, "C");
  return $applied;
}

function _mirabel_voc_add_words($vid_insert, $toadd) {
  global $_mirabel_dry_run;
  $applied = array();

  foreach($toadd as $word) {
    // shortcut for
    // $oterm = array_pop(taxonomy_get_term_by_name($word));
    $tid = db_select('taxonomy_term_data', 't')->fields('t', array('tid'))->condition('name', trim($word))->execute()->fetchField();
    if($tid) {
      $applied[]['tid'] = $tid;
      continue;
    }
    drush_log("voc: added \"{$word}\" to the catchall vocabulary", 'status');
    $oterm = (object) array(
      'name' => $word,
      'vid' => $vid_insert
    );
    if(! $_mirabel_dry_run)
      taxonomy_term_save($oterm);
    // avoid php notice in dry-run
    else
      $oterm->tid = rand(50,90);
    // associate this term with the node
    $applied[$oterm->tid] = (array)$oterm;
  }
  return $applied;
}

// Taxonomy for articles
function _mirabel_match_article_with_terms($string, $voc, $limit = 3) {
  $tid_applied = $terms_found = array();

  setlocale(LC_CTYPE, "fr_FR.UTF-8");

  $string = iconv('UTF-8', 'ASCII//TRANSLIT', $string);
  $string = preg_replace(
    array('/massif|Haye/i', '/PLU|foncier/', '/maladie/', '/agrocarburant/i', '/carburant|petrole/i',
	  '/(incineration|ordures|biodegradable|ecolabel|nettoy)/i',
	  '/(cycl|velo|mobilite|autorout|fluviale|canal)/i',
	  '/sdage/i', '/hambregie|gaz/i', '/Villoncourt/', '/ulcos|canicule/i', '/(forum|conference)/i',
	  '/micheville|belval/i', '/photovoltaique/i'
    ),
    array(' foret ', ' amenagement territoire ', ' sante ', ' agriculture climat ', ' climat transport ',
	  ' dechets ',
	  ' transport ',
	  ' eau ', ' energie climat ', ' eau déchets ', ' climat ', ' formations ',
	  ' milieux naturels ', 'energie',
    ),
    $string);

  foreach ($voc as $oterm) {
    $ascii_term = strtolower(iconv('UTF-8', 'ASCII//TRANSLIT', $oterm->name));
    if(preg_match("![\s']*${ascii_term}!i", $string) && ! isset($terms_found[$oterm->name]) ) {
      $terms_found[$oterm->name] = TRUE;
      //echo "=> $oterm->name (voc $vocid) found" . "\n";
      //$tid_applied[$vocid][] = $oterm->tid;
      $tid_applied[] = array('tid' => $oterm->tid);
    }
    if(count($terms_found) == $limit) break;
  }

  if(count($terms_found) == 0) {
    //drush_log("voc: no matching term for this article", "status");
    //echo "voc: no matching term for this article\n";
  }
  else {
    echo '[' . implode(', ', array_keys($terms_found)) . "]\n";
  }

  setlocale(LC_CTYPE, "C");
  return $tid_applied;
}


// Taxonomy
function _mirabel_assoc_match_word_with_terms(&$words, $voc_search) {
  setlocale(LC_CTYPE, 'fr_FR.UTF-8');

  $applied = array();
  // pour chaque theme de la DB originale (annuaire->themes)...
  foreach ($words as $i => $word) {
    $found = false;
    // on cherche dans chaque vocabulaire
    foreach ($voc_search as $voc) {
      $terms = $voc;
      // si l'un de ses termes
      foreach ($terms as $oterm) {
	$term = strtolower(iconv('UTF-8', 'ASCII//TRANSLIT',$oterm->name));
	// peut matcher
	$ascii_word = $word;
	$word_length = strlen($ascii_word);
	if($word_length > 8 && $ascii_word{$word_length-1} == 's')
	  $ascii_word = substr($ascii_word, 0, -1);

	// dans ce cas,
	if($ascii_word == $term ||
	   ( ! in_array($term, array('eau','air')) && (
	     stripos($term, $ascii_word) !== FALSE ||
	     stripos($ascii_word, $term) !== FALSE   )
	   )
	) {
	  //echo "$ascii_word <=> $term (vid={$oterm->vid})" . "\n";
	  array_splice($words, $i, 1);
	  $found = true;
	  // on ajoute ce tid à la liste des termes à appliquer à l'asso
	  //$applied[$vocid][] = $oterm->tid;
	  //$applied[$oterm->tid] = (array)$oterm;
	  $applied[] = array('tid' => $oterm->tid);
	  break;
	}
      }
      if($found)
	break;
    }
    //if(!$found) drush_log("no match for \"$word\" in any vocabulary", 'status');
  }

  setlocale(LC_CTYPE, 'C');

  return $applied;
}
