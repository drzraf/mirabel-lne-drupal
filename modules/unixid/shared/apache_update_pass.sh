#!/bin/bash

# apache_update_pass.sh <login> <password>
#  set password to <password> for the unix account <login>
# if successful, update the passwd in the samba database too

[[ -z $1 ]] && exit 1
# see NAME_REGEX in /etc/adduser.conf
[[ $1 =~ ^[a-z][a-z0-9-]+$ ]] || exit 1
login=$1 && shift

# exit if the account does not exists
getent passwd "$login" &> /dev/null || exit 1

# exit if the UID of the account if <= 1000
let id=$(id -u "$login")
(( $id > 1000 )) || exit 1

# exit if the user is not already in the "sambashare" group
# @see apache_useradd.sh
[[ $(groups "$login") =~ sambashare ]] || exit 1

# exit if no password is provided
[[ -z $1 ]] && exit 1

# TODO:
# exit if ! -w /etc/shadow && no sudo

plain_passwd="$1" && shift
passwd=$(printf %s "$plain_passwd"|mkpasswd -s -m sha-256)

usermod -p "$passwd" "$login"
let uret=$?

if (( $uret == 0 )); then
    let sret=1
    # if the Samba database management utility is here
    if type -P pdbedit smbpasswd &> /dev/null && pdbedit -L -u "$login" &> /dev/null; then
	printf "%s\n%s\n" "$plain_passwd" "$plain_passwd"|smbpasswd -s "$login" &> /dev/null
	let sret=$?
    fi

    email="unix password updated"
    if (( $sret == 0 )); then
	email+="\nsamba password updated"	
    else
	email+="\nsamba password NOT updated"
    fi

    email+="\n($(date +'%F %X'))"
    echo -e "$email"|mail -s "\"$login\" password change" root@localhost
fi

exit $uret
