#!/bin/bash

# apache_user_endis.sh enable|disable <login>
#  set password to <password> for the unix account <login>
# if successful, update the passwd in the samba database too

[[ -z $1 ]] && exit 1
[[ $1 != enable && $1 != disable ]] && exit 1
action=$1 && shift

[[ -z $1 ]] && exit 1
# see NAME_REGEX in /etc/adduser.conf
[[ $1 =~ ^[a-z][a-z0-9-]+$ ]] || exit 1
login=$1 && shift

# exit if the account does not exists
getent passwd "$login" &> /dev/null || exit 1

# exit if the UID of the account if <= 1000
let id=$(id -u "$login")
(( $id > 1000 )) || exit 1

# exit if the user is not already in the "sambashare" group
# @see apache_useradd.sh
[[ $(groups "$login") =~ sambashare ]] || exit 1

if [[ $action = disable ]]; then
    passwd -l "$login"
    echo|mail -s "\"$login\" access locked" root@localhost
elseif [[ $action = enable ]]; then
    passwd -u "$login"
    echo|mail -s "\"$login\" access unlocked" root@localhost
else
    exit 1
fi

exit 0
