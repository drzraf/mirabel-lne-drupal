#!/bin/bash

# apache_useradd.sh <login> <password>
#  create the unix account <login> using the password <password>
# if successful, create an entryin the samba database too

[[ -z $1 ]] && exit 1
# see NAME_REGEX in /etc/adduser.conf
[[ $1 =~ ^[a-z][a-z0-9-]+$ ]] || exit 1
login=$1 && shift

# exit if the account does exists already
getent passwd "$login" &> /dev/null && exit 1

# exit if no password is provided
[[ -z $1 ]] && exit 1

# TODO:
# exit if ! -w /etc/shadow && no sudo

plain_passwd="$1" && shift
passwd=$(printf %s "$plain_passwd"|mkpasswd -s -m sha-256)

comment="drupal account $(date +%F-%H-%M)"

# TODO: specific "drupal" group ?
# note: sshd_config contains "DenyGroups nogroup sambashare"
useradd -M -N -b /srv/ftp -g nogroup \
    -G sambashare -c "$comment" -s /usr/sbin/nologin \
    -p "$passwd" "$login"
let uret=$?

if (( $uret == 0 )); then
    uid=$(id -u $login)
    email="unix account \"$login\" ($uid) [$comment] created"

    # if the Samba database management utility is here
    if ! type -P pdbedit &> /dev/null; then
	email+="\nno samba pdbedit utility"
    elif pdbedit -L -u "$login" &> /dev/null; then
	email+="\nsamba account already exists..."
    else
	printf "%s\n%s\n" "$plain_passwd" "$plain_passwd"|pdbedit -a -t -u "$login" &> /dev/null
	let sret=$?

	if (($sret == 0)); then
	    email+="\nsamba account created"
	else
	    email+="\nsamba account NOT created"
	fi
    fi

    echo -e $email|mail -s "unix account \"$login\": success" root@localhost
else
    echo "unix account creation failed: returned $uret"|mail -s "unix account \"$login\": failure" root@localhost
fi

exit $uret
