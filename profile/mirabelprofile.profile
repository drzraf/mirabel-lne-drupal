<?php

// see
// https://drupal.org/node/180078
// http://api.drupal.org/api/drupal/developer--example.profile/7/source
// http://drupalcode.org/project/drupalorg_testing.git/blob/HEAD:/drupalorg_testing.profile
// http://drupalcode.org/project/indymedia_alba.git/blob/HEAD:/indymedia_alba.profile
// http://drupalcode.org/project/drupal_democracy_forum.git/blob/HEAD:/drupal_democracy_forum.profile
// https://github.com/rocksoup/art_profile/blob/master/art.profile

function mirabelprofile_install_tasks() {
  return array(
    '_mirabelprofile_profile_theme_task' => array(),
    '_mirabelprofile_profile_config_task' => array(),
  );
}

function _mirabelprofile_profile_theme_task() {
  $theme_settings = variable_get('theme_settings', array());
  $theme_settings['default_logo'] = '0';
  $theme_settings['logo_path'] = 'sites/default/files/lne_1.png';
  $theme_settings['default_favicon'] = '0';
  $theme_settings['favicon_path'] = 'sites/default/files/favicon_0.ico';
  
  variable_set('theme_mirabel_theme_settings', $theme_settings);
  theme_enable(array('mirabel_theme'));

  //module_enable(_mirabelprofile_profile_modules(), TRUE);
  //module_disable(_mirabelprofile_disable_modules(), TRUE);
}

function _mirabelprofile_profile_config_task() {
  variable_set('date_format_short', 'd/m/Y - H:i');
  variable_set('date_format_medium', 'D, d/m/Y - H:i');
  variable_set('date_format_long', 'l, j F, Y - H:i');
  variable_set('date_first_day', '1');
  variable_set('configurable_timezones', '0');
  variable_set('user_email_verification', '0');
  // Drupal 7.15
  variable_set("user_password_reset_timeout", 24*4*3600);


  // audio field not well initialized: see bug #1069298
  variable_set('audiofield_audioplayer_ogg', 'jplayer');

  variable_set('comment_default_mode_article', 0);
  variable_set('comment_default_mode_book', 0);
  variable_set('comment_default_mode_assoc', 0);
  variable_set('comment_default_mode_page', 0);
}

function mirabelprofile_form_install_configure_form_alter(&$form, $form_state) {
  // other variables from private context, not versioned here
  ini_set('include_path', '/root:'.ini_get('include_path'));
  require_once('connect-mirabel.php');
  _privmirabel_form_install_configure_form_alter($form, $form_state);

  $form['server_settings']['site_default_country']['#default_value'] = "FR";
  $form['server_settings']['date_default_timezone']['#default_value'] = "Europe/Paris";
  // Unset the timezone detect stuff
  unset($form['server_settings']['date_default_timezone']['#attributes']['class']);

  $form['update_notifications']['update_status_module']['#default_value'] = array(0);
}

// removed as a hook (in D7), TODO: here or .info ?
function _mirabelprofile_profile_modules() {
  // with sed -nr "/dependencies/s/.* = (.*)/'\1',/p" mirabelprofile.info|sort
  return array(
    'block',
    'book',
    'dblog',
    'field_ui',
    'help',
    'media_image_transform',
    'menu',
    'number',
    'options',
    'overlay',
    'path',
    'taxonomy',
  );
}
