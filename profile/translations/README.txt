# This directory should be used to place downloaded translations
# for installing Drupal core.
http://ftp.drupal.org/files/translations/7.x/drupal/drupal-7.12.fr.po
http://ftp.drupal.org/files/translations/7.x/views/views-7.x-3.2.fr.po
http://ftp.drupal.org/files/translations/7.x/media/media-7.x-2.0-unstable2.fr.po
http://ftp.drupal.org/files/translations/7.x/ctools/ctools-7.x-1.0-rc1.fr.po
http://ftp.drupal.org/files/translations/7.x/wysiwyg/wysiwyg-7.x-2.1.fr.po
http://ftp.drupal.org/files/translations/7.x/date/date-7.x-2.3.fr.po
http://ftp.drupal.org/files/translations/7.x/addressfield/addressfield-7.x-1.0-beta2.fr.po
http://ftp.drupal.org/files/translations/7.x/email/email-7.x-1.0.fr.po
http://ftp.drupal.org/files/translations/7.x/captcha/captcha-7.x-1.0-beta2.fr.po
http://ftp.drupal.org/files/translations/7.x/pathauto/pathauto-7.x-1.1.fr.po
http://ftp.drupal.org/files/translations/7.x/token/token-7.x-1.1.fr.po

# beta
http://ftp.drupal.org/files/translations/7.x/apachesolr/apachesolr-7.x-1.0-beta19.fr.po
http://ftp.drupal.org/files/translations/7.x/facetapi/facetapi-7.x-1.0-rc4.fr.po

