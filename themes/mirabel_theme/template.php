<?php

function mirabel_theme_theme($existing, $type, $theme, $path) {
  return array(
    'menu_tree__main_customtopmenu' => array(
      'render element'  => 'tree',
    ),
  );
  // the function stack explodes (up to 128M)
  // with this simple template inclusion !
    /*
      'views_view__vassoc__page' => array (
      'arguments'	=> array(),
      'path'		=> drupal_get_path('theme', 'mirabel_theme') . '/templates',
      'template'	=> 'views-view--vassoc--page',
      'preprocess functions' => array(
        'template_preprocess',
        'template_preprocess_views_view',
        'mirabel_theme_preprocess_views_view__vassoc__page'
      ),
    ),
    */
}

/*
  http://drupal.org/node/223440#custom-suggestions
  http://opensourcecms.pro/drupal/howto-drupal-7-sub-theme-creation-step-by-step/
function mirabel_theme_preprocess_node(&$variables) {
  vardump(__FILE__, $variables);die();
  $variables['theme_hook_suggestion'][] = 'node__promoted';
  }
*/

// Single page, create links from raw text of field_web
function mirabel_theme_field__field_web__assoc($variables) {
  $url = $variables['items'][0]['#markup'];
  return l($url, $url);
}

/* Refs:
   http://drupal.org/node/777930
   http://ourladysmaronite.org/
   http://deltup.org/download.php?view.6

   http://api.drupal.org/api/drupal/includes--menu.inc/function/theme_menu_tree/7
   http://drupal.org/node/748018
   http://drupal.stackexchange.com/questions/2258/how-do-i-append-a-unique-menu-id-to-the-menu
   http://thereisamoduleforthat.com/content/overriding-menu-item-themes
   This is a special one, usable for top-level <ul>.
   The standard (down-level) <ul> are implementable using hook_menu_tree__main_menu(&$variables),
   otherwise theme_menu_tree() is used.

   This hook does not redefine the id <ul> attribute. It's optional.
   But in such a case, a static $var may be used for id increment.
*/
function mirabel_theme_menu_tree__main_customtopmenu(&$variables) {
  return '<ul class="links clearfix nice-menu nice-menu-down" id="main-menu-links">' .
    $variables['tree'] .
    '</ul>';
}

function _mirabel_theme_add_li_attr(&$item1, $key, $data = array()) {
  if(is_integer($key) && isset($item1['#below']) && ! empty($item1['#below'])) {
    $item1['#attributes']['class'][] = $data['class'];
    // down: &#x25BE is stripped by Drupal, right: 0x25B8
    $item1['#title'] .= ' ' . $data['arrow'];
    $data['arrow'] = '▸';
    array_walk($item1['#below'], '_mirabel_theme_add_li_attr', $data);
  }
}

function mirabel_theme_links__system_main_menu($variables) {
  // TODO: si this test needed ?
  if (array_key_exists('id', $variables['attributes']) && $variables['attributes']['id'] == 'main-menu-links') {
    $pid = variable_get('menu_main_links_source', 'main-menu');
    $tree = menu_tree($pid);
    $tree['#theme_wrappers'][0] = 'menu_tree__main_customtopmenu';
    array_walk($tree, '_mirabel_theme_add_li_attr', array('class' => 'menuparent', 'arrow' => '▾'));
    // sadly don't work
    // $tree['#attributes']['class'][] = 'nice-menu';
    // $tree['#attributes']['class'][] = 'nice-menu-down';
    return drupal_render($tree);
  }
  return theme_links($variables);
}


/*
  Old nodes get their (unique) picture in the "large" format automatically inlined.
  It avoid setting the article node-type auto-display for the 'full' view_mode while
  still allowing to click on the attachment to get the original file.
*/
function mirabel_theme_preprocess_node(&$variables) {
  // XXX: hook_node_view_alter(&$build) would use
  // $build['body'][0]['#markup']
  if(intval($variables['created']) < 1326898148			// TVB Laxou janv 2012
     && $variables['view_mode'] == 'full' && $variables['type'] == 'article'
     && ( $variables['field_content_type'][LANGUAGE_NONE][0]['tid'] == '25'	// Article
	  || $variables['field_content_type'][LANGUAGE_NONE][0]['tid'] == '28')	// Coupure de presse
  ) {
      if(empty($variables['field_fichier']) ||
	 empty($variables['field_fichier'][0])) return;

      $markup = &$variables['content']['body'][0]['#markup'];

      // non-null node body ?
      //if(trim(preg_replace(';<!-- compaturi[^>]+>;', '', $markup)) ) return;
      if(! preg_match(';<!-- compaturi[^>]+>;', $markup)) return;

      /*
	double check the file-display settings, media_large (or file_styles_large) should be
	either activated in the default view_mode or "customized" with a valid display
	see #1418772, #1421444 and #1421542
      */
      $file = (object) ($variables['field_fichier'][0]);
      $imgsettings = _mirabel_media_code($file);
      // https://www.drupal.org/node/2143847
      $m = media_wysiwyg_get_file_without_label($file, 'media_large', $imgsettings);
      $new_markup = drupal_render($m);
      $markup = $new_markup . "\n" . $markup;
    }

  // here instead of mirabel.module because we not only need to override
  // template_preprocess_node from modules/node/node.module
  // but touch_preprocess_node !
  if($variables['view_mode'] == 'teaser' /* && $variables['is_front']*/) {
    // $variables['submitted'] = format_string('Le !datetime', array('!datetime' => $variables['date']));
    $variables['submitted'] = $variables['date'];
  }

}

// from _mirabel_do_media_string()
function _mirabel_media_code($file) {
  $image_info = array(
    'type' => 'media',
    'view_mode' => 'media_large',
    'fid' => $file->fid,
    'attributes' => array(
      'alt' => '',
      'title' => '',
      'class' => 'media-image',
      'typeof' => 'foaf:Image',
    ),
  );
  return $image_info;
}

/*
  TODO: see #939462
  https://drupal.org/node/939462#comment-5316602
function mirabel_theme_preprocess_views_view_fields__vassoc__page(&$vars) {
// tweak each title (eg: font++)
}

//  or create views-view--vassoc--page.tpl.php symlinked to
//  modules/views/theme/views-view-list.tpl.php
// in templates/

function mirabel_theme_preprocess_views_view__vassoc__page(&$variables) {
// drupal_add_css() if used with view custom css
}
*/

/*
function mirabel_theme_theme_registry_alter(&$theme_registry) {
  $theme_registry['views_view__vassoc__page']['preprocess functions'][] = 'mirabel_theme_preprocess_views_view__vassoc__page';
  }*/

/*
function mirabel_theme_preprocess_views_view__vassoc__page(&$vars) {
  drupal_add_css(drupal_get_path('theme', 'mirabel_theme') . '/css/mirabel-custom.css',
		 array('type' => 'file',
		       'group' => CSS_THEME));
}
*/

/*
  doc:
  insert a view: http://drupal.org/node/246742
  // $view = views_get_view('viewname');
  // print $view->execute_display('default', $args);
  or http://drupal.org/node/343533
  // print views_embed_view('viewname','displayname')
  or even: http://drupal.org/node/342132#comment-3726226
 */
