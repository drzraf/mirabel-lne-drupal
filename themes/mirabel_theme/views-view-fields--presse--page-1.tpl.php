<?php

// see http://api.drupal.org/api/views/theme!views-view-fields.tpl.php/7
echo '<div class="views-field-field-MYillustration">'
. (count($row->field_field_illustration) ? $fields['field_illustration']->content : '')
. '</div>'
. '<span>'
. $fields['title']->content
. $fields['field_date']->content
. (count($row->field_field_fichier) ? $fields['field_fichier']->content : '')
. '</span>';
